//Juego del saloon

#define COCOS2D_DEBUG 1
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "GameOver.h"
#include "CountryScene.h"
#include "GameInformation.h"
#include "FinalChallenge.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

extern int vidas;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene5::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene5::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene5::init()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoSaloon.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Saloon/EnemigoAparece.mp3");

	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	srand(time(NULL));
	_podVector = Vec2::ZERO;
	disparado = false;
	_isMoving = false;
	
	counter = 5;
	posicion1.x = 150.54;
	posicion1.y = 220.22;
	posicion2.x = 188.478;
	posicion2.y = 460.81;
	posicion3.x = 770.52;
	posicion3.y = 460.8;
	posicion4.x = 801.47;
	posicion4.y = 223.15;

	
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;
	
	
	
	this->schedule(schedule_selector(PlayScene5::UpdateTimer),1.0f);
	this->scheduleUpdate();

	

	auto listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene5::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene5::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene5::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	

	mouseListener = EventListenerMouse::create();
	mouseListener->onMouseMove = CC_CALLBACK_1(PlayScene5::onMouseMove, this);
 
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);


    //////////////////////////////
    // 1. super init remove
    if ( !Layer::init() )
    {
        return false;
    }

	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);
	
	Ventana_saloon = Sprite::create("PlayScene5/Ventana_saloon.png");
	   Ventana_saloon ->setPosition(posicion1);
	   Ventana_saloon ->setScale(0.6);
	   addChild(Ventana_saloon,1);

	   Ventana_saloon2 = Sprite::create("PlayScene5/Ventana_saloon.png");
	   Ventana_saloon2 ->setPosition(posicion2);
	   Ventana_saloon2 ->setScale(0.6);
	   addChild(Ventana_saloon2,1);

	   Ventana_saloon3 = Sprite::create("PlayScene5/Ventana_saloon.png");
	   Ventana_saloon3 ->setPosition(posicion3);
	   Ventana_saloon3 ->setScale(0.6);
	   addChild(Ventana_saloon3,1);

	   Ventana_saloon4 = Sprite::create("PlayScene5/Ventana_saloon.png");
	   Ventana_saloon4 ->setPosition(posicion4);
	   Ventana_saloon4 ->setScale(0.6);
	   addChild(Ventana_saloon4,1);

	Asteroid = Sprite::create("PlayScene/Cowboy004.png");
 			 Asteroid->setPosition(2000.0,2000.0);
			 Asteroid -> setScale(2);
			addChild(Asteroid, 1);

		Puerta_saloon = Sprite::create("PlayScene5/Puerta_saloon.png");
	Puerta_saloon ->setPosition(472.01,212.176);
	addChild(Puerta_saloon,1);

	 Fondo = Sprite::create("PlayScene5/Saloon.png");
	Fondo ->setPosition(visibleSize.width/2,visibleSize.height/2);
	Fondo ->setScale(0.6);
	addChild(Fondo,1);


    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

	

	 posx = visibleSize.width/2;
	posy = visibleSize.height/2;
	 _playerSprite = Sprite::create("PlayScene/Objetivo.png");
	 _playerSprite -> setPosition(Point(rand()%posx ,rand()%posy));
	 _playerSprite -> setScale(0.3);
 	 //_playerSprite->setPosition(Point(visibleSize.width/2,
	 //_playerSprite->getContentSize().height * 0.75));

	 addChild(_playerSprite, 1);

	 /*
    auto closeItem = MenuItemImage::create(
                                           "MenuScene/CloseNormal.png",
                                           "MenuScene/CloseSelected.png",
                                           CC_CALLBACK_1(PlayScene::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	auto BackItem = MenuItemImage::create(
											"AboutScene/Back_Button.png",
											"AboutScene/Back_Button.png",
											CC_CALLBACK_1(PlayScene::gotoMenu, this));

	BackItem->setScale(0.4);
	//Size final_length = BackItem->getContentSize()*0.3;
	//BackItem->setContentSize(final_length);
	BackItem->setPosition(BackItem->getContentSize().width/5,BackItem->getContentSize().height/2-60);
	
    // create menu, it's an autorelease object

    auto menu = Menu::create(closeItem,BackItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	*/


	
	/*
    auto label = LabelTTF::create("Pagina en construccion ", "Helvetica", 35);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 ));
	
	label->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label, 1);

	auto label2 = LabelTTF::create(" Por favor, disculpen las molestias", "Helvetica", 35);
    
    // position the label on the center of the screen
    label2->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 - 35 ));
	
	label2->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label2, 1);
	*/



    return true;
}

void PlayScene5::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void PlayScene5::UpdateTimer(float ct)
{

	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());

	if (counter == 3)
	{
		poscowboy = rand() % 4;
	
		switch (poscowboy)
		{
			case 0:
				this->removeChild(Asteroid);
			 Asteroid = Sprite::create("PlayScene/Cowboy004.png");
 			 Asteroid->setPosition(Point(posicion1.x,posicion1.y - 30));
			 Asteroid -> setScale(2);
			addChild(Asteroid, 1);
			break;

			case 1:
				this->removeChild(Asteroid);
				 Asteroid = Sprite::create("PlayScene/Cowboy004.png");
 			 Asteroid->setPosition(Point(posicion2.x,posicion2.y - 30));
			 Asteroid -> setScale(2);
			addChild(Asteroid, 1);
		

			break;
			case 2:
				this->removeChild(Asteroid);
				 Asteroid = Sprite::create("PlayScene/Cowboy004.png");
 			 Asteroid->setPosition(Point(posicion3.x,posicion3.y - 30));
			 Asteroid -> setScale(2);
			addChild(Asteroid, 1);
			break;
			case 3:
				this->removeChild(Asteroid);
				 Asteroid = Sprite::create("PlayScene/Cowboy004.png");
 			 Asteroid->setPosition(Point(posicion4.x,posicion4.y - 30));
			 Asteroid -> setScale(2);
			addChild(Asteroid, 1);
			break;
		}
		
		if (Asteroid->boundingBox().containsPoint(Ventana_saloon->getPosition()))
		{
			this->removeChild(Ventana_saloon);
		}

		else if (Asteroid->boundingBox().containsPoint(Ventana_saloon2->getPosition()))
		{
			this->removeChild(Ventana_saloon2);
		}

		else if (Asteroid->boundingBox().containsPoint(Ventana_saloon3->getPosition()))
		{
			this->removeChild(Ventana_saloon3);
		}

		else if(Asteroid->boundingBox().containsPoint(Ventana_saloon4->getPosition()))
		{
			this->removeChild(Ventana_saloon4);
		}
		/*
		for (int i = 0; i < 4; i++)
		{
			switch (i)
			{
			case 0:
				Ventana_saloon = Sprite::create("PlayScene5/Ventana_saloon.png");
		   Ventana_saloon ->setPosition(posicion1);
		   Ventana_saloon ->setScale(0.6);
		   addChild(Ventana_saloon,1);
		   if (Asteroid->boundingBox().containsPoint(Ventana_saloon->getPosition()))
		   {
			   this->removeChild(Ventana_saloon);
		   }
				break;
			case 1:
				Ventana_saloon2 = Sprite::create("PlayScene5/Ventana_saloon.png");
		   Ventana_saloon2 ->setPosition(posicion2);
		   Ventana_saloon2 ->setScale(0.6);
		   addChild(Ventana_saloon2,1);
			if (Asteroid->boundingBox().containsPoint(Ventana_saloon2->getPosition()))
		   {
			   this->removeChild(Ventana_saloon2);
		   }
				break;
			case 2:
				Ventana_saloon3 = Sprite::create("PlayScene5/Ventana_saloon.png");
		   Ventana_saloon3 ->setPosition(posicion3);
		   Ventana_saloon3 ->setScale(0.6);
		   addChild(Ventana_saloon3,1);
			if (Asteroid->boundingBox().containsPoint(Ventana_saloon3->getPosition()))
		   {
			   this->removeChild(Ventana_saloon3);
		   }
				break;
			case 3:
				Ventana_saloon4 = Sprite::create("PlayScene5/Ventana_saloon.png");
		   Ventana_saloon4 ->setPosition(posicion4);
		   Ventana_saloon4 ->setScale(0.6);
		   addChild(Ventana_saloon4,1);
			if (Asteroid->boundingBox().containsPoint(Ventana_saloon4->getPosition()))
		   {
			   this->removeChild(Ventana_saloon4);
		   }
				break;
			}
		
		}
		*/


		Puerta_saloon = Sprite::create("PlayScene5/Puerta_saloon.png");
	Puerta_saloon ->setPosition(472.01,212.176);
	addChild(Puerta_saloon,1);

	 Fondo = Sprite::create("PlayScene5/Saloon.png");
	Fondo ->setPosition(visibleSize.width/2,visibleSize.height/2);
	Fondo ->setScale(0.6);
	addChild(Fondo,1);

	 
	 posx = _playerSprite->getPositionX();
	 posy = _playerSprite->getPositionY();
	 this->removeChild(_playerSprite);
	 _playerSprite = Sprite::create("PlayScene/Objetivo.png");
	 _playerSprite -> setPosition(Point(posx ,posy));
	 _playerSprite -> setScale(0.3);
 	 //_playerSprite->setPosition(Point(visibleSize.width/2,
	 //_playerSprite->getContentSize().height * 0.75));

	 addChild(_playerSprite, 1);



	}


	if (counter==0)
	{
		this->unschedule( schedule_selector(PlayScene::UpdateTimer));
		//menuCloseCallback(this);
		
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		  if (!disparado)
		{
			vidas--;
			vidas_final--;
			alegre = false;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		}
		else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		  
		gotoCountry(this);
	}
}

void PlayScene5::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();
}


void PlayScene5::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}


void PlayScene5::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
		//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");
      CCPoint location = touch->getLocation();
	  CCLOG("%f,%f", location.x, location.y);
	  if (Asteroid->boundingBox().containsPoint(location) && _playerSprite->boundingBox().containsPoint(location))
	  {

	      disparado = true;
		  posicion_cowboy_disparado = Asteroid ->getPosition();
		  this->removeChild(Asteroid,true);
		   Cowboy_disparado = Sprite::create("PlayScene/Cowboy011.png");
		   Cowboy_disparado->setPosition(posicion_cowboy_disparado);
	       Cowboy_disparado -> setScale(2);
	      addChild(Cowboy_disparado, 1);
		  this->removeChild(Fondo);
		  _playerSprite->setVisible(false);
		   Fondo = Sprite::create("PlayScene5/Saloon.png");
		   Fondo ->setPosition(visibleSize.width/2,visibleSize.height/2);
	       Fondo ->setScale(0.6);
	      addChild(Fondo,1);

		  //gotoCountry(this);
	  }

	  
	       
    }
 }

void PlayScene5::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	/*
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_podVector = Vec2(0, POD_STEP_MOVE );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_podVector = Vec2(0, -POD_STEP_MOVE );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_podVector = Vec2(-POD_STEP_MOVE, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_podVector = Vec2(POD_STEP_MOVE, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_W:
			if (_playerSprite->boundingBox().containsPoint(Asteroid->getPosition()))
			{
				 disparado = true;
		  this->removeChild(Asteroid,true);
		  Cowboy_disparado = Sprite::create("PlayScene/Cowboy011.png");
		Cowboy_disparado->setPosition(Point(visibleSize.width/2 + 200,
						   visibleSize.height/2 + 75));
	 Cowboy_disparado -> setScale(1.3);
	 addChild(Cowboy_disparado, 1);
	  _playerSprite->setVisible(false);
		 // this->removeChild(_playerSprite,true);
				 //menuCloseCallback(this);
				 //gotoCountry(this);
			}
			break;
		
	}
	*/
}

void PlayScene5::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}

void PlayScene5::onMouseMove(Event *event) {
	static Vec2 *oldPosition;
	auto *e = dynamic_cast<EventMouse *>(event) ;
	if (oldPosition == NULL) {
		oldPosition = new Vec2(e->getCursorX(), e->getCursorY());
		
		} else {
			_podVector = Vec2(e->getCursorX() - oldPosition->x,
			e->getCursorY() - oldPosition->y);
 
	 if (!_isMovingByMouse) {
		_isMovingByMouse = true;
		oldPosition->x = e->getCursorX();
		oldPosition->y = e->getCursorY();
		}
	}
}

void PlayScene5::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	posicion_mouse = get_cursor_pos();
	_playerSprite->setPosition(posicion_mouse);
	/*
	 if (_isMoving || _isMovingByMouse) {
		 Vec2 newPos = Vec2(_playerSprite->getPosition().x + _podVector.x,
		 _playerSprite->getPosition().y + _podVector.y);
 
	 if (newPos.x >=_playerSprite->getBoundingBox().size.width/2 &&
		 newPos.x <= visibleSize.width - _playerSprite->getBoundingBox().size.width/2 &&
		 newPos.y >=_playerSprite->getBoundingBox().size.height/2 &&
		 newPos.y <= visibleSize.height - _playerSprite->getBoundingBox().size.height/2)
		{
		  _playerSprite->setPosition(newPos);
		}		_isMovingByMouse = false;	 }
	 */
		
}

cocos2d::Point PlayScene5::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}







