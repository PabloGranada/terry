#ifndef __PLAY_SCENE1JAPAN_H__
#define __PLAY_SCENE1JAPAN_H__

#include "cocos2d.h"

USING_NS_CC;



class PlayScene1Japan : public cocos2d::Layer
{
public:

	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	//Fin de la declaracion de mierda para la animacion.

	bool movido;
	float VELOCIDAD ;
	float pos_abajo_y;
	float pos_arriba_y;
	int counter,i;
	 Sprite *SpriteVacio;
	 Sprite *Fondo;
	 Sprite *Terry;
	 Sprite *Dhalsim1;
	 Sprite *Dhalsim2;
	 LabelTTF *label_timer;
	 Point posicion_mouse,posicion_mouse_original;
	 EventListenerKeyboard *keyboardListener;
	 EventListenerTouchAllAtOnce *touchListener;
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	void update(float dt);
	void UpdateTimer(float ct);
	void MoverAbajo(Node *pSender);
	void MoverArriba(Node *pSender);
	void Movido(Node *pSender);

	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	cocos2d::Point get_cursor_pos();
	void gotoCountry2(Ref* pSender);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene1Japan);
};

#endif // __MENU_SCENE_H__
