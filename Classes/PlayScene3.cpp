//Juego del boxeo

#define COCOS2D_DEBUG 1

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "CountryScene.h"
#include "GameInformation.h"
#include "FinalChallenge.h"
#include "GameOver.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

extern int vidas ; 
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene3::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene3::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene3::init()
{	
	
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoBoxeo.mp3");

	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;


	_podVector = Vec2::ZERO;
	_isMoving = false;
	golpe_bueno = false;
	pulsado = false;
	counter = 5;
	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);

	this->schedule(schedule_selector(PlayScene3::UpdateTimer),1.0f);
	this->scheduleUpdate();

	listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene3::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);
	

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene3::keyPressed, this);
	//keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene2::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
	//Fondo
	Fondo = Sprite::create("PlayScene3/FondosLittleMac.png");
	Fondo->setPosition(visibleSize.width/2 - 3,visibleSize.height/2);
	Fondo->setScale(3);
	Fondo->setScaleX(3.7);
	
	addChild(Fondo,1);



	 //Barra
	 _playerSprite = Sprite::create("PlayScene3/Medidor.png");
 	 _playerSprite->setPosition(Point(_playerSprite->getContentSize().width / 2 - 250 ,
	  visibleSize.height /2 ));
	 _playerSprite->setScale(0.2);

	 addChild(_playerSprite, 1);

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

	 //Pu�etazo
	Asteroid = Sprite::create("PlayScene3/Punyetazo.png");
 	 Asteroid->setPosition(Point(Asteroid->getContentSize().width/2 - 55,
	  visibleSize.height /2 - 120 ));
	 Asteroid->setScale(0.25);
	 addChild(Asteroid, 1);
	 
	 //Aqui poner sprite en blanco
	 //"PlayScene3/spriteVacio.png"
	 Tope = Sprite::create("PlayScene3/spriteVacio.png");
	 Tope -> setPosition(Point(Asteroid->getContentSize().width/2 - 55 , visibleSize.height/2 + visibleSize.height/4 - 25));
	 Tope-> setScale(0.1);
	 addChild(Tope,1);

	 //Poner barra negra
	 Objetivo = Sprite::create("PlayScene3/spriteVacio.png");
	 Objetivo -> setPosition(Point(Asteroid->getContentSize().width/2 -55,visibleSize.height/2 + visibleSize.height/4 - 100));
	 Objetivo-> setScaleX(0.25);
	 Objetivo -> setScaleY(0.5);
	 addChild(Objetivo,1);

	  //Sprite boxeador
	 Boxeador = Sprite::create("PlayScene3/standBy.png");
	 Boxeador -> setPosition(Point(visibleSize.width/2,visibleSize.height/2));
	 Boxeador-> setScale(3);
	 addChild(Boxeador,1);

	 //Sprite Mac
	 Mac = Sprite::create("PlayScene3/espera.png");
	 Mac -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
	 Mac-> setScale(3);
	 addChild(Mac,1);


	
	 /*
    auto closeItem = MenuItemImage::create(
                                           "MenuScene/CloseNormal.png",
                                           "MenuScene/CloseSelected.png",
                                           CC_CALLBACK_1(PlayScene3::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	auto BackItem = MenuItemImage::create(
											"AboutScene/Back_Button.png",
											"AboutScene/Back_Button.png",
											CC_CALLBACK_1(PlayScene3::gotoMenu, this));

	BackItem->setScale(0.4);
	//Size final_length = BackItem->getContentSize()*0.3;
	//BackItem->setContentSize(final_length);
	BackItem->setPosition(BackItem->getContentSize().width/5,BackItem->getContentSize().height/2-60);
	
    // create menu, it's an autorelease object

    auto menu = Menu::create(closeItem,BackItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	*/

	/*
    auto label = LabelTTF::create("Pagina del nivel 3 en construccion ", "Helvetica", 35);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 ));
	
	label->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label, 1);

	auto label2 = LabelTTF::create(" Por favor, disculpen las molestias", "Helvetica", 35);
    
    // position the label on the center of the screen
    label2->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 - 35 ));
	
	label2->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label2, 1);
	*/
	
	return true;
}

void PlayScene3::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void PlayScene3::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		this->unschedule( schedule_selector(PlayScene3::UpdateTimer));
		//menuCloseCallback(this);
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
	      CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		 
		  if (!golpe_bueno)
		  {
			  vidas--;
			  vidas_final--;
			  alegre = false;
			  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		  }
		 else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		gotoCountry(this);
	}
}

void PlayScene3::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}

void PlayScene3::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}


void PlayScene3::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  if (Asteroid->boundingBox().containsPoint(Objetivo->getPosition()) && Asteroid->boundingBox().containsPoint(location))
	  {
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/PunchOut/PunyoFuerte.mp3");
				//Pega el pu�etazo

			this->removeChild(Boxeador,true);
			Boxeador_golpeado = Sprite::create("PlayScene3/tumbado.png");
		    Boxeador_golpeado -> setPosition(Point(visibleSize.width/2,visibleSize.height/2));
			Boxeador_golpeado -> setScale(3);
			addChild(Boxeador_golpeado,1);
			this->removeChild(Mac,true);
			Mac_golpeado = Sprite::create("PlayScene3/Punyetazo_Mac.png");
		    Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
			Mac_golpeado -> setScale(3);
			addChild(Mac_golpeado,1);
			golpe_bueno = true;
			pulsado = true;
			listener->setEnabled(false);
	  }

	  else
	  {
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/PunchOut/PunyoDebil.mp3");
			this->removeChild(Mac,true);
			Mac_golpeado = Sprite::create("PlayScene3/Punyetazo_Mac.png");
			Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
			Mac_golpeado -> setScale(3);
			addChild(Mac_golpeado,1);
			golpe_bueno = false;
			pulsado = true;
			listener->setEnabled(false);
	  }
			
	  
	       
    }
 }
 
 

void PlayScene3::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		/*
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_podVector = Vec2(0, POD_STEP_MOVE2 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_podVector = Vec2(0, -POD_STEP_MOVE2 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_podVector = Vec2(-POD_STEP_MOVE2, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_podVector = Vec2(POD_STEP_MOVE2, 0 );
			_isMoving = true;
			break;
			*/
		case EventKeyboard::KeyCode::KEY_SPACE:
			
			if (Asteroid->boundingBox().containsPoint(Objetivo->getPosition()))
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/PunchOut/PunyoFuerte.wav");
				//Pega el pu�etazo

				this->removeChild(Boxeador,true);
				Boxeador_golpeado = Sprite::create("PlayScene3/tumbado.png");
		        Boxeador_golpeado -> setPosition(Point(visibleSize.width/2,visibleSize.height/2));
			    Boxeador_golpeado -> setScale(3);
				addChild(Boxeador_golpeado,1);
				this->removeChild(Mac,true);
				Mac_golpeado = Sprite::create("PlayScene3/Punyetazo_Mac.png");
		        Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
			    Mac_golpeado -> setScale(3);
				addChild(Mac_golpeado,1);
				golpe_bueno = true;
				
				

				 //menuCloseCallback(this);
				 //gotoCountry(this);
			}

			else
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/PunchOut/PunyoDebil.wav");
				this->removeChild(Mac,true);
				Mac_golpeado = Sprite::create("PlayScene3/Punyetazo_Mac.png");
		        Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
			    Mac_golpeado -> setScale(3);
				addChild(Mac_golpeado,1);
				golpe_bueno = false;
				
				
			}
			
			pulsado = true;
			break;
		
	}
}

/*
void PlayScene2::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}
*/


void PlayScene3::update(float dt) {


	if (pulsado)
	{
		keyboardListener->setEnabled(false);
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();

	auto *move = (FiniteTimeAction *) MoveBy::create(2/VELOCIDAD_MEDIDOR,
	 Point(0,visibleSize.height - 
	 Asteroid ->getBoundingBox().size.height ));

	 move->retain();

	 auto *seq = Sequence::create(move, NULL);

	 seq->retain();
		 
	 Asteroid->runAction(seq);

	 if (Asteroid->boundingBox().containsPoint(Tope->getPosition()))
	 {
		 Asteroid->stopAllActions();
	 }

	 //Llega al final y el jugador ha golpeado
	 if (Asteroid->boundingBox().containsPoint(Tope->getPosition()) && !golpe_bueno)
	 {
		 this->removeChild(Boxeador,true);
		 Boxeador_golpeado = Sprite::create("PlayScene3/Golpeo.png");
		 Boxeador_golpeado -> setPosition(Point(visibleSize.width/2,visibleSize.height/2));
		 Boxeador_golpeado -> setScale(3);
		 addChild(Boxeador_golpeado,1);
		 this->removeChild(Mac,true);
		 this->removeChild(Mac_golpeado,true);
		 Mac_golpeado = Sprite::create("PlayScene3/golpeado.png");
		 Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
		 Mac_golpeado -> setScale(3);
		 addChild(Mac_golpeado,1);
		 alegre = false;
		 
	 }

	 if (Asteroid->boundingBox().containsPoint(Tope->getPosition()) && golpe_bueno)
	 {
		 this->removeChild(Mac,true);
		 this->removeChild(Mac_golpeado,true);
		 Mac_golpeado = Sprite::create("PlayScene3/win.png");
		 Mac_golpeado -> setPosition(Point(visibleSize.width/2 - 50,visibleSize.height/2 - 100));
		 Mac_golpeado -> setScale(3);
		 addChild(Mac_golpeado,1);
	 }
	 
	 
	/*
	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	 if (_isMoving) {
		 Vec2 newPos = Vec2(_playerSprite->getPosition().x + _podVector.x,
		 _playerSprite->getPosition().y + _podVector.y);
 
	 if (newPos.x >=_playerSprite->getBoundingBox().size.width/2 &&
		 newPos.x <= visibleSize.width - _playerSprite->getBoundingBox().size.width/2 &&
		 newPos.y >=_playerSprite->getBoundingBox().size.height/2 &&
		 newPos.y <= visibleSize.height - _playerSprite->getBoundingBox().size.height/2)
		{
		  _playerSprite->setPosition(newPos);
		}	 }
	 */
}








