#ifndef __CREDITS_H__
#define __CREDITS_H__

#include "cocos2d.h"

USING_NS_CC;



class Credits : public cocos2d::Layer
{
public:
	int pos_y;
	float altura;
	int counter;
	const char* Spritesheet;	
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSprite* Frame2;
	//Vec2* charDirector;
	int i;
	Size visibleSize;
	Sprite *Fondo;
	Sprite *Pablo;
	Sprite *Alejandro;
	Sprite *Jorge;
	Sprite *Creditos;
	Sprite *GameOver;
	Sprite *Thanks;
	Sprite *PabloCartel;
	Sprite *AlexCartel;
	Sprite *JorgeCartel;
	Sprite *Avion;
	Sprite *_backgroundSpriteArray[2];
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	void update(float dt);
	void UpdateTimer(float ct);
	void gotoMenu(Ref *pSender);
	void menuCloseCallback(Ref* pSender);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(Credits);
};

#endif // __MENU_SCENE_H__
