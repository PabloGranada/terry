//Juego del sushi

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene3Japan.h"
#include "CountryScene2.h"
#include "PlayScene4.h"
#include "CountryScene.h"
#include "FinalChallenge.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene3Japan ::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene3Japan ::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene3Japan ::init()
{
	srand(time(NULL));
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoCinta.mp3");
	Size visibleSize = Director::getInstance()->getVisibleSize();
	 Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	posicion1.x = 80;
	posicion2.x = 390;
	posicion3.x = 550;
	posicion4.x = 700;
	posicion1.y= visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8;
	posicion2.y= visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8;
	posicion3.y= visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8;
	posicion4.y= visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8;
	pos_comida = 1;
	counter = 5;
	seleccion_correcta = false;
	repe = false;

	listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene3Japan::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	auto Fondo = Sprite::create("PlayScene3Japan/FondoJuego.png");

    // position the sprite on the center of the screen
    Fondo->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2 ));
	//FondoJapon->setScale(0.85);
	Fondo->setScaleX(1.55);
	Fondo->setScaleY(1.38);
	
    // add the sprite as a child to this layer
    this->addChild(Fondo, 0);

	//Loading scrollable background
	for (int i = 0; i < 2; i++) {
		_backgroundSpriteArray[i] = Sprite::create("PlayScene3Japan/CintaTransportadora/CintaCompleta.png");
		_backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * (i - 0.5f),
		 visibleSize.height/2 ));
		addChild(_backgroundSpriteArray[i], 0);
 }

	Terry = Sprite::create("PlayScene3Japan/TerrySprites/TerryStandBy.png");
	Terry ->setPosition(Vec2(visibleSize.width/2 + 80, visibleSize.height/2 + 100));
	addChild(Terry,0);

	/*
	MensajeComida1 = Sprite::create("PlayScene3Japan/Comida/ComidaPensada/Comida1.png");
	MensajeComida1 ->setPosition(Vec2(80, visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8));
	MensajeComida1 ->setScale(1.5);
	addChild(MensajeComida1,0);

	MensajeComida2 = Sprite::create("PlayScene3Japan/Comida/ComidaPensada/Comida2.png");
	MensajeComida2 ->setPosition(Vec2(390, visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8));
	MensajeComida2 ->setScale(1.5);
	addChild(MensajeComida2,0);

	MensajeComida3 = Sprite::create("PlayScene3Japan/Comida/ComidaPensada/Comida3.png");
	MensajeComida3 ->setPosition(Vec2(550, visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8));
	MensajeComida3 ->setScale(1.5);
	addChild(MensajeComida3,0);

	MensajeComida4 = Sprite::create("PlayScene3Japan/Comida/ComidaPensada/Comida4.png");
	MensajeComida4 ->setPosition(Vec2(700, visibleSize.height/2 + visibleSize.height/4 + visibleSize.height/8));
	MensajeComida4 ->setScale(1.5);
	addChild(MensajeComida4,0);
	*/

	SpriteVacio = Sprite::create("PlayScene3/spriteVacio.png");
	SpriteVacio->setPosition(posicion3);
	addChild(SpriteVacio,0);

	//pos_comida = rand()%4;
	for (int i = 0; i < 4; i++)
	{
		tipo_comida = aleatorio(min,max,repe);
		
		 __String *filename = __String::createWithFormat ("PlayScene3Japan/Comida/ComidaPensada/Comida%i.png",
		 tipo_comida);
		 __String *filename2 = __String::createWithFormat ("PlayScene3Japan/Comida/PlatosCinta/Servido%i.png",
		 tipo_comida);
		 switch (i)
		 {
			 case 0:
				 Mensaje_Comida = Sprite::create(filename->getCString());
				 Mensaje_Comida ->setPosition(posicion1);
				 Mensaje_Comida ->setScale(1.5);
				 addChild( Mensaje_Comida,0);
				 Comida2 = Sprite::create(filename2->getCString());
				 Comida2 -> setPosition(Vec2(- 400, visibleSize.height/2));
				 Comida2->setScale(1.5);
				 //move = (FiniteTimeAction *) JumpTo::create(2/COMIDA_SPEED,
				// Point(visibleSize.width - 
				// Comida2->getBoundingBox().size.width,visibleSize.height/2 ),300,1);
				 move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
				 Point(visibleSize.width - 
				 Comida2->getBoundingBox().size.width,0 ));

				 move->retain();

				 seq = Sequence::create(move,
				 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
				 NULL);

				 seq->retain();
		 
				 Comida2->runAction(seq);
 
				addChild(Comida2, 2);
	       
			 break;
			 case 1:
				 Mensaje_Comida = Sprite::create(filename->getCString());
				 Mensaje_Comida ->setPosition(posicion2);
				 Mensaje_Comida ->setScale(1.5);
				 addChild( Mensaje_Comida,0);
				 Comida3 = Sprite::create(filename2->getCString());
				 Comida3 -> setPosition(Vec2(- 100, visibleSize.height/2));
				 Comida3->setScale(1.5);
				 move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
				 Point(visibleSize.width - 
				 Comida3->getBoundingBox().size.width,0 ));

				 move->retain();

				seq = Sequence::create(move,
				 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
				 NULL);

				 seq->retain();
		 
				 Comida3->runAction(seq);
 
				addChild(Comida3, 2);
			 
			 break;
			 case 2:
				 Comida_correcta = Sprite::create(filename->getCString());
				 Comida_correcta ->setPosition(posicion3);
				 Comida_correcta ->setScale(1.5);
				 addChild(Comida_correcta,0);
				 Comida_cinta_correcta = Sprite::create(filename2->getCString());
				  Comida_cinta_correcta -> setPosition(Vec2(- 300, visibleSize.height/2));
				  Comida_cinta_correcta->setScale(1.5);
				 move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
				 Point(visibleSize.width - 
				 Comida_cinta_correcta->getBoundingBox().size.width,0 ));

				 move->retain();

				 seq = Sequence::create(move,
				 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
				 NULL);

				 seq->retain();
		 
				  Comida_cinta_correcta->runAction(seq);
 
				addChild( Comida_cinta_correcta, 2);
			 

			 break;
			 case 3:
				 Mensaje_Comida = Sprite::create(filename->getCString());
			 	 Mensaje_Comida ->setPosition(posicion4);
				 Mensaje_Comida ->setScale(1.5);
				 addChild( Mensaje_Comida,0);
				 Comida4 = Sprite::create(filename2->getCString());
				 Comida4 -> setPosition(Vec2(- 200, visibleSize.height/2));
				 Comida4->setScale(1.5);
				 move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
				 Point(visibleSize.width - 
				 Comida4->getBoundingBox().size.width,0 ));

				 move->retain();

				seq = Sequence::create(move,
				 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
				 NULL);

				 seq->retain();
		 
				 Comida4->runAction(seq);
 
				addChild(Comida4, 2);
			 
			 break;
		 }
		
	}
	
	/*
	Comida = Sprite::create("PlayScene3Japan/Comida/PlatosCinta/Servido1.png");
	Comida -> setPosition(Vec2(- 100, visibleSize.height/2));
	Comida->setScale(1.5);
	auto *move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
	 Point(visibleSize.width - 
	 Comida->getBoundingBox().size.width,0 ));

	 move->retain();

	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
	 NULL);

	 seq->retain();
		 
	 Comida->runAction(seq);
 
	 addChild(Comida, 2);

	 Comida2 = Sprite::create("PlayScene3Japan/Comida/PlatosCinta/Servido2.png");
	Comida2 -> setPosition(Vec2(- 200, visibleSize.height/2));
	Comida2->setScale(1.5);
	move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
	 Point(visibleSize.width - 
	 Comida2->getBoundingBox().size.width,0 ));

	 move->retain();

	 seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
	 NULL);

	 seq->retain();
		 
	 Comida2->runAction(seq);
 
	 addChild(Comida2, 2);

	  Comida3 = Sprite::create("PlayScene3Japan/Comida/PlatosCinta/Servido3.png");
	Comida3 -> setPosition(Vec2(- 300, visibleSize.height/2));
	Comida3->setScale(1.5);
	move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
	 Point(visibleSize.width - 
	 Comida3->getBoundingBox().size.width,0 ));
	 

	 move->retain();

	 seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
	 NULL);

	 seq->retain();
		 
	 Comida3->runAction(seq);
 
	 addChild(Comida3, 2);

	   Comida4 = Sprite::create("PlayScene3Japan/Comida/PlatosCinta/Servido4.png");
	Comida4 -> setPosition(Vec2(- 400, visibleSize.height/2));
	Comida4->setScale(1.5);
	move = (FiniteTimeAction *) MoveBy::create(2/COMIDA_SPEED,
	 Point(visibleSize.width - 
	 Comida4->getBoundingBox().size.width,0 ));

	 move->retain();

	 seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene3Japan::Comidallegada, this)), 
	 NULL);

	 seq->retain();
		 
	 Comida4->runAction(seq);
 
	 addChild(Comida4, 2);
	 */
	

	this->schedule(schedule_selector(PlayScene3Japan::UpdateTimer),1.0f);
	this->scheduleUpdate();

	return true ;
}   

void PlayScene3Japan::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		this->unschedule( schedule_selector(PlayScene3::UpdateTimer));
		//menuCloseCallback(this);
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		
		 
		  if (!seleccion_correcta)
		  {
			  vidas_japon--;
			  vidas_final--;
			  alegre = false;
			   CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
			 // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		  }

		  else
		  
			  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		  
		auto director = Director::getInstance();
		auto glview = director->getOpenGLView();
	
		glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
		//	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		gotoCountry2(this);
	}
}

void PlayScene3Japan::gotoCountry2(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();
}

void PlayScene3Japan::update(float dt) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
 
	// Scrolls background
	for (int i = 0; i < 2; i++) {
		 if (_backgroundSpriteArray[i]->getPosition().x < (visibleSize.width* -0.5))
		 _backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * 1.5f,
	     visibleSize.height/2 ));
		 else
			 _backgroundSpriteArray[i]->setPosition(
			 Point(_backgroundSpriteArray[i]->getPosition().x  +
			 (CINTA_SPEED * visibleSize.width * dt),
			 _backgroundSpriteArray[i]->getPosition().y));
	}
	 
}void PlayScene3Japan::Comidallegada(Node *pSender) {	pSender->stopAllActions();
	 
	 removeChild(pSender);}void PlayScene3Japan::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
		//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");

        CCPoint location = touch->getLocation();		CCLOG("%f,%f", location.x, location.y);		if (Comida_cinta_correcta->boundingBox().containsPoint(location))		{			seleccion_correcta = true;			this->removeChild(Terry);			Terry = Sprite::create("PlayScene3Japan/TerrySprites/TerryWin.png");
			Terry ->setPosition(Vec2(visibleSize.width/2 + 80, visibleSize.height/2 + 100));
			addChild(Terry,0);			Comida_cinta_correcta->stopAllActions();			Comida_cinta_correcta->setPosition(Vec2(Terry->getPositionX() + 30, Terry->getPositionY() - 60));		}		else		{			this->removeChild(Terry);			Terry = Sprite::create("PlayScene3Japan/TerrySprites/TerryLose.png");
			Terry ->setPosition(Vec2(visibleSize.width/2 + 80, visibleSize.height/2 + 100));
			addChild(Terry,0);			/*			if (Comida->boundingBox().containsPoint(location))
			{
				Comida->stopAllActions();				Comida->setPosition(Vec2(Terry->getPositionX() + 30, Terry->getPositionY() - 60));
			}			*/						if (Comida2->boundingBox().containsPoint(location))
			{
				Comida2->stopAllActions();				Comida2->setPosition(Vec2(Terry->getPositionX() + 30, Terry->getPositionY() - 60));
			}			if (Comida3->boundingBox().containsPoint(location))
			{
				Comida3->stopAllActions();				Comida3->setPosition(Vec2(Terry->getPositionX() + 30, Terry->getPositionY() - 60));
			}			if (Comida4->boundingBox().containsPoint(location))
			{
				Comida4->stopAllActions();				Comida4->setPosition(Vec2(Terry->getPositionX() + 30, Terry->getPositionY() - 60));
			}					}		listener->setEnabled(false);	}}
bool PlayScene3Japan::repetido(int numero)
{
	numero_repetido = false;
	for (int i = 0; i < comidas.size(); i++)
	{
		if (numero == comidas[i])
		{
			numero_repetido = true;
			break;
		}
	}

	return numero_repetido;
}

long int PlayScene3Japan::aleatorio(int min, int max,bool repe)
{
	
	if (comidas.size() != 4)
	{
		do
		{
			num = rand()% 4 + 1;
			repe = repetido(num);
		} while (repe != false);
		
		
		comidas.push_back(num);
		return num;
	}
	else
	{
		return -1;
	}
}