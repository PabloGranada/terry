//Juego de la copichuela

#define COCOS2D_DEBUG 1

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene6.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "PlayScene6.h"
#include "GameOver.h"
#include "CountryScene.h"
#include "SelectCountryScene.h"
#include "GameInformation.h"
#include "SimpleAudioEngine.h"
#include "PantallaWin.h"

USING_NS_CC;

extern int vidas; 
extern int vidas_final;
extern bool alegre;

extern bool desafio_activo;

Scene* PlayScene6::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    // 'layer' is an autorelease object
    auto layer = PlayScene6::create();

	layer->setPhysicsWorld(scene->getPhysicsWorld());

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene6::init()
{	

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoZombies.mp3");
	srand(time(NULL));
	posicion = 1;
	
	posicion1.x = 830.42;
	posicion1.y = 78.41;
	posicion2.x = 775.51;
    posicion2.y = 208.2;
	posicion3.x = 720.6;
	posicion3.y = 340.98;
	posicion4.x = 663.69;
	posicion4.y = 470.76;
	posicion1_izq.x = 141.55;
	posicion1_izq.y = 104.35;
	posicion2_izq.x = 192.47;
	posicion2_izq.y = 238.13;
	posicion3_izq.x = 243.39;
	posicion3_izq.y = 376.9;
	posicion4_izq.x = 288.31;
	posicion4_izq.y = 508.69;
	Offset.x = 21.0;
	Offset.y = 20.0;

	
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;


	_podVector = Vec2::ZERO;
	_isMoving = false;
	navajazo = false;
	colisionado = false;
	fallado = true;
	Zombi_perder_int = 4;
	counter = 12;

	
	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);

	this->schedule(schedule_selector(PlayScene6::UpdateTimer),1.0f);
	this->schedule(schedule_selector(PlayScene6::spawnZombi), 0.85f);
	this->scheduleUpdate();

	//auto listener = EventListenerTouchAllAtOnce::create();
	
   // listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene6::onTouchesBegan, this);
	//_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);
	
	contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(PlayScene6::onContactBegin, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene6::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene6::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	//CCLOG("Pos_Terry: %d", pos_terry);


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

		//Fondo

	Fondo = Sprite::create("PlayScene6/FondoZombies.png");
	Fondo->setPosition(visibleSize.width/2,visibleSize.height/2);
	Fondo ->setScale(1);
	addChild(Fondo,1);

	
    
	//Terry->runAction(action4);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	//A�adido para terry derecha
	
	frameCache4 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache4->addSpriteFramesWithFile("PlayScene6/terry/terry.plist");
    
	spritesheet4 = CCSpriteBatchNode::create("PlayScene6/terry/terry.png");
	this->addChild(spritesheet4,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames4(5);
	for ( i = 1; i <= 4; i++)
	{
		CCString* filename4 = CCString::createWithFormat("terryd_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame4 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename4->getCString());
		SpriteFrames4.pushBack(frame4);
	}

	CCAnimation* runAnim4 = CCAnimation::createWithSpriteFrames(SpriteFrames4,0.3);
	Terry = CCSprite::createWithSpriteFrameName("terryd_01.png");
	Terry->setPosition(Vec2(visibleSize.width/2 + 50, visibleSize.height/2));
	Terry->setScale(1);

    
	action4 = CCRepeatForever::create(CCAnimate::create(runAnim4));
	Terry->runAction(action4);

	auto *body = PhysicsBody::createCircle(Terry->getBoundingBox().size.width/2);
		body->setContactTestBitmask(true);
		body->setDynamic(true);
		Terry->setPhysicsBody(body);
	    

	addChild(Terry,1);

	//A�adido para terry izquierda.

	frameCache5 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache5->addSpriteFramesWithFile("PlayScene6/terry/terry.plist");
    
	spritesheet5 = CCSpriteBatchNode::create("PlayScene6/terry/terry.png");
	this->addChild(spritesheet5,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames5(5);
	for ( i = 1; i <= 4; i++)
	{
		CCString* filename5 = CCString::createWithFormat("terryi_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame5 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename5->getCString());
		SpriteFrames5.pushBack(frame5);
	}

	CCAnimation* runAnim5 = CCAnimation::createWithSpriteFrames(SpriteFrames5,0.3);
    

    

	Terry2 = CCSprite::createWithSpriteFrameName("terryi_01.png");
	Terry2->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	action5 = CCRepeatForever::create(CCAnimate::create(runAnim5));
	Terry2->setScale(1);
	Terry2->setVisible(false);

	Terry2->runAction(action5);

	 //Terry = Sprite::create("PlayScene1Japan/terryMeditando.png");
	//Terry ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	//Terry -> setScale(2.5);
	//body = PhysicsBody::createCircle(Terry2->getBoundingBox().size.width/2);
	//	body->setContactTestBitmask(true);
	//	body->setDynamic(true);
	//	Terry2->setPhysicsBody(body);
	addChild(Terry2,1);

	//FARAMIR
	//A�adido para el turista.
	
	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene6/turistas/turista.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene6/turistas/turista.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(5);
	for ( i = 1; i <= 5; i++)
	{
		CCString* filename = CCString::createWithFormat("turista_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.7);
	Frame2 = CCSprite::createWithSpriteFrameName("turista_01.png");

	Frame2->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Frame2->setScale(1);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Frame2->runAction(action2);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	
	


	/* TERRY PREVIO
	Terry = Sprite::create("PlayScene1Japan/terryMeditando.png");
	Terry ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Terry -> setScale(2.5);
	*/
	 body = PhysicsBody::createCircle(spritesheet2->getBoundingBox().size.width/2);
	 body->setContactTestBitmask(true);
	 body->setDynamic(true);
	 spritesheet2->setPhysicsBody(body);

	 spritesheet2->addChild(Frame2,3);	

	
	
	return true;
     	//Aqui termina el a�adido para el turista.
	
}

/*
void PlayScene6::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
*/
bool PlayScene6::onContactBegin(PhysicsContact &contact) {
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//goToPauseScene(this);
	// CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Crash.wav");
	// goToGameOverScene(this);
	
	 auto nodeA = contact.getShapeA()->getBody()->getNode();
     auto nodeB = contact.getShapeB()->getBody()->getNode();

	
// nodo A o B son Terry
	if (nodeA->getPosition() == Terry->getPosition() || nodeA->getPosition() == Terry2->getPosition()|| nodeB->getPosition() == Terry->getPosition() || nodeB->getPosition() == Terry2->getPosition()) //node B es zombi
	{
		//nodo A es terry, nodo B zombi
		if (nodeA->getPosition() == Terry->getPosition() || nodeA->getPosition() == Terry2->getPosition())
		{
			this->removeChild(nodeB);
		}
		//nodoB zombi
		else
		{
			this->removeChild(nodeA);
		}
		
	}
	//Game over
	else if (nodeA ->getPosition() == Frame2->getPosition() ||nodeB ->getPosition() == Frame2->getPosition())
	{
		//if (nodeB->isVisible()==true){
		this->unscheduleUpdate();
		this->unschedule(schedule_selector(PlayScene6::UpdateTimer));
		this->schedule(schedule_selector(PlayScene6::Zombi_perder,1.0f));
			//_Zombis.remove(nodeB);
			//nodeB->removeFromParentAndCleanup(true);
		//}
	}

	else
	{
		return true;
	}
	return true;
	 

	 
}
void PlayScene6::Zombi_perder(float ct)
{
	Zombi_perder_int --;
	if (Zombi_perder_int==3)
	{
			vidas --;
			vidas_final--;
	alegre = false;
	
	//FARAMIR
	//A�adido para los turistas muertos.

	frameCache6 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache6->addSpriteFramesWithFile("PlayScene6/turistas/turistad.plist");
    
	spritesheet6 = CCSpriteBatchNode::create("PlayScene6/turistas/turistad.png");
	this->addChild(spritesheet6,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames6(5);
	for ( i = 1; i >= 6; i++)
	{
		CCString* filename6 = CCString::createWithFormat("turistad_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame6 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename6->getCString());
		SpriteFrames6.pushBack(frame6);
	}

	CCAnimation* runAnim6 = CCAnimation::createWithSpriteFrames(SpriteFrames6,1);
	Frame6 = CCSprite::createWithSpriteFrameName("turistad_01.png");

	Frame6->setPosition(spritesheet2->getPosition());
	Frame6->setScale(0.5);
    spritesheet2->setVisible(false);
	CCAction* action6 = CCRepeatForever::create(CCAnimate::create(runAnim6));
    
	Frame6->runAction(action6);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet6->addChild(Frame6,3);	
	
	
	}
	if (Zombi_perder_int == 0)
	{
		Zombillegado(this);
	}
}

void PlayScene6::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
			
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		 if (!alegre)
		 {
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		 }
		 else
		 	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		this->unschedule( schedule_selector(PlayScene6::UpdateTimer));
		//menuCloseCallback(this);
		 gotoCountry(this);
		
	}
}


void PlayScene6::Acabar(float pt)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		this->unscheduleUpdate();
		this->unschedule(schedule_selector(PlayScene6::UpdateTimer));
		this->unschedule( schedule_selector(PlayScene6::Acabar));
		//menuCloseCallback(this);
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");

	   	gotoPantallaWin(this);
	}
}


void PlayScene6::spawnZombi(float dt) {
	 Size visibleSize = Director::getInstance()->getVisibleSize();

	 	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene6/zombies/zombie.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene6/zombies/zombie.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( j = 1; j <= 3; j++)
	{
		CCString* filename2 = CCString::createWithFormat("zombie_0%d.png", j);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename2->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,0.5);
	tempZombi = CCSprite::createWithSpriteFrameName("zombie_01.png");

	//A�adido para los zombies desde el lado derecho.

	frameCache3 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache3->addSpriteFramesWithFile("PlayScene6/zombies/zombie.plist");
    
	spritesheet3 = CCSpriteBatchNode::create("PlayScene6/zombies/zombie.png");
	this->addChild(spritesheet3,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(5);
	for ( l = 4; l <= 6; l++)
	{
		CCString* filename3 = CCString::createWithFormat("zombie_0%d.png", l);//Spritesheet
		CCSpriteFrame* frame3 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename3->getCString());
		SpriteFrames2.pushBack(frame3);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.5);
	
	//Aqui termina el a�adido para los zombies.


	// int asteroidIndex = (std::rand() % 3 + 1);
	// __String *filename = __String::createWithFormat ("GameScreen/Asteroid_%i.png",
	 //asteroidIndex);
 
	 //Sprite *tempZombi = Sprite::create("PlayScene6/Jorge.jpg");
	 tempZombi->setScale(1);
	
	 int posicion = rand()%2;

	 int xRandomPosition = (std::rand() % (int)(visibleSize.width - 
	 tempZombi->getContentSize().width)) + 
	 tempZombi->getContentSize().width / 2;
	
	 int yRandomPosition = (std::rand() % (int)(200 - //Numero a cambiar para acotar salida
	 tempZombi->getContentSize().height)) + 
	 tempZombi->getContentSize().height / 2;

	 switch (posicion)
	 {
		 case 2: // Sale arriba
			  tempZombi->setPosition(Point(xRandomPosition,
			  visibleSize.height + 
			  tempZombi->getContentSize().height));
		 break;

		 case 1: //Derecha
			  tempZombi->setPosition(Point( visibleSize.width ,
			 yRandomPosition));
		 break;

		 case 0: //Izquierda
			   tempZombi->setPosition(Point(-xRandomPosition,
			 0));
		 break;

		 case 3://Abajo
			  tempZombi->setPosition(Point( visibleSize.width + 
			  tempZombi->getContentSize().width,
			- yRandomPosition));
		 break;
	 }
	
	 
	 auto asteroid_body = PhysicsBody::createCircle(tempZombi->getBoundingBox().size.width/2);
	 asteroid_body->setContactTestBitmask(true);
     asteroid_body->setDynamic(true);
     tempZombi->setPhysicsBody(asteroid_body);
 	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));

	 auto *move = (FiniteTimeAction *) MoveTo::create(1.5/ZOMBI_SPEED,Vec2(visibleSize.width/2, visibleSize.height/2));

	 move->retain();

	 auto *seq = Sequence::create(move,NULL);
	// CallFuncN::create(CC_CALLBACK_1(PlayScene6::Zombillegado, this)), 
	// NULL);

	 seq->retain();
	
	_Zombis.push_back(tempZombi);
	 
	 tempZombi->runAction(seq);

	 // Tengo dos animaciones creadas una moviendo hacia la derecha y otra hacia la izquierda,
	 // quiero que cambien una o otra dependendiendo de la direccion de salida del zombie.

	 if (tempZombi->getPosition().x>0)
	 {
		 tempZombi->runAction(action2);
	 }
	 else
	 {
 		tempZombi->runAction(action);
	 }
	//tempZombi->runAction(action2);

	 addChild(tempZombi, 2);


	// int angle = (std::rand() % 360) - 180;
	 //Action *rotate = RepeatForever::create(RotateBy::create(0.5, angle));
 
//	 rotate->retain();
	// tempAsteroid->runAction(rotate);

 }

void PlayScene6::Zombillegado(Node *pSender) {
	

	 gotoCountry(this);

	// _score += 10;
	// if (_score > GameScene::_maxScore
	//	GameScene::_maxScore = _score;
 
	//__String *text = __String::createWithFormat("Score %d MAX %d",
	//_score, GameScene::_maxScore);
	//_labelScore->setString(text->getCString());
}

void PlayScene6::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}

void PlayScene6::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

/*
void PlayScene6::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;

	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  if (Asteroid->boundingBox().containsPoint(location) && Terry->boundingBox().containsPoint(location))
	  {
	    menuCloseCallback(this);
	  }
	       
    }
 }
 */
 /*
void PlayScene6::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  CCLOG("X: %f, Y: %f", location.x, location.y);
	}
}
*/
	


void PlayScene6::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	
	switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_UP_ARROW: 
			_podVector = Vec2(0, POD_STEP_MOVE_ZOMBI );
			_isMoving = true;
			
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW: 
			_podVector = Vec2(0, -POD_STEP_MOVE_ZOMBI );
			_isMoving = true;

			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:	//Animacion hacia izquierda Terry->runAction(action5);
			_podVector = Vec2(-POD_STEP_MOVE_ZOMBI, 0 );
			_isMoving = true;
			terryizq=true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:	//Animacion hacia derecha Terry->runAction(action4);
			_podVector = Vec2(POD_STEP_MOVE_ZOMBI, 0 );
			_isMoving = true;
			terryizq=false;
			break;
		case EventKeyboard::KeyCode::KEY_SPACE:
			navajazo = true;
			break;
			
	}
}


void PlayScene6::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 navajazo = false;

		 _podVector = Vec2::ZERO;
	}
}



void PlayScene6::update(float dt) {
	Size visibleSize = Director::getInstance()->getVisibleSize();

	//cocos2d::Node* _Zombi;
	/*
	
	
		std::list<cocos2d::node*>::iterator iterador;
		for(iterador=_zombis.begin(); iterador!=_zombis.end(); iterador++)
			{
				if (terry->boundingbox().containspoint((*iterador)->getposition()))
				{
					//(*iterador)->setvisible(false);
					//_zombis.remove((*iterador));
					zombi = iterador;
					break;
				}
			}
		
		if (!zombi)
		{
				//this->removechild(zombi);
			zombi->setvisible(false);
		}
		*/
		
		
	
	
	Terry2->setPosition(Terry->getPosition());
	if (terryizq==false)
	{
		Terry->setVisible(true);
		Terry2->setVisible(false);
	}
	else
	{
		Terry->setVisible(false);
		Terry2->setVisible(true);
	}


		
	 if (_isMoving ) {
		 Vec2 newPos = Vec2(Terry->getPosition().x + _podVector.x,
		 Terry->getPosition().y + _podVector.y);
 
	 if (newPos.x >=Terry->getBoundingBox().size.width/2 &&
		 newPos.x <= visibleSize.width - Terry->getBoundingBox().size.width/2 &&
		 newPos.y >=Terry->getBoundingBox().size.height/2 &&
		 newPos.y <= visibleSize.height - Terry->getBoundingBox().size.height/2)
		{
		  Terry->setPosition(newPos);
		}	 	 }
	
}

void PlayScene6::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void PlayScene6::setPhysicsWorld(PhysicsWorld *world) {
 mWorld = world;
 mWorld->setGravity(Vec2::ZERO);
}

void PlayScene6::gotoPantallaWin(Ref* pSender)
{

	auto scene = PantallaWin::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void PlayScene6::gotoGameOver(Ref* pSender)
{

	auto scene = GameOver::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}







