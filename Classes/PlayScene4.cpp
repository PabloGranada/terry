//Juego del rugby

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "GameOver.h"
#include "CountryScene.h"
#include "GameInformation.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;


extern int vidas;
extern bool alegre;
extern bool desafio_activo;
extern int vidas_final;

Scene* PlayScene4::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

    
    // 'layer' is an autorelease object
    auto layer = PlayScene4::create();

	layer->setPhysicsWorld(scene->getPhysicsWorld());

    // add layer as a child to scene
    scene->addChild(layer);
	

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene4::init()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoAmericano.mp3");

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
	glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	_podVector = Vec2::ZERO;
	_isMoving = false;
	alcanzado = false;
	acabado = false;
	//_score = 0;
	 counter = 5 ;
	
		this->scheduleUpdate();

	this->schedule(schedule_selector(PlayScene4::spawnAsteriod), 0.5f);
	this->schedule(schedule_selector(PlayScene4::UpdateTimer),1.0f);
	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene4::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene4::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	// Setting and binding mouse callbacks
	/*
	mouseListener = EventListenerMouse::create();
	mouseListener->onMouseMove = CC_CALLBACK_1(PlayScene4::onMouseMove, this);
 
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);
	*/
	
	

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	// Contact manager
	contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(PlayScene4::onContactBegin, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	/*
	//Loading scrollable background
	 for (int i = 0; i < 2; i++) {
	 _backgroundSpriteArray[i] = Sprite::create("GameScreen/Game_Screen_Background.png");
	 _backgroundSpriteArray[i]->setPosition(Point(visibleSize.width / 2,
	 visibleSize.height * (i + 0.5f)));
	 addChild(_backgroundSpriteArray[i], 0);
	 }
	 */

	
	//Fondo

	Fondo = Sprite::create("PlayScene4/Escenario/FondoDEFINITIVO.png");
	Fondo->setPosition(visibleSize.width/2,visibleSize.height/2);
	Fondo ->setScale(3);
	addChild(Fondo,1);

	 // Loading player sprite
	 _playerSprite = Sprite::create("PlayScene4/Terry/TerryMovArriba.png");
 	 _playerSprite->setPosition(Point(visibleSize.width/2,
	 _playerSprite->getContentSize().height * 0.75));
	 _playerSprite->setScale(2);

	 auto body = PhysicsBody::createCircle(_playerSprite->getBoundingBox().size.width/2);
	 body->setContactTestBitmask(true);
	 body->setDynamic(true);
	 _playerSprite->setPhysicsBody(body);
	 addChild(_playerSprite, 1);

	  __String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(_playerSprite->getPositionX() + 350,
                            _playerSprite->getPositionY()+ 300 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);


	 float playfield_width = visibleSize.width * 2.0; // make the x-boundry 2 times the screen width
     float playfield_height = visibleSize.height * 2.0; // make the y-boundry 2 times the screen height
 
    //note : since bounddries are 2 times the screen size calculate new center point
    //this->runAction(Follow::create(_playerSprite, Rect( Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y).x - playfield_width/2, Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y).y - playfield_height/2 , playfield_width, playfield_height)));
 
	/*
	 auto pauseItem = MenuItemImage::create("GameScreen/Pause_Button.png",
	 "GameScreen/Pause_Button(Click).png",
	 CC_CALLBACK_1(PlayScene4::goToPauseScene, this));
  
	 auto pause = Menu::create(pauseItem, NULL);
 
     pause->alignItemsVerticallyWithPadding(visibleSize.height / 4);
	 pause->alignItemsHorizontallyWithPadding(visibleSize.width / 4);
	 pause -> setPosition(20,visibleSize.height - 20);
	 this->addChild(pause, 1);
	 */
      
	 return true;
}


/*
void PlayScene4::goToPauseScene(Ref *pSender) {

	auto scene = PauseScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/ButtonClick.wav");
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}
*/

/*
void PlayScene4::goToGameOverScene(Ref *pSender) {
		
	auto scene = GameOverScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/ButtonClick.wav");
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene, Color3B(255,0,0)));
}
*/
void PlayScene4::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	
	if (counter == 1)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/FAmericano/SilbidoFinal.mp3");
		this->unschedule( schedule_selector(PlayScene4::spawnAsteriod));
		contactListener->setEnabled(false);
		Pos_alcanzado = _playerSprite->getPosition();
		if (!alcanzado)
		{
			this->removeChild(_playerSprite);
		_playerSprite = Sprite::create("PlayScene4/Terry/TerryWin.png");
		_playerSprite->setPosition(Pos_alcanzado);
		_playerSprite->setScale(2);
		addChild(_playerSprite,1);
		}
		
		acabado = true;
	}

	if (counter==0)
	{
		this->unschedule( schedule_selector(PlayScene4::UpdateTimer));
		//menuCloseCallback(this);
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		  if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
	      CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		  if (alcanzado)
		  {
			  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		  }
		  else
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		gotoCountry(this);
	}
}

void PlayScene4::update(float dt) {
	 

	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	label_timer->setPosition(Vec2(visibleSize.width - 50, visibleSize.height-50));
	/*
	// Scrolls background
	 for (int i = 0; i < 2; i++) {

	 if (_backgroundSpriteArray[i]->getPosition().y < (visibleSize.height* -0.5))
	 _backgroundSpriteArray[i]->setPosition(Point(visibleSize.width/2,
	 visibleSize.height * 1.5f));

		 else

		 _backgroundSpriteArray[i]->setPosition(
		 Point(_backgroundSpriteArray[i]->getPosition().x,
		 _backgroundSpriteArray[i]->getPosition().y -
		 (BACKGROUND_SPPED * visibleSize.height * dt)));	 }	 */	if (!alcanzado && !acabado)
	{
		posicion_mouse = get_cursor_pos();
		_playerSprite->setPosition(posicion_mouse);
	}		/*	 if (_isMoving || _isMovingByMouse) {
		 Vec2 newPos = Vec2(_playerSprite->getPosition().x + _podVector.x,
		 _playerSprite->getPosition().y + _podVector.y);
 
	 if (newPos.x >=_playerSprite->getBoundingBox().size.width/2 &&
		 newPos.x <= visibleSize.width - _playerSprite->getBoundingBox().size.width/2 &&
		 newPos.y >=_playerSprite->getBoundingBox().size.height/2 &&
		 newPos.y <= visibleSize.height - _playerSprite->getBoundingBox().size.height/2)
		{
		  _playerSprite->setPosition(newPos);
		}		_isMovingByMouse = false;	 }	 */}

void PlayScene4::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}

void PlayScene4::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

void PlayScene4::spawnAsteriod(float dt) {
	 Size visibleSize = Director::getInstance()->getVisibleSize();

	//FARAMIR
	//A�adido para el vaquero.
	
	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene4/Jugador/correr.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene4/Jugador/correr.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(5);
	for ( j = 2; j <= 3; j++)
	{
		CCString* filename = CCString::createWithFormat("correr_0%d.png", j);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.5);
	tempAsteroid = CCSprite::createWithSpriteFrameName("correr_02.png");
	
	//tempAsteroid->setPosition(Point((float)posxcowboy,
	//					   visibleSize.height/2 - 75));
	//tempAsteroid->setScale(1.3);
    
	
    
	
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	
	
	//Aqui termina el a�adido para el vaquero.


	 int asteroidIndex = (std::rand() % 3 + 1);
	 __String *filename = __String::createWithFormat ("PlayScene4/Jugador/JugCorrerAbajo.png");
 
	 Sprite *tempAsteroid = Sprite::create(filename->getCString());
 
	 int xRandomPosition = (std::rand() % (int)(visibleSize.width - 
	 tempAsteroid->getContentSize().width)) + 
	 tempAsteroid->getContentSize().width / 2;
 
	 tempAsteroid->setPosition(Point(xRandomPosition,
	 visibleSize.height + 
	 tempAsteroid->getContentSize().height));
	 tempAsteroid->setScale(2);

	 auto asteroid_body = PhysicsBody::createCircle(tempAsteroid->getBoundingBox().size.width/2);
	 asteroid_body->setContactTestBitmask(true);
     asteroid_body->setDynamic(true);
     tempAsteroid->setPhysicsBody(asteroid_body);	 CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
 
	 auto *move = (FiniteTimeAction *) MoveBy::create(2/RUGBY_SPEED,
	 Point(0, - visibleSize.height - 
	 tempAsteroid->getBoundingBox().size.height));

	 move->retain();

	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene4::asteroidDone, this)), 
	 NULL);

	 seq->retain();
	
	 _asteroids.push_back(tempAsteroid);
	 
	 tempAsteroid->runAction(seq);
	 tempAsteroid->runAction(action2);
	 addChild(tempAsteroid, 2);

	 /*
	 int angle = (std::rand() % 360) - 180;
	 Action *rotate = RepeatForever::create(RotateBy::create(0.5, angle));
 
	 rotate->retain();
	 tempAsteroid->runAction(rotate);
	 */

 }bool PlayScene4::onContactBegin(PhysicsContact &contact) {

	 auto nodeA = contact.getShapeA()->getBody()->getNode();
     auto nodeB = contact.getShapeB()->getBody()->getNode();

	Size visibleSize = Director::getInstance()->getVisibleSize();
	//goToPauseScene(this);
	 //CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Crash.wav");
	alcanzado = true;
	/*
	 if (nodeA->getPosition() == _playerSprite->getPosition())
	 {
		 
		// auto Jugador_rugby = Sprite::create("PlayScene4/Jugador/JugCorrerAbajo.png");
		// Jugador_rugby->setPosition(nodeA->getPosition());
		// this->removeChild(nodeA);
		// addChild(Jugador_rugby,0);
		 
		 //_Zombis.remove(nodeB);
		  //nodeB->removeFromParentAndCleanup(true);
	 }

	 else
	 {
		  auto Jugador_rugby = Sprite::create("PlayScene4/Jugador/JugCorrerAbajo.png");
		 Jugador_rugby->setPosition(nodeA->getPosition());
		 this->removeChild(nodeA);
		 addChild(Jugador_rugby,0);
	 }
	 */


	//AQUI PONER CODIGO PARA QUE SE QUEDE TONTO AL CHOCAR
	Pos_alcanzado = _playerSprite->getPosition();
	this->removeChild(_playerSprite);
	 _playerSprite = Sprite::create("PlayScene4/Terry/TerryLose.png");
 	 _playerSprite->setPosition(Pos_alcanzado);
	 _playerSprite->setScale(2);

	
	 addChild(_playerSprite, 1);
	//mouseListener->setEnabled(false);
	 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/FAmericano/TirarseEncimaTerry.mp3");
	vidas --;
	vidas_final --;
	alegre = false;
	return true;
}void PlayScene4::asteroidDone(Node *pSender) {
	 pSender->stopAllActions();
	 _asteroids.remove(pSender);
	 removeChild(pSender);

	 /*
	 _score += 10;
	 if (_score > PlayScene4::_maxScore)
		PlayScene4::_maxScore = _score;
 
	__String *text = __String::createWithFormat("Score %d MAX %d",
	_score, PlayScene4::_maxScore);
	_labelScore->setString(text->getCString());	*/
}void PlayScene4::playerDone(Node *pSender) {
	 pSender->stopAllActions();
	
}void PlayScene4::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_podVector = Vec2(0, POD_STEP_MOVE_RUGBY );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_podVector = Vec2(0, -POD_STEP_MOVE_RUGBY );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_podVector = Vec2(-POD_STEP_MOVE_RUGBY, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_podVector = Vec2(POD_STEP_MOVE_RUGBY, 0 );
			_isMoving = true;
			break;
			/*
		case EventKeyboard::KeyCode::KEY_SPACE:
			goToPauseScene(this);
			break;
			*/
			/*
		case EventKeyboard::KeyCode::KEY_M:
			if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

			else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				break;
				*/
	}
}
void PlayScene4::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}/*void PlayScene4::onMouseMove(Event *event) {
	static Vec2 *oldPosition;
	auto *e = dynamic_cast<EventMouse *>(event) ;
	if (oldPosition == NULL) {
		oldPosition = new Vec2(e->getCursorX(), e->getCursorY());
		} else {
			_podVector = Vec2(e->getCursorX() - oldPosition->x,
			e->getCursorY() - oldPosition->y);
 
	 if (!_isMovingByMouse) {
		_isMovingByMouse = true;
		oldPosition->x = e->getCursorX();
		oldPosition->y = e->getCursorY();
		}
	}
}*/void  PlayScene4::setPhysicsWorld(PhysicsWorld *world) {
		mWorld = world;
		mWorld->setGravity(Vec2::ZERO);
	}cocos2d::Point PlayScene4::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}



