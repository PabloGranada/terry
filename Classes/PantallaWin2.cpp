#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "SelectCountryScene.h"
#include "PlayScene6.h"
#include "CountryScene.h"
#include "PantallaWin2.h"
#include "GameInformation.h"

USING_NS_CC;

Scene* PantallaWin2::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PantallaWin2::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PantallaWin2::init()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter = 6;
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Win/FondoWin.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Win/MisionCumplida.mp3");

	Fondo = Sprite::create("PantallaWin2/FondoJapon.png");
	Fondo->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2));
	Fondo->setScaleY(2.33);
	Fondo->setScaleX(2.4);
	addChild(Fondo,0);

	Terry = Sprite::create("PantallaWin2/TerryFinNivel.png");

	Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height + 20));
    Terry->setScale(2.5);
	this->addChild(Terry,0);

	//__String *text = __String::createWithFormat(" %d",
    //counter );
	//label_timer = LabelTTF::create(text->getCString(), "Arial", 35);

	// label_timer->setPosition(Vec2(visibleSize.width - 50,
//                            visibleSize.height - 50 ));
	
	//label_timer->setColor(Gold);
    // add the label as a child to this layer
   // addChild(label_timer, 3);

	Cartel = Sprite::create("PantallaWin2/JapanComplete.png");
	Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
	//Cartel ->setScale(2);
	this->addChild(Cartel,0);


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
	this->schedule(schedule_selector(PantallaWin2::UpdateTimer),1.0f);
	return true ;
}   

void PantallaWin2::UpdateTimer(float ct)
{

	//	__String *text = __String::createWithFormat("%d ",
	//counter);
	//label_timer->setString(text->getCString());
	counter --;
	if (counter == 0)
	{
		LanzarAvion();
		
	}

}

void PantallaWin2::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void PantallaWin2::LanzarAvion()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	this->removeChild(Terry);
	auto *move = (FiniteTimeAction *) MoveTo::create(2/RUGBY_SPEED,Vec2(visibleSize.width + 100,visibleSize.height - 100));
	move->retain();
	//auto *seq = Sequence::create(move);
	auto *seq = Sequence::create(move,
	CallFuncN::create(CC_CALLBACK_1(PantallaWin2::gotoSelectCountry, this)), 
	NULL);
	seq->retain();
	this->removeChild(Terry);
	Terry = Sprite::create("PantallaWin2/TerryAvionDerecha.png");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
	Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height + 20));
	Terry->setScale(1.5);
	Terry->runAction(seq);
 	addChild(Terry, 2);
}
