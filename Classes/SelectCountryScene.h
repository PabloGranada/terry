#ifndef __SELECT_COUNTRYSCENE_H__
#define __SELECT_COUNTRYSCENE_H__

#include "cocos2d.h"

USING_NS_CC;



class SelectCountryScene : public cocos2d::Layer
{
public:

	bool UsaPulsado;
	int contador ;
	char Pais;
	Sprite *Barra;
	Sprite *Letrero;
	Sprite *Letrero2;
	Sprite *Letrero3;
	Sprite *Terry;
	Sprite *BanderaUsa;
	Sprite *BanderaSpain;
	Sprite *BanderaJapan;
	Sprite *SpriteVacio;
	Sprite *Paisbloqueado1;
	Sprite *Paisbloqueado2;
	Point posicion_mouse;

	Vec2 PosUsa;
	Vec2 PosSpain;
	Vec2 PosJapan;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	void update(float dt);
	void UpdateTimer(float dt);
	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	void AvionLanzado(Node *pSender);
	void gotoCountry(Ref* pSender);
	void gotoCountry2(Ref* pSender);
	void gotoFinalChallenge(Ref* pSender);
	cocos2d::Point get_cursor_pos();
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(SelectCountryScene);
};

#endif // __MENU_SCENE_H__
