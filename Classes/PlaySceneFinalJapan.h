#ifndef __PLAY_SCENEFINALJAPAN_H__
#define __PLAY_SCENEFINALJAPAN_H__

#include "cocos2d.h"

USING_NS_CC;

const float DISPARO_SPEED_GODZILLA = 0.7f;
const float BOLA_SPEED_GODZILLA = 2.0F;
const float BACKGROUND_SPEED_GODZILLA = 0.3f;
const float POD_STEP_MOVE_GODZILLA = 10;


class PlaySceneFinalJapan : public cocos2d::Layer
{

private: 
	Vec2 _podVector;
	 bool _isMoving,disparo_disponible,disparo_disponible_nave, pasado_shake;
	 int counter_disparo;
	 int counter_naves;
	 int counter_disparo_naves;
	 int counter_godzilla;
	 int godzilla_disparando;
	 int shake;
	 int impactos_godzilla;
	 int fin_nivel;
	 int ganar_nivel;
	 int desvanecerse_disparo;
	 
	 

public:
	const char* Spritesheet;	
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSprite* Frame2;
	//Vec2* charDirector;
	int i;
	PhysicsWorld *mWorld; 
	FiniteTimeAction *move;
	Sequence *seq;
	Size visibleSize;
	Point pos_terry,pos_disparo;
	cocos2d::EventKeyboard::KeyCode _pressedKey;
	Point initial_pos_nave, initial_pos_godzilla;
	Sprite *_backgroundSpriteArray[2];
	Sprite *Nave_enemiga;
	Sprite *Bolazo;
	Sprite *Nave;
	Sprite *Godzilla;
	Sprite *Humo;
	Sprite *Cabeza;
	Sprite *PlaceHolder;
	Sprite *Proyectil;
	PhysicsBody *GodziBody;
	EventListenerPhysicsContact *contactListener;
	

	EventListenerKeyboard *keyboardListener;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();


	void update(float dt);
	void TimerNaves(float ct);
	void TimerDisparo(float ct);
	void TimerGodzilla(float ct);
	void shakeScreen(float dt);
	float rangeRandom( float min, float max);
	bool onContactBegin(PhysicsContact &contact);
	void setPhysicsWorld(PhysicsWorld *world);
	void Disparollegado(Node* pSender);
	void GodzillaAcabado(float ct);
	void Desvanecerse_disparo(float ct);
	void gotoPantallaWin(Ref* pSender);
	void Proceso_disparar(float ct);
	void GanarNivel(float ct);
	void gotoCountry2(Ref* pSender);
	
	void Navellegada(Node* pSender);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	 void menuCloseCallback(cocos2d::Ref* pSender);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlaySceneFinalJapan);
};

#endif // __MENU_SCENE_H__
