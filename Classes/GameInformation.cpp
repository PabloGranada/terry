#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "CountryScene.h"
#include "GameInformation.h"

USING_NS_CC;



Scene* GameInformation::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameInformation::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameInformation::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	return true ;
}   
