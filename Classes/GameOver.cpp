#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "CountryScene.h"
#include "GameOver.h"
#include "GameInformation.h"

USING_NS_CC;



Scene* GameOver::createScene()
{
    // 'scene' is an go object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameOver::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameOver::init()
{

	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GameOverTheme.mp3");
	Size visibleSize = Director::getInstance()->getVisibleSize();
	 Vec2 origin = Director::getInstance()->getVisibleOrigin();

	 auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
	auto Game_Over = Sprite::create("GameOver/GameOver.png");

    // position the sprite on the center of the screen
    Game_Over->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	//Game_Over->setScale(1.5);
    // add the sprite as a child to this layer
    this->addChild(Game_Over, 0);

	auto Exit = MenuItemImage::create(
											"HowToPlayScene/BotonExit.png",
											"HowToPlayScene/BotonExit.png",
											CC_CALLBACK_1(GameOver::menuCloseCallback, this));

	Exit ->setPosition(visibleSize.width/2 - visibleSize.width/4 , visibleSize.height/2 - visibleSize.height/4 + 30);
	Exit->setScale(0.5);

	
	auto Next = MenuItemImage::create(
											"GameOver/botonMenu.png",
											"GameOver/botonMenu.png",
											CC_CALLBACK_1(GameOver::gotoMenu, this));

	Next->setPosition(visibleSize.width/2 + visibleSize.width/4, visibleSize.height/2 - visibleSize.height/4 + 30);
	Next->setScale(0.5);

	auto menu = Menu::create(Exit,Next,NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

	return true ;
}   

void GameOver::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	
}

void GameOver::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}
