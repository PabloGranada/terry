
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "PlayScene6.h"
#include "PlaySceneFinal.h"
#include "PantallaWin2.h"
#include "HowToPlay1.h"
#include "HowToPlay2.h"
#include "HowToPlay3.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "PlayScene5Japan.h"
#include "PlaySceneFinalJapan.h";
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "SelectCountryScene.h"
#include "SimpleAudioEngine.h"
#include "FinalChallenge.h"
#include "GameInformation.h"
#include "PlayScene6.h"
#include "Credits.h"

USING_NS_CC;


int vidas = 2;
int vidas_japon = 2;
int vidas_final = 3;
bool desafio_activo = false;

Scene* MenuScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MenuScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool MenuScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    vidas = 2;
	vidas_japon = 2;
	vidas_final = 3;
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/USA.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/MusicaFinal1Min.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/MusicaFinalExtendida.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/JAPAN.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/SonidoAvion.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/FINALCHALLENGE.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/FINALCHALLENGE.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/GanarMinijuego.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/SeleccionMenu.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/GameOverTheme.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/MainTheme.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/MusicaPaisUSA.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Menu/SeleccionPais.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoAmericano.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoBoxeo.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoOeste.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoZombies.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoSaloon.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoTapper.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoVersion1.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoVersion4.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoCinta.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoMagneto.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoRelax.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoSamurai.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoPanda.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoGodzilla.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoGodzillaPre.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/FAmericano/SilbidoFinal.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/FAmericano/TirarseEncimaTerry.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/CargarBola1.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/CargarBola2.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/CargarBola3.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/ColisionBola.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/LanzarBola.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/Intro1.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/Intro2.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/MagnetoAtaque.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/MagnetoDamage.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/MagnetoSonidoAtaque.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/MagnetoLose.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/MagnetoWin.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/Rayo.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Magneto/Reflector.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/PunchOut/PunyoDebil.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/PunchOut/PunyoFuerte.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Saloon/EnemigoAparece.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Saloon/Shoot.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Tapper/Lanzamiento.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Tapper/Moverse.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Tapper/MusicaFondo.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Tapper/Acierto.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Tapper/Perder.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Win/FondoWin.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Win/MisionCumplida.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Samurai/CorteEspada.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Samurai/GritoYRomperBarril.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Samurai/RomperBarril.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/DanyoGodzilla.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/Destruccion1.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/Destruction2.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/DisparoTerry1.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/DisparoTerry2.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/DisparoTerry3.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/Godzilla/GodzillaLanzaElFuego.mp3");

	
	//if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
	//{
	//	CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	//}

	//else
	//	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

    Size visibleSize = Director::getInstance()->getVisibleSize();
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;
	_isMoving = false;
	_isMovingByMouse = false;
	encima = false;
	_podVector = Vec2::ZERO;

	/*
	mouseListener = EventListenerMouse::create();
    mouseListener->onMouseMove = CC_CALLBACK_1(MenuScene::onMouseMove, this);
	mouseListener->onMouseDown = CC_CALLBACK_1(MenuScene::onMouseDown,this);
 
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);
	*/

	auto listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(MenuScene::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	this->scheduleUpdate();

	
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
	/*
    auto closeItem = MenuItemImage::create(
                                           "MenuScene/CloseNormal.png",
                                           "MenuScene/CloseSelected.png",
                                           CC_CALLBACK_1(MenuScene::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	auto AboutItem = MenuItemImage::create(
											"MenuScene/About_Button.png",
											"MenuScene/About_Button.png",
											CC_CALLBACK_1(MenuScene::gotoAbout, this));

	AboutItem->setPosition(Vec2(visibleSize.width/2,(visibleSize.height/2) -(visibleSize.height/2.5) - 25));

	
	*/
	/*
	auto PlayItem = MenuItemImage::create(
											"MenuScene/Play_Button.png",
											"MenuScene/Play_Button.png",
											CC_CALLBACK_1(MenuScene::gotoCountry, this));

	PlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-50));

	*/
	
	
	auto Play2Item = MenuItemImage::create(
											"PlayScene2/Play2.jpg",
											"PlayScene2/Play2.jpg",
											CC_CALLBACK_1(MenuScene::gotoPlay4, this));
	/*
	Play2Item->setScale(0.1);
	Play2Item->setPosition(Vec2(visibleSize.width/2 + 300,visibleSize.height/4-50));


    // create menu, it's an autorelease object

    auto menu = Menu::create(Play2Item, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	*/
	
		

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
	/*
    auto label = LabelTTF::create("Terry Around The World", "Helvetica", 30);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(origin.x + visibleSize.width/2,
                            origin.y + visibleSize.height - label->getContentSize().height));
	
	label->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label, 1);
	*/

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("MenuScene/PantallaTitulo.png");

    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
	sprite->setScale(1.5);
    // add the sprite as a child to this layer
    this->addChild(sprite, 0);

	auto Titulo = Sprite::create("MenuScene/tituloJuego.png");
	Titulo ->setPosition(Vec2(visibleSize.width/2, visibleSize.height / 2 + visibleSize.height / 3 + 50));
	Titulo ->setScale(3.0);
	this -> addChild(Titulo,0);

    PlayItem = Sprite::create("MenuScene/startGame.png");

	PlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-50));
	this->addChild(PlayItem,0);

	HowToPlayItem = Sprite::create("MenuScene/howtoplay.png");
	HowToPlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-100));
	this->addChild(HowToPlayItem,0);

	SpriteVacio = Sprite::create("PlayScene3/spriteVacio.png");
	//SpriteVacio = Sprite::create("SelectCountryScene/Cursor.png");
	SpriteVacio->setPosition(visibleSize.width/2, visibleSize.height/2);
	//posicion_mouse = get_cursor_pos();
	//SpriteVacio->setPosition(posicion_mouse);
	this->addChild(SpriteVacio,0);
	
	
    
    return true;
}


void MenuScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void MenuScene::gotoAbout(Ref* pSender)
{   
	auto scene = AboutScene::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void MenuScene::gotoPlay4(Ref* pSender)
{   
	 if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

			else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	
	auto scene = FinalChallenge::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void MenuScene::gotoPlay(Ref* pSender)
{
	auto scene = PlayScene::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void MenuScene::gotoHowToPlay1(Ref* pSender)
{
	auto scene = HowToPlay1::createScene();
	//if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			//{
			//	CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			//}

			//else
			//	CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void MenuScene::gotoCountry(Ref* pSender)
{
	auto scene = CountryScene::createScene();
   if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

			else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void MenuScene::update(float dt) 
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	posicion_mouse = get_cursor_pos();
	SpriteVacio->setPosition(posicion_mouse);
	
	
	 
	if (PlayItem->boundingBox().containsPoint(SpriteVacio->getPosition()))
	{
		
		this->removeChild(PlayItem);
		PlayItem = Sprite::create("MenuScene/startGameConTerry.png");
		
		PlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-50));
		this->addChild(PlayItem,0);
		
		encima = true;
	}

	else
	{
		
		this->removeChild(PlayItem);
		PlayItem = Sprite::create("MenuScene/startGame.png");

		PlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-50));
		this->addChild(PlayItem,0);
		encima = false;
	}

	if (HowToPlayItem->boundingBox().containsPoint(SpriteVacio->getPosition()))
	{
		
		this->removeChild(HowToPlayItem);
		HowToPlayItem = Sprite::create("MenuScene/howtoplayConTerry.png");
	
		HowToPlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-100));
		this->addChild(HowToPlayItem,0);
		encima = true;
	}

	else
	{
		
		this->removeChild(HowToPlayItem);
		HowToPlayItem = Sprite::create("MenuScene/howtoplay.png");

		HowToPlayItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-100));
		this->addChild(HowToPlayItem,0);
		encima = false;
	}

	if (encima)
	{
		
	}

}

void MenuScene::onMouseDown(Event *event)
{
	auto *e = dynamic_cast<EventMouse *>(event) ;

	posx = e->getCursorX();
	posy = e->getCursorY();
}

/*
void MenuScene::onMouseMove(Event *event) {
	static Vec2 *oldPosition;
	 
	auto *e = dynamic_cast<EventMouse *>(event) ;
	
	if (oldPosition == NULL) {
		oldPosition = new Vec2(e->getCursorX(), e->getCursorY()); 
		
		} else {
			_podVector = Vec2(e->getCursorX()- oldPosition->x,
			e->getCursorY() - oldPosition->y);
 
	 if (!_isMovingByMouse) {
		_isMovingByMouse = true;
		oldPosition->x = e->getCursorX();
		oldPosition->y = e->getCursorY();
		}
	}
}
*/

cocos2d::Point MenuScene::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}

void MenuScene::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  CCLOG("%f,%f", location.x, location.y);
	  if (PlayItem->boundingBox().containsPoint(location) )
	  {
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
	      gotoSelectCountry(this);
		
	  }

	  else if (HowToPlayItem->boundingBox().containsPoint(location))
	  {
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
		  gotoHowToPlay1(this);
	  }
	       
    }
 }

void MenuScene::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}