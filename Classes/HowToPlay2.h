#ifndef __HOWTO_PLAY2_H__
#define __HOWTO_PLAY2_H__

#include "cocos2d.h"

USING_NS_CC;



class HowToPlay2 : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	Sprite *Fondo;
	Sprite *Exit;
	Sprite *Previous;
	Sprite *Next;
	
	void gotoHowToPlay3(Ref* pSender);
	void gotoHowToPlay1(Ref* pSender);
	 void gotoMenu(Ref* pSender);
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(HowToPlay2);
};

#endif // __MENU_SCENE_H__
