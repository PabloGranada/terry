#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "CountryScene.h"
#include "HowToPlay3.h"
#include "HowToPlay2.h"

USING_NS_CC;

Scene* HowToPlay3::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HowToPlay3::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HowToPlay3::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	Size visibleSize = Director::getInstance()->getVisibleSize();

	Fondo = Sprite::create("HowToPlayScene/HowToPlayPantalla3.png");
	Fondo->setPosition(visibleSize.width/2,visibleSize.height/2);
	this->addChild(Fondo,0);

	auto Exit = MenuItemImage::create(
											"HowToPlayScene/BotonExit.png",
											"HowToPlayScene/BotonExit.png",
											CC_CALLBACK_1(HowToPlay3::gotoMenu, this));

	Exit ->setPosition(visibleSize.width/2 , visibleSize.height/2 - visibleSize.height/4 + 30);
	Exit->setScale(0.5);


	auto Previous = MenuItemImage::create(
											"HowToPlayScene/BotonPrevious.png",
											"HowToPlayScene/BotonPrevious.png",
											CC_CALLBACK_1(HowToPlay3::gotoHowToPlay2, this));

	Previous->setPosition(visibleSize.width/2 + visibleSize.width/4, visibleSize.height/2 - visibleSize.height/4 + 30);
	Previous->setScale(0.5);

	auto menu = Menu::create(Exit,Previous, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);




	return true ;
}   

void HowToPlay3::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	
}

void HowToPlay3::gotoHowToPlay2(Ref* pSender)
{   
	auto scene = HowToPlay2::createScene();
	
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	
}
