#ifndef __COUNTRY_SCENE_H__
#define __COUNTRY_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

const float POD_STEP_MOVE3 = 10;
const int NUMERO_NIVELES = 6.0;

class CountryScene : public cocos2d::Layer
{
public:

	 Vec2 _podVector;
	 int counter;
	 long int num;
	 long int numero_nivel;
	 int minimo,maximo;
	 bool repe;
	 bool generado;
	 bool senyalando;
	 bool displayed;
	 bool numero_repetido; // el booleano que utiliza la funcion con el mismo nombre
	 std::vector< long int > niveles;
	 bool empezado;
	 bool _isMoving;
	 Sprite *Vidas_banderas;
	 Sprite *_playerSprite;
	 Sprite *Terry;
	 Sprite *Cartel;
	 Sprite *Cartel2;
	 Sprite *Cara_terry;
     LabelTTF *label_timer, *label_vidas;
	 unsigned int Fondo_Musica;
	//Evento de toques con el rat�n
	 
	
	 // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
	 void update(float dt);
	 void UpdateTimer(float ct);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoGameOver(Ref* pSender);
	 void gotoPlay(Ref* pSender);
	 void gotoPlay2(Ref* pSender);
	 void gotoPlay3(Ref* pSender);
	 void gotoPlay4(Ref* pSender);
	 void gotoPlay5(Ref* pSender);
	 void gotoPlay6(Ref* pSender);
	 void gotoPlayFinal(Ref* pSender);
	 void gotoSelectCountry(Ref* pSender);
	 bool repetido(int numero);
	 long int aleatorio(int min, int max,bool repe);
	 //void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);

	// void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	//void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	 //void onTouchesMoved(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	// void onTouchesEnded(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(CountryScene);
};

#endif // __ABOUT_SCENE_H__
