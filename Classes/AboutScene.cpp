#include "MenuScene.h"
#include "AboutScene.h"


USING_NS_CC;

Scene* AboutScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = AboutScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool AboutScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    auto closeItem = MenuItemImage::create(
                                           "MenuScene/CloseNormal.png",
                                           "MenuScene/CloseSelected.png",
                                           CC_CALLBACK_1(AboutScene::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	auto BackItem = MenuItemImage::create(
											"AboutScene/Back_Button.png",
											"AboutScene/Back_Button.png",
											CC_CALLBACK_1(AboutScene::gotoMenu, this));

	BackItem->setScale(0.4);
	//Size final_length = BackItem->getContentSize()*0.3;
	//BackItem->setContentSize(final_length);
	BackItem->setPosition(BackItem->getContentSize().width/5,BackItem->getContentSize().height/2-60);
	
    // create menu, it's an autorelease object

    auto menu = Menu::create(closeItem,BackItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);


	
    auto label = LabelTTF::create("Juego creado por Jorge Varea, ", "Helvetica", 35);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 ));
	
	label->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label, 1);

	auto label2 = LabelTTF::create(" Alejandro Sanz y Pablo Granada", "Helvetica", 35);
    
    // position the label on the center of the screen
    label2->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 - 35 ));
	
	label2->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label2, 1);

    return true;
}

void AboutScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void AboutScene::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
}




