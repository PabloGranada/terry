#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "PlayScene5Japan.h"
#include "PlaySceneFinalJapan.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas_japon ;
extern bool alegre;

Scene* CountryScene2::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = CountryScene2::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool CountryScene2::init()
{
	Fondo_Musica = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
	srand(time(NULL));
	counter = 5;
	generado = false;
	alegre = true;
	empezado = false;
	repe = false;
	senyalando = true;
	displayed = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	 Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	auto FondoJapon = Sprite::create("CountryScene2/FondoJapon.png");

    // position the sprite on the center of the screen
    FondoJapon->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2 ));
	//FondoJapon->setScale(0.85);
	FondoJapon->setScaleX(2.1);
	FondoJapon->setScaleY(2.4);
    // add the sprite as a child to this layer
    this->addChild(FondoJapon, 0);

	Terry = Sprite::create("CountryScene2/TerrySenyalando.png");
	Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
	Terry->setScale(3.0);
	this->addChild(Terry,0);

	

	//__String *text = __String::createWithFormat(" %d",
   // counter );
	// label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
   // label_timer->setPosition(Vec2(visibleSize.width - 50,
    //                        visibleSize.height - 50 ));
	
	//label_timer->setColor(Gold);
    // add the label as a child to this layer
   // addChild(label_timer, 3);

	Vidas_banderas = Sprite::create("CountryScene2/BanderasSeparadas.png");
	Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
	Vidas_banderas->setScale(0.25);
	addChild(Vidas_banderas,1);

	Cartel = Sprite::create("CountryScene/TerrySpriteSheet.png");
	Cartel ->setPosition(2000.0,2000.0);
	Cartel ->setVisible(false);
	this->addChild(Cartel,0);

	this->scheduleUpdate();
	this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	return true ;
}   

void CountryScene2::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;
	//__String *text = __String::createWithFormat("%d ",
	//counter);
	//label_timer->setString(text->getCString());

	if (numero_nivel == -1 && !displayed)
	{
		this->removeChild(Cartel);
		Cartel = Sprite::create("CountryScene2/Destroy.png");
		Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
		Cartel ->setScale(1.5);
		this->addChild(Cartel,0);

		Cartel2 = Sprite::create("CountryScene/TituloMicrojuegoFinal.png");
		Cartel2 ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 200));
		//Cartel2 ->setScale(1.5);
		this->addChild(Cartel2,0);
		//Cartel ->setVisible(false);
		displayed = true;
	}
	
	 if (!generado)
		 {
			numero_nivel = aleatorio(minimo,maximo,repe);
			//if (numero_nivel == -1)
			//{
			//	Cartel->setVisible(false);
			//}
			generado = true;
		  }

	 if (counter == 3)
	{
		senyalando = true;
		
	}

	 
	  if (counter == 1)
	 {
		// if (numero_nivel != -1)
		// {
		//	  Cartel->setVisible(true);
		// }
		
		 switch (numero_nivel)
		{
			case 1:
		
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoMeditar.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 2:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoGenki.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 3:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoSushi.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 4:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/Cut.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 5:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/Climb.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
				
		}
	 }
	  

	if (counter==0)
	{
		Cartel->setVisible(false);
		empezado = true;
		generado = false;
		senyalando = false;
		this->unschedule( schedule_selector(CountryScene2::UpdateTimer));

		if (numero_nivel == -1)
		{
			//gotoMenu(this);
			//gotoGameOver(this);
			 alegre = true;
			 Cartel ->setVisible(false);
			 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
			 gotoPlayFinalJapan(this);
			 this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
			// CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
			 counter = 5;
		}

		else
		{
			switch (numero_nivel)
			{
				case 1:

					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay1Japan(this);
					this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 2:

					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay2Japan(this);
					this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 3:
					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay3Japan(this);
					this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 4:
					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay4Japan(this);
					this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 5:
					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay5Japan(this);
					this->schedule(schedule_selector(CountryScene2::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

			}
		}
		
	}
}

void CountryScene2::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

bool CountryScene2::repetido(int numero)
{
	numero_repetido = false;
	for (int i = 0; i < niveles.size(); i++)
	{
		if (numero == niveles[i])
		{
			numero_repetido = true;
			break;
		}
	}

	return numero_repetido;
}

long int CountryScene2::aleatorio(int min, int max,bool repe)
{
	
	if (niveles.size() != NUMERO_NIVELES_JAPON)
	{
		do
		{
			num = rand()% NUMERO_NIVELES_JAPON + 1;
			repe = repetido(num);
		} while (repe != false);
		
		
		niveles.push_back(num);
		return num;
	}
	else
	{
		return -1;
	}
}

void CountryScene2::gotoPlay1Japan(Ref* pSender)
{
	auto scene = PlayScene1Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene2::gotoPlay2Japan(Ref* pSender)
{
	auto scene = PlayScene2Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene2::gotoPlay3Japan(Ref* pSender)
{
	auto scene = PlayScene3Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene2::gotoPlay4Japan(Ref* pSender)
{
	auto scene = PlayScene4Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene2::gotoPlay5Japan(Ref* pSender)
{
	auto scene = PlayScene5Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene2::gotoPlayFinalJapan(Ref* pSender)
{
	auto scene = PlaySceneFinalJapan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}



void CountryScene2::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();

	//Cartel->setVisible(false);
	if (vidas_japon == 0)
		{
			gotoGameOver(this);
	
		}

	else if (vidas_japon == 1)
	{
			this->removeChild(Vidas_banderas);
			Vidas_banderas = Sprite::create("CountryScene2/BanderaJapon.png");
			Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
			Vidas_banderas->setScale(0.25);
			addChild(Vidas_banderas,1);
	}
		
	
	if (alegre && empezado)
	{
		this->removeChild(Terry);
		
		Terry = Sprite::create("CountryScene2/TerryGanando.png");

		Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	else if(!alegre && empezado)
	{
		
		this->removeChild(Terry);
		Terry = Sprite::create("CountryScene2/TerryPerdiendo.png");
		Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
		Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	if (senyalando)
	{
		this->removeChild(Terry);
		 Terry = Sprite::create("CountryScene2/TerrySenyalando.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
		 Terry->setScale(3.0);
		 this->addChild(Terry,0);
	}
		

}

void CountryScene2::gotoGameOver(Ref* pSender)
{   
	auto scene =GameOver::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

