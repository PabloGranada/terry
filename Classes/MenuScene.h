#ifndef __MENU_SCENE_H__
#define __MENU_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

class MenuScene : public cocos2d::Layer
{
public:


	Vec2 _podVector;
	Vec2 _posinicialraton;
	
	Sprite *PlayItem;
	Sprite *HowToPlayItem;
	Sprite *SpriteVacio;
	bool _isMoving, _isMovingByMouse;
	bool encima ;
	float posx, posy;
	EventListenerMouse *mouseListener;
	Point posicion_mouse;
	
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
    void menuCloseCallback(cocos2d::Ref* pSender);
	void gotoAbout(cocos2d::Ref* pSender);
	void update(float dt);
	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	//void onMouseMove(Event *event);
	void onMouseDown(Event *event);
	void gotoHowToPlay1(Ref* pSender);
	void gotoPlay(Ref* pSender);
	void gotoPlay4(Ref* pSender);
	void gotoCountry(Ref* pSender);
	void gotoSelectCountry(Ref* pSender);
	cocos2d::Point get_cursor_pos();
    
    // implement the "static create()" method manually
    CREATE_FUNC(MenuScene);
};

#endif // __MENU_SCENE_H__
