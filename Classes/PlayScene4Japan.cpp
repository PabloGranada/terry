#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "FinalChallenge.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene4Japan::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene4Japan::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene4Japan::init()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	counter = 7;
	counter_espadazo = 4;
	tajado = false;
	eliminado = false;

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoSamurai.mp3");

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene4Japan::keyPressed, this);
		
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(7);
	for ( i = 6; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	//spritesheet->addChild(Frame,3);	

	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene4Japan/Sprites/Samurai/standby.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene4Japan/Sprites/Samurai/standby.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(6);
	for ( i = 1; i <= 6; i++)
	{
		CCString* filename = CCString::createWithFormat("stanby_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.2);
	Samurai = CCSprite::createWithSpriteFrameName("stanby_01.png");

	Samurai->setPosition(Vec2( 200,150));
	Samurai->setScale(1.7);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Samurai->runAction(action2);
	//addChild(Samurai);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Samurai,3);

	


	Fondo = Sprite::create("PlayScene4Japan/FondoSamurai.png");
	Fondo->setPosition(visibleSize.width/2, visibleSize.height/2);
	Fondo ->setScaleX(1.5);
	Fondo->setScaleY(1.375);
	addChild(Fondo,0);
	
	__String *text = __String::createWithFormat(" %d",
    counter_espadazo );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	//label_timer->setColor(Gold);
	//label_timer->setVisible(false);
    // add the label as a child to this layer
    addChild(label_timer, 3);

	Madera = Sprite::create("PlayScene4Japan/Sprites/Barril/BarrilCompleto.png");
	Madera->setPosition(2000,2000);
	addChild(Madera,0);

	//Samurai = Sprite::create("PlayScene4Japan/Sprites/Samurai/stanby_01.png");
	//Samurai->setPosition(Vec2( 200,150));
	//Samurai ->setScale(1.7);
	//addChild(Samurai,0);

	Terry = Sprite::create("PlayScene4Japan/Sprites/Terry/TerryConBarril.png");
	Terry->setPosition(Vec2(visibleSize.width - 200,100));
	Terry->setScale(1.7);
	addChild(Terry,0);
	
	this->schedule(schedule_selector(PlayScene4Japan::UpdateTimer),1.0f);
	
	this->scheduleUpdate();

	return true ;
}   

void PlayScene4Japan::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;

	if (counter == 6)
	{
		this->removeChild(Terry);
		Terry = Sprite::create("PlayScene4Japan/Sprites/Terry/TerrySinBarril.png");
		Terry->setPosition(Vec2(visibleSize.width - 200,100));
		Terry->setScale(1.7);
		addChild(Terry,0);

		this->removeChild(Madera);
		Madera = Sprite::create("PlayScene4Japan/Sprites/Barril/BarrilCompleto.png");
		Madera->setPosition(Terry->getPosition());
		Madera->setScale(1.7);
		//Madera->setScale(0.4);
		move = (FiniteTimeAction *) JumpTo::create(2.0f,
		Point(Samurai->getPositionX(),Samurai->getPositionY()),300,1);
		move->retain();

		seq = Sequence::create(move,
		CallFuncN::create(CC_CALLBACK_1(PlayScene4Japan::Maderallegada, this)), 
		NULL);

		seq->retain();
		 
		Madera->runAction(seq);
 
		addChild(Madera,2);
	}
	
	if (counter==0)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		if (eliminado)
		{
			alegre = true;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");

		}
		else
		{
			alegre = false;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
			vidas_japon--;
			vidas_final--;
		}

		
		if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
		gotoCountry2(this);
		//gotoMenu(this);
	}
}

void PlayScene4Japan::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	__String *text = __String::createWithFormat("%d ",
	counter_espadazo);
	label_timer->setString(text->getCString());

//	this->removeChild(Samurai);
	//Samurai = Sprite::create("PlayScene4Japan/Sprites/Samurai/SamuraiAtaque.png");
	//Samurai->setPosition(Vec2( Samurai->getContentSize().width + 100,Samurai->getContentSize().height/2));
	//addChild(Samurai,0);

	if (Samurai->boundingBox().containsPoint(Madera->getPosition()))
	{
		if (tajado)
		{

			this->removeChild(Terry);
			Terry = Sprite::create("PlayScene4Japan/Sprites/Terry/TerryGanarMinijuego.png");
			Terry->setPosition(Vec2(visibleSize.width - 200,100));
			Terry->setScale(1.7);
			addChild(Terry,0);
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Samurai/GritoYRomperBarril.mp3");
		//	posicion_barril = Madera->getPosition();
			BarrilIzq = Sprite::create("PlayScene4Japan/Sprites/Barril/BarrilRotoDer.png");
			BarrilIzq->setPosition(Vec2(Madera->getPositionX() - 50, Madera->getPositionY()));
			BarrilIzq->setScale(1.7);
			move = (FiniteTimeAction *) MoveTo::create(2.0f,
			Point(BarrilIzq->getPositionX(),30));
			move->retain();

			seq = Sequence::create(move,NULL);
			//CallFuncN::create(CC_CALLBACK_1(PlayScene4Japan::Maderallegada, this)), 
			//NULL);

			seq->retain();
		 
			BarrilIzq->runAction(seq);
			addChild(BarrilIzq,2);
			BarrilDer = Sprite::create("PlayScene4Japan/Sprites/Barril/BarrilRotoIzq.png");
			BarrilDer->setPosition(Vec2(Madera->getPositionX() + 50, Madera->getPositionY()));
			BarrilDer->setScale(1.7);
			move = (FiniteTimeAction *) MoveTo::create(2.0f,
			Point(BarrilDer->getPositionX(),30));
			move->retain();

			seq = Sequence::create(move,NULL);
			//CallFuncN::create(CC_CALLBACK_1(PlayScene4Japan::Maderallegada, this)), 
			//NULL);

			seq->retain();
		 
			BarrilDer->runAction(seq);
			addChild(BarrilDer,2);
			eliminado = true;
		}
		//Cambiar sprite a ganar
		
		else
		{
			this->removeChild(Terry);
			Terry = Sprite::create("PlayScene4Japan/Sprites/Terry/TerryPerderMinijuego.png");
			Terry->setPosition(Vec2(visibleSize.width - 200,100));
			Terry->setScale(1.7);
			addChild(Terry,0);
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Samurai/RomperBarril.mp3");
			this->removeChild(spritesheet2);
			frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene4Japan/Sprites/Samurai/ostion.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene4Japan/Sprites/Samurai/ostion.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(9);
	for ( i = 1; i <= 9; i++)
	{
		
			CCString* filename = CCString::createWithFormat("ostion_0%d.png", i);//Spritesheet
			CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
			SpriteFrames2.pushBack(frame2);
		
		
		
		
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.5);
	Samurai = CCSprite::createWithSpriteFrameName("ostion_01.png");

	Samurai->setPosition(Vec2( 200,150));
	Samurai->setScale(1.7);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Samurai->runAction(action2);
	//addChild(Samurai);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Samurai,3);

		}
		this->removeChild(Madera);
		this->unscheduleUpdate();
	}
	
}

void PlayScene4Japan::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

void PlayScene4Japan::Maderallegada(Node *pSender) {	pSender->stopAllActions();	/*	if (tajado)
	{
		//Ganas, cambiar variables y gesto de victoria o algo asi
	}	else	{		//Pierdes, idem que arriba pero al rev�s	}	*/
	 }

void PlayScene4Japan::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;		switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_SPACE:
			tajado = true;
			
			this->removeChild(spritesheet2);
			frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene4Japan/Sprites/Samurai/ataque.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene4Japan/Sprites/Samurai/ataque.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(18);
	for ( i = 1; i <= 18; i++)
	{
		if (i<10)
		{
			CCString* filename = CCString::createWithFormat("ataque_0%d.png", i);//Spritesheet
			CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
			SpriteFrames2.pushBack(frame2);
		}
		
		else
		{
			CCString* filename = CCString::createWithFormat("ataque_%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
		}
		
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.1);
	Samurai = CCSprite::createWithSpriteFrameName("ataque_01.png");

	Samurai->setPosition(Vec2( 200,150));
	Samurai->setScale(1.7);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Samurai->runAction(action2);
	//addChild(Samurai);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Samurai,3);
	this->schedule(schedule_selector(PlayScene4Japan::TimerEspadazo), 1.0f);
			keyboardListener->setEnabled(false);
			break;
	}
}


void PlayScene4Japan::TimerEspadazo(float ct)
{
	counter_espadazo --;
	
	if (counter_espadazo==2)
	{
		this->removeChild(spritesheet2);
		frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene4Japan/Sprites/Samurai/standby.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene4Japan/Sprites/Samurai/standby.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(6);
	for ( i = 1; i <= 6; i++)
	{
		CCString* filename = CCString::createWithFormat("stanby_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.2);
	Samurai = CCSprite::createWithSpriteFrameName("stanby_01.png");

	Samurai->setPosition(Vec2( 200,150));
	Samurai->setScale(1.7);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Samurai->runAction(action2);
	//addChild(Samurai);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Samurai,3);
	}
	
	
}



void PlayScene4Japan::gotoCountry2(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();
}

