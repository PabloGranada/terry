#ifndef __PLAY_SCENE3JAPAN_H__
#define __PLAY_SCENE3JAPAN_H__

#include "cocos2d.h"

USING_NS_CC;

const float CINTA_SPEED = 0.1f;
const float COMIDA_SPEED = 0.4f;


class PlayScene3Japan : public cocos2d::Layer
{

	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	
	CCPoint posicion1;
	CCPoint posicion2;
	CCPoint posicion3;
	CCPoint posicion4;
	//Fin de la declaracion de mierda para la animacion.

	Sprite *_backgroundSpriteArray[2];
	Sprite *SpriteVacio;
	Sprite *Comida_correcta;
	Sprite *Comida_cinta_correcta;
	Sprite *Comida;
	Sprite *Comida2;
	Sprite *Comida3;
	Sprite *Comida4;
	Sprite *Mensaje_Comida;
	
	Sprite *Terry;
	


public:

	int counter,i,min,max;
	int DespIzq;
	int tipo_comida;
	long int num;
	int pos_comida;
	std::vector< long int > comidas;
	bool seleccion_correcta;
	bool repe;
	bool numero_repetido;
	

	LabelTTF *label_timer;
	EventListenerTouchAllAtOnce *listener;
	cocos2d::FiniteTimeAction *move;
	cocos2d::Sequence *seq;
	cocos2d::FiniteTimeAction *move2;
	cocos2d::Sequence *seq2;
	cocos2d::FiniteTimeAction *move3;
	cocos2d::Sequence *seq3;
	cocos2d::FiniteTimeAction *move4;
	cocos2d::Sequence *seq4;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone

	void UpdateTimer(float ct);
	void update(float dt);
	void gotoCountry2(Ref* pSender);
	void Comidallegada(Node* pSender);
    bool repetido(int numero);
	long int aleatorio(int min, int max,bool repe);
	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);

    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene3Japan);
};

#endif // __MENU_SCENE_H__
