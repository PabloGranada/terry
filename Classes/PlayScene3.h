#ifndef __PLAY_SCENE3_H__
#define __PLAY_SCENE3_H__

#include "cocos2d.h"

USING_NS_CC;

const float VELOCIDAD_MEDIDOR = 0.002f;

class PlayScene3 : public cocos2d::Layer
{
public:

	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	//Fin de la declaracion de mierda para la animacion.

	 Vec2 _podVector;
	 int counter,i;
	 bool _isMoving;
	 bool golpe_bueno;
	 bool pulsado;
	 Sprite *Fondo;
	 Sprite *_playerSprite;
	 Sprite *Asteroid;
	 Sprite *Tope;
	 Sprite *Objetivo;
	 Sprite *Mac;
	 Sprite *Boxeador;
	 Sprite *Mac_golpeado;
	 Sprite *Boxeador_golpeado;
	 LabelTTF *label_timer;
	 EventListenerTouchAllAtOnce *listener;
	//Evento de toques con el rat�n
	 
	
	 // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
	 void update(float dt);
	 void UpdateTimer(float ct);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoCountry(Ref* pSender);
	 void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	
	 EventListenerKeyboard *keyboardListener;
	 void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	//void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	 //void onTouchesMoved(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	// void onTouchesEnded(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene3);
};

#endif // __ABOUT_SCENE_H__
