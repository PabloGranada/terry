#ifndef __PLAY_SCENE4_H__
#define __PLAY_SCENE4_H__

#include "cocos2d.h"

USING_NS_CC;

const float RUGBY_SPEED = 1.0f;
const float BACKGROUND_SPPED = 0.3f;
const float POD_STEP_MOVE_RUGBY = 10;


class PlayScene4 : public cocos2d::Layer
{

 private:
	 Vec2 _podVector;
	 bool _isMoving;
	 bool _isMovingByMouse;
	 bool alcanzado;
	 bool acabado;
	 int _score;
	 int counter;
	 CCPoint Pos_alcanzado;

public:
	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;	
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	//Vec2* charDirector;
	int i;

	const char* Spritesheet2;
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSprite* tempAsteroid;
	int j;
	//Fin de la declaracion de mierda para la animacion.

	static int _maxScore;
	Sprite *_backgroundSpriteArray[2];
	std::list<cocos2d::Node *> _asteroids;
	Sprite *_playerSprite;
	Sprite *Fondo;
	PhysicsWorld *mWorld; 
	LabelTTF *_labelScore;
	LabelTTF *label_timer;
	Point posicion_mouse;
	EventListenerKeyboard *keyboardListener;
	EventListenerMouse *mouseListener;
	EventListenerPhysicsContact *contactListener;
	
	
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	void gotoCountry(Ref* pSender);

	 void gotoMenu(Ref* pSender);

	 cocos2d::Point get_cursor_pos();

	void update(float dt);
	
	void setPhysicsWorld (PhysicsWorld *world);
	
	bool onContactBegin(PhysicsContact &contact);

	//void onMouseMove(Event *event);

	void spawnAsteriod(float dt);

	void UpdateTimer(float ct);

	void asteroidDone(Node *pSender);

	void playerDone(Node *pSender);

	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	void goToPauseScene(Ref *pSender);
 
	void goToGameOverScene(Ref *pSender);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene4);
		
};




#endif // __GAME_SCENE_H__
