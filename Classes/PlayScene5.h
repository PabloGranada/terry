#ifndef __PLAY_SCENE5_H__
#define __PLAY_SCENE5_H__

#include "cocos2d.h"

USING_NS_CC;


class PlayScene5 : public cocos2d::Layer
{
public:


	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	//Fin de la declaracion de mierda para la animacion.

	 bool _isMovingByMouse;
	 Vec2 _podVector;
	 int counter, posx,posy,poscowboy,i;
	 Vec2 posicion_cowboy_disparado;
	 bool _isMoving;
	 bool disparado;
	 bool jugador_colocado;
	 Sprite *_playerSprite;
	 Sprite *Asteroid;
	 Sprite *Cowboy_disparado;
	 Sprite *Fondo;
	 Sprite *Ventana_saloon;
	 Sprite *Ventana_saloon2;
	 Sprite *Ventana_saloon3;
	 Sprite *Ventana_saloon4;
	 Sprite *Puerta_saloon;
	 CCPoint posicion1;
	 CCPoint posicion2;
	 CCPoint posicion3;
	 CCPoint posicion4;
     LabelTTF *label_timer;
	 Point posicion_mouse;
	 EventListenerMouse *mouseListener;
	// Point _playerSpritePosition;
	//Evento de toques con el rat�n
	 
	
	 // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
	 void update(float dt);
	 void UpdateTimer(float ct);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoCountry(Ref* pSender);
	 void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
   	void onMouseMove(Event *event);
	cocos2d::Point get_cursor_pos();

	 void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	 //void onTouchesMoved(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	// void onTouchesEnded(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene5);
};

#endif // __ABOUT_SCENE_H__
