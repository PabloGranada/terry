#ifndef __FINAL_CHALLENGE_H__
#define __FINAL_CHALLENGE_H__

#include "cocos2d.h"

USING_NS_CC;



class FinalChallenge : public cocos2d::Layer
{
public:
	
	Vec2 _podVector;
	 int counter;
	 int counter_credits;
	 int NUMERO_NIVELES_FINAL;
	 long int num;
	 int finales;
	 long int numero_nivel;
	 int minimo,maximo;
	 bool repe;
	 bool generado;
	 bool aumentado;
	 bool senyalando;
	 bool displayed;
	 bool un_final_hecho;
	 bool numero_repetido; // el booleano que utiliza la funcion con el mismo nombre
	 std::vector< long int > niveles;
	 bool empezado;
	 bool _isMoving;
	 Sprite *Vidas_banderas;
	 Sprite *_playerSprite;
	
	 Sprite *Terry;
	 Sprite *Cartel;
	 Sprite *Blanco;
	 Sprite *Cartel2;
	 Sprite *Cara_terry;

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  

	 void update(float dt);
	 void UpdateTimer(float ct);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoGameOver(Ref* pSender);
	 void gotoPlay(Ref* pSender);
	 void gotoPlay2(Ref* pSender);
	 void gotoPlay3(Ref* pSender);
	 void gotoPlay4(Ref* pSender);
	 void gotoPlay5(Ref* pSender);
	 void gotoPlay6(Ref* pSender);
	 void gotoPlay1Japan(Ref* pSender);
	 void gotoPlay2Japan(Ref* pSender);
	 void gotoPlay3Japan(Ref* pSender);
	 void gotoPlay4Japan(Ref* pSender);
	 void gotoPlay5Japan(Ref* pSender);
	 void gotoPlayFinalJapan(Ref* pSender);
	 void gotoPlayFinal(Ref* pSender);
	 void gotoSelectCountry(Ref* pSender);
	 void gotoCredits(Ref* pSender);
	 void Transition(float ct);
	 bool repetido(int numero);
	 long int aleatorio(int min, int max,bool repe);
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(FinalChallenge);
};

#endif // __MENU_SCENE_H__
