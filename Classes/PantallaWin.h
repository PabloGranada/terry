#ifndef __PANTALLA_WIN_H__
#define __PANTALLA_WIN_H__

#include "cocos2d.h"

USING_NS_CC;



class PantallaWin : public cocos2d::Layer
{
public:

	int counter;
	Sprite *Terry;
	//Sprite *Avion;
	Sprite *Cartel;
	Sprite *Fondo;
	 LabelTTF *label_timer;
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();


	void PantallaWin::UpdateTimer(float ct);
	void gotoSelectCountry(Ref* pSender);
	void LanzarAvion();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PantallaWin);
};

#endif // __MENU_SCENE_H__
