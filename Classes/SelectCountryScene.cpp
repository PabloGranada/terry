#define COCOS2D_DEBUG 1

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene6.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameInformation.h"
#include "GameOver.h"
#include "SelectCountryScene.h"
#include "SimpleAudioEngine.h"
#include "FinalChallenge.h"

USING_NS_CC;

//Cambiad estos dos bools para ir donde se quiera

bool japon_bloqueado = true;
bool desafio_final_bloqueado = true;
extern int vidas_japon;
extern int vidas_final;


Scene* SelectCountryScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer =SelectCountryScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool SelectCountryScene::init()
{
	auto Fondo_Musica = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionPais.mp3");

	//UsaPulsado = true;
	contador = 3;
	Pais = 'U';
	PosUsa.x = 222.42;
	PosUsa.y = 499.70;
	PosSpain.x = 370;
	PosSpain.y = 600;
	PosJapan.x = 872.35;
	PosJapan.y = 457.77;

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	auto listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(SelectCountryScene::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	this->scheduleUpdate();

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	auto Mapa = Sprite::create("SelectCountryScene/MapaTerryAroundTheWorld.png");

    // position the sprite on the center of the screen
    Mapa->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y + visibleSize.height/10 +1 ));
	Mapa->setScaleY(1.05);
    // add the sprite as a child to this layer
    this->addChild(Mapa, 0);
	
	Barra = Sprite::create("SelectCountryScene/parteInferiorMapaPrincipal.png");
    Barra->setPosition(Vec2(visibleSize.width/2 + origin.x,  Barra->getContentSize().height/2));
	//Mapa->setScale(1.5);
    // add the sprite as a child to this layer
    this->addChild(Barra, 0);

	Letrero = Sprite::create("SelectCountryScene/SeleccionaUnPais.png");
	Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
	this->addChild(Letrero,0);

	Terry = Sprite::create("SelectCountryScene/TerryPensandoPais.png");
	Terry->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY() +  Terry->getContentSize().height + Terry->getContentSize().height/2));
	this->addChild(Terry,0);

	BanderaUsa = Sprite::create("SelectCountryScene/BanderaUSAGrises.png");
	BanderaUsa->setPosition(PosUsa);
	BanderaUsa->setScale(0.05);
	this->addChild(BanderaUsa,0);

	BanderaSpain = Sprite::create("SelectCountryScene/BanderaGrisesDesafioFinal.png");
	BanderaSpain->setPosition(PosSpain);
	BanderaSpain->setScale(0.07);
	BanderaSpain->setVisible(false);
	this->addChild(BanderaSpain,0);

	BanderaJapan = Sprite::create("SelectCountryScene/BanderaJAPONGrises.png");
	BanderaJapan->setPosition(PosJapan);
	BanderaJapan->setScale(0.05);
	this->addChild(BanderaJapan,0);
	
	
	//Paisbloqueado1 = Sprite::create("SelectCountryScene/bloqueado.png");
	//Paisbloqueado1->setPosition(PosSpain);
	//Paisbloqueado1->setScale(0.25);
	//Paisbloqueado1->setVisible(false);
	//this->addChild(Paisbloqueado1,0);

	
	Paisbloqueado2 = Sprite::create("SelectCountryScene/bloqueado.png");
	Paisbloqueado2->setPosition(PosJapan);
	Paisbloqueado2->setScale(0.25);
	Paisbloqueado2->setVisible(false);
	this->addChild(Paisbloqueado2,0);
		

	SpriteVacio = Sprite::create("PlayScene3/spriteVacio.png");
	SpriteVacio->setPosition(visibleSize.width/2, visibleSize.height/2);
	this->addChild(SpriteVacio,0);

	return true ;
}   

void SelectCountryScene::update(float dt) 
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	posicion_mouse = get_cursor_pos();
	SpriteVacio->setPosition(posicion_mouse);

	if (BanderaUsa->boundingBox().containsPoint(SpriteVacio->getPosition()))
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
		this->removeChild(BanderaUsa);
		BanderaUsa = Sprite::create("SelectCountryScene/BanderaUSAColor.png");
		BanderaUsa->setScale(0.05);
		BanderaUsa->setPosition(PosUsa);
		Pais = 'U';
		this->addChild(BanderaUsa,0);
		
	}

	else
	{
		this->removeChild(BanderaUsa);
		BanderaUsa = Sprite::create("SelectCountryScene/BanderaUSAGrises.png");
		BanderaUsa->setScale(0.05);
		BanderaUsa->setPosition(PosUsa);
		this->addChild(BanderaUsa,0);
		
	}
	
	if (BanderaJapan->boundingBox().containsPoint(SpriteVacio->getPosition()) || BanderaSpain->boundingBox().containsPoint(SpriteVacio->getPosition()))
	{
	//	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
		
		if (BanderaSpain->isVisible())
		{
			this->removeChild(Letrero);
			Letrero = Sprite::create("SelectCountryScene/TituloFinalChallenge.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
		}
		
		if (BanderaJapan->boundingBox().containsPoint(SpriteVacio->getPosition()) && japon_bloqueado)
		{
			this->removeChild(BanderaJapan);
			this->removeChild(Letrero);
			Letrero = Sprite::create("SelectCountryScene/JAPAN.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
			BanderaJapan = Sprite::create("SelectCountryScene/BanderaJAPONGrises.png");
			BanderaJapan->setPosition(PosJapan);
			BanderaJapan->setScale(0.05);
			this->addChild(BanderaJapan,0);
			//Paisbloqueado2->setVisible(true);
			this->removeChild(Paisbloqueado2);
			Paisbloqueado2 = Sprite::create("SelectCountryScene/bloqueado.png");
			Paisbloqueado2->setPosition(PosJapan);
			Paisbloqueado2->setScale(0.25);
			//Paisbloqueado2->setVisible(false);
			this->addChild(Paisbloqueado2,0);
			
		}

		else if (BanderaJapan->boundingBox().containsPoint(SpriteVacio->getPosition()) && !japon_bloqueado)
		{
			this->removeChild(BanderaJapan);
			this->removeChild(Letrero);
			Letrero = Sprite::create("SelectCountryScene/JAPAN.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
			BanderaJapan = Sprite::create("SelectCountryScene/BanderaJAPONColor.png");
			BanderaJapan->setPosition(PosJapan);
			BanderaJapan->setScale(0.05);
			this->addChild(BanderaJapan,0);
			if (desafio_final_bloqueado)
			{
				Pais = 'J';
			}

			else
			{
				Pais = 'D';
			}
			
			Paisbloqueado2->setVisible(false);
		}

		else if(!BanderaJapan->boundingBox().containsPoint(SpriteVacio->getPosition()) && BanderaSpain->isVisible() )
		{
			this->removeChild(BanderaJapan);
			this->removeChild(Letrero);
			Letrero = Sprite::create("SelectCountryScene/TituloFinalChallenge.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
			BanderaJapan = Sprite::create("SelectCountryScene/BanderaJAPONGrises.png");
			BanderaJapan->setPosition(PosJapan);
			BanderaJapan->setScale(0.05);
			this->addChild(BanderaJapan,0);
			Paisbloqueado2->setVisible(true);
		}
				

		if (BanderaSpain->boundingBox().containsPoint(SpriteVacio->getPosition()))
		{
			//Paisbloqueado1->setVisible(true);
			Paisbloqueado2->setVisible(false);
		}
			


	}

	else if ((BanderaUsa->boundingBox().containsPoint(SpriteVacio->getPosition())))
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
		this->removeChild(Letrero);
		Letrero = Sprite::create("SelectCountryScene/USA.png");
		Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
		this->addChild(Letrero,0);
	}

	else
	{
		this->removeChild(Letrero);	
		Letrero = Sprite::create("SelectCountryScene/SeleccionaUnPais.png");
		Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
		this->addChild(Letrero,0);
		//Paisbloqueado1->setVisible(false);
		if (japon_bloqueado)
		{
			Paisbloqueado2->setVisible(false);
		}
	
	}

	if (!japon_bloqueado && !desafio_final_bloqueado)
	{
		Pais = 'D';
		BanderaSpain->setVisible(true);
		if (BanderaSpain->boundingBox().containsPoint(SpriteVacio->getPosition()))
		{
			this->removeChild(Letrero);
			this->removeChild(BanderaSpain);
			Letrero = Sprite::create("SelectCountryScene/TituloFinalChallenge.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
			BanderaSpain = Sprite::create("SelectCountryScene/BanderaColorDesafioFinal.png");
			BanderaSpain->setScale(0.07);
			BanderaSpain->setPosition(PosSpain);
			addChild(BanderaSpain);
		}

		else
		{
			this->removeChild(Letrero);
			this->removeChild(BanderaSpain);
			Letrero = Sprite::create("SelectCountryScene/SeleccionaUnPais.png");
			Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
			this->addChild(Letrero,0);
			BanderaSpain = Sprite::create("SelectCountryScene/BanderaGrisesDesafioFinal.png");
			BanderaSpain->setScale(0.07);
			BanderaSpain->setPosition(PosSpain);
			addChild(BanderaSpain);
		}
	}

	
}

void SelectCountryScene::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
		CCPoint location = touch->getLocation();
		if (BanderaUsa->boundingBox().containsPoint(location))
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
			 this->unscheduleUpdate();
			//UsaPulsado = true;
			 auto *move = (FiniteTimeAction *) MoveTo::create(2/RUGBY_SPEED,PosUsa);
		     move->retain();
		     //auto *seq = Sequence::create(move);
		     auto *seq = Sequence::create(move,
		     CallFuncN::create(CC_CALLBACK_1(SelectCountryScene::AvionLanzado, this)), 
		     NULL);
		     seq->retain();
		     this->removeChild(Terry);
		     Terry = Sprite::create("SelectCountryScene/TerryAvionIzquierda.png");
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
	         Terry->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY() +  Terry->getContentSize().height + Terry->getContentSize().height/2));
	         Terry->runAction(seq);
 		     addChild(Terry, 2);
		}

		if (BanderaJapan->boundingBox().containsPoint(location) && !japon_bloqueado && desafio_final_bloqueado)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
			 this->unscheduleUpdate();
			//UsaPulsado = true;
			 auto *move = (FiniteTimeAction *) MoveTo::create(2/RUGBY_SPEED,PosJapan);
		     move->retain();
		     //auto *seq = Sequence::create(move);
		     auto *seq = Sequence::create(move,
		     CallFuncN::create(CC_CALLBACK_1(SelectCountryScene::AvionLanzado, this)), 
		     NULL);
		     seq->retain();
		     this->removeChild(Terry);
		     Terry = Sprite::create("SelectCountryScene/TerryAvionDerecha.png");
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
	         Terry->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY() +  Terry->getContentSize().height + Terry->getContentSize().height/2));
	         Terry->runAction(seq);
 		     addChild(Terry, 2);
		}

		if (BanderaSpain->boundingBox().containsPoint(location) && !japon_bloqueado && !desafio_final_bloqueado)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/SeleccionMenu.wav");
			 this->unscheduleUpdate();
			//UsaPulsado = true;
			 auto *move = (FiniteTimeAction *) MoveTo::create(2/RUGBY_SPEED,PosSpain);
		     move->retain();
		     //auto *seq = Sequence::create(move);
		     auto *seq = Sequence::create(move,
		     CallFuncN::create(CC_CALLBACK_1(SelectCountryScene::AvionLanzado, this)), 
		     NULL);
		     seq->retain();
		     this->removeChild(Terry);
		     Terry = Sprite::create("SelectCountryScene/TerryAvionIzquierda.png");
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
	         Terry->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY() +  Terry->getContentSize().height + Terry->getContentSize().height/2));
	         Terry->runAction(seq);
 		     addChild(Terry, 2);
		}
	}
}

cocos2d::Point SelectCountryScene::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}

void SelectCountryScene::AvionLanzado(Node *pSender) {
	pSender->stopAllActions();
	
	if (Pais == 'U')
	{
		this->removeChild(Letrero);
		Letrero = Sprite::create("SelectCountryScene/USA.png");
		Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
		this->addChild(Letrero,0);

	
		this->removeChild(Terry);
		Terry = Sprite::create("SelectCountryScene/TerryHaLlegado.png");
		//Terry->setScale(0.05);
		Terry->setPosition(PosUsa);
		this->addChild(Terry);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/USA.wav");

		this->schedule(schedule_selector(SelectCountryScene::UpdateTimer),1.0f);
		 //removeChild(pSender);
	}

	else if (Pais == 'J')
	{
		this->removeChild(Letrero);
		Letrero = Sprite::create("SelectCountryScene/JAPAN.png");
		Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
		this->addChild(Letrero,0);

	
		this->removeChild(Terry);
		Terry = Sprite::create("SelectCountryScene/TerryHaLlegado.png");
		//Terry->setScale(0.05);
		Terry->setPosition(PosJapan);
		this->addChild(Terry);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/JAPAN.mp3");

		this->schedule(schedule_selector(SelectCountryScene::UpdateTimer),1.0f);
		 //removeChild(pSender);
	}

	else if (Pais == 'D')
	{
		this->removeChild(Letrero);
		Letrero = Sprite::create("SelectCountryScene/TituloFinalChallenge.png");
		Letrero->setPosition(Vec2(Barra->getPositionX(),Barra->getPositionY()));
		this->addChild(Letrero,0);

	
		this->removeChild(Terry);
		Terry = Sprite::create("SelectCountryScene/TerryHaLlegado.png");
		//Terry->setScale(0.05);
		Terry->setPosition(PosSpain);
		this->addChild(Terry);
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/JAPAN.mp3");

		this->schedule(schedule_selector(SelectCountryScene::UpdateTimer),1.0f);
		 //removeChild(pSender);
	}
	
}

void SelectCountryScene::UpdateTimer(float ct)
{
	contador --;
	if (contador == 0)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		if (Terry->getPosition() == PosJapan)
		{
			gotoCountry2(this);
		}

		else if(Terry->getPosition() == PosUsa)
		{
			gotoCountry(this);
		}

		else
		{
			gotoFinalChallenge(this);
		}
		
	}
}

void SelectCountryScene::gotoCountry(Ref* pSender)
{
	auto scene = CountryScene::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void SelectCountryScene::gotoCountry2(Ref* pSender)
{
	auto scene = CountryScene2::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void SelectCountryScene::gotoFinalChallenge(Ref* pSender)
{
	vidas_final = 3;
	auto scene = FinalChallenge::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}



