#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "PlayScene5Japan.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "FinalChallenge.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene5Japan::createScene()
{
    // 'scene' is an autorelease object
    
	auto scene = Scene::createWithPhysics();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene5Japan::create();
	
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	layer->setPhysicsWorld(scene->getPhysicsWorld());

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene5Japan::init()
{

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoPanda.mp3");

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Size size = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	counter = 7;
	movimiento = -1;
	pos_y = 0;
	alcanzado = false;
	bool cambiado = false;
	colision = false;
		
	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene5Japan::keyPressed, this);
		
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(PlayScene5Japan::onContactBegin, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene5Japan::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	posicion_mouse_original = get_cursor_pos();


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(8);
	for ( i = 8; i >= 1; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	//spritesheet->addChild(Frame,3);	



	Fondo = Sprite::create("PlayScene5Japan/Sprites/FondoPanda.png");
	Fondo->setPosition(visibleSize.width/2,visibleSize.height/2 + 160);  // Fondo->getContentSize().height/4);
	Fondo->setScaleX(1.5);
	addChild(Fondo,0);

	Arbol = Sprite::create("PlayScene5Japan/Sprites/ArbolSinRamas.png");
	Arbol->setPosition(visibleSize.width/2, visibleSize.height/2);
	addChild(Arbol,0);

	Rama1 = Sprite::create("PlayScene5Japan/Sprites/RamaDerecha.png");
	Rama1->setPosition(visibleSize.width/2 + 50, visibleSize.height/2 );
	
/*	auto body = PhysicsBody::createCircle(Rama1->getBoundingBox().size.width/2 - 50);
	body->setContactTestBitmask(true);
	body->setDynamic(true);
	Rama1->setPhysicsBody(body);*/

	addChild(Rama1,0);

	Rama2 = Sprite::create("PlayScene5Japan/Sprites/RamaIzquierda.png");
	Rama2->setPosition(visibleSize.width/2 - 50, visibleSize.height/2 + 1.5*(visibleSize.height/4));
	
/*	body = PhysicsBody::createCircle(Rama2->getBoundingBox().size.width/2 - 50);
	body->setContactTestBitmask(true);
	body->setDynamic(true);
	Rama2->setPhysicsBody(body);*/

	addChild(Rama2,0);

	Rama3 = Sprite::create("PlayScene5Japan/Sprites/RamaDerecha.png");
	Rama3->setPosition(visibleSize.width/2 + 50, visibleSize.height/2 + 3*(visibleSize.height/4));
	
/*	body = PhysicsBody::createCircle(Rama3->getBoundingBox().size.width/2 - 50);
	body->setContactTestBitmask(true);
	body->setDynamic(true);
	Rama3->setPhysicsBody(body);*/

	addChild(Rama3,0);

	
	
	Seguir = Sprite::create("PlayScene3/spriteVacio.png");
	Seguir->setPositionX(visibleSize.width/2);
	Seguir->setPositionY(Seguir->getContentSize().height/2);

	
	move = (FiniteTimeAction *) MoveBy::create(2/KOALA_SPEED,
	Point(0,visibleSize.height - 
	Seguir->getBoundingBox().size.height ));
	move->retain();

	seq = Sequence::create(move,NULL);
	 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
//	NULL);

	seq->retain();
		 
	Seguir->runAction(seq);
	addChild(Seguir,2);

	//FARAMIR
	//PANDA TREPANDO IZQ spritesheet2
	
	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene5Japan/Sprites/Panda/pandatrapando.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene5Japan/Sprites/Panda/pandatrepando.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Koala = CCSprite::createWithSpriteFrameName("PandaTrepando1.png");

	Vector<SpriteFrame*> SpriteFrames2(7);
	for ( i = 1; i <= 7; i++)
	{
		CCString* filename = CCString::createWithFormat("PandaTrepando%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.5);
	

	action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));

	move = (FiniteTimeAction *) MoveBy::create(2/KOALA_SPEED,
	Point(0,visibleSize.height - 
	Koala->getBoundingBox().size.height ));
	move->retain();

	seq = Sequence::create(move,NULL);
	 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
//	NULL);

	seq->retain();

	Koala->setPositionX(visibleSize.width/2 - 60);
	Koala->setPositionY(Koala->getContentSize().height + pos_y);
		 
	Koala->runAction(seq);
	Koala->runAction(action2);
	
	spritesheet2->addChild(Koala,3);	


	//PANDA TREPANDO DER spritesheet3

	frameCache3 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache3->addSpriteFramesWithFile("PlayScene5Japan/Sprites/Panda/pandatrapandoder.plist");
    
	spritesheet3 = CCSpriteBatchNode::create("PlayScene5Japan/Sprites/Panda/pandatrepandoder.png");
	this->addChild(spritesheet3,3); //Lo ponemos en primera plana con el 3.

	KoalaDer = CCSprite::createWithSpriteFrameName("PandaTrepandoDer1.png");


	Vector<SpriteFrame*> SpriteFrames3(7);
	for ( i = 1; i <= 7; i++)
	{
		CCString* filename = CCString::createWithFormat("PandaTrepandoDer%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames3.pushBack(frame2);
	}

	CCAnimation* runAnim3 = CCAnimation::createWithSpriteFrames(SpriteFrames3,0.5);
	action3 = CCRepeatForever::create(CCAnimate::create(runAnim3));

	KoalaDer->setPositionX(visibleSize.width/2 + 40);
	KoalaDer->setPositionY(Koala->getContentSize().height + pos_y);
	KoalaDer->runAction(action3);
	spritesheet3->addChild(KoalaDer,3);

	float playfield_width = size.width * 2.0; // make the x-boundry 2 times the screen width
    float playfield_height = size.height * 2.0; // make the y-boundry 2 times the screen height
 
    //note : since bounddries are 2 times the screen size calculate new center point
    this->runAction(Follow::create(Seguir, Rect( Vec2(size.width/2 + origin.x, size.height/2 + origin.y).x - playfield_width/2, Vec2(size.width/2 + origin.x, size.height/2 + origin.y).y - playfield_height/2 , playfield_width, playfield_height)));
 

	//this->schedule(schedule_selector(PlayScene5Japan::spawnMadera), 1.0f);
	this->schedule(schedule_selector(PlayScene5Japan::UpdateTimer),1.0f);
	this->scheduleUpdate();


	if (posicion_mouse_original.x < visibleSize.width/2)
	{
		Cursor = Sprite::create("PlayScene5Japan/Sprites/RatonParteIzq.png");
				
	}

	else
	{
		Cursor = Sprite::create("PlayScene5Japan/Sprites/RatonParteDer.png");
		
	}

	Cursor->setPosition(posicion_mouse_original);
	addChild(Cursor,0);


	PlaceHolder1 = Sprite::create("PlayScene3/spriteVacio.png");
	PlaceHolder1->setPosition(spritesheet2->getPosition());
	addChild(PlaceHolder1);
	PlaceHolder2 = Sprite::create("PlayScene3/spriteVacio.png");
	PlaceHolder2->setPosition(spritesheet3->getPosition());
	addChild(PlaceHolder2);
	return true ;
}   

void PlayScene5Japan::update(float dt) {
	
	Size visibleSize = Director::getInstance()->getVisibleSize();

	//COMPRUEBO QUE NO HAYAN COLISIONADO PARA NO HACER SETPOSITION SI YA NO EXISTEN KOALA O KOALADER
	
	
		KoalaDer->setPositionY(Koala->getContentSize().height + pos_y);
		Koala->setPositionY(KoalaDer->getContentSize().height + pos_y); 
		PlaceHolder1->setPosition(Koala->getPosition());
		PlaceHolder2->setPosition(KoalaDer->getPosition());

		if (movimiento == -1) 
		{
			KoalaDer->setVisible(false);
			Koala->setVisible(true);
			PlaceHolder2->setVisible(false);
			PlaceHolder1->setVisible(true);

		}

		else if (movimiento == 1)
		{
		
			Koala->setVisible(false);
			KoalaDer->setVisible(true);
			PlaceHolder1->setVisible(false);
			PlaceHolder2->setVisible(true);
		
		}
	
		
		
		
		
	

	
	// COMPROBAMOS LAS COLISIONES CON LAS RAMAS Y SI EL PANDA IZQUIERDO ES VISIBLE
	if (PlaceHolder1->isVisible()){
		if (PlaceHolder1->boundingBox().containsPoint(Rama2->getPosition())){
			
			cambiado = true;
			koala_position = Koala->getPosition();
			this->removeChild(Koala);
			this->removeChild(KoalaDer);
		
			this->removeChild(spritesheet3);
			this->removeChild(spritesheet2);
			Koala2 = Sprite::create("PlayScene5Japan/Sprites/Panda/PandaTrepando2.png");
	
			Koala2->setPosition(koala_position);
			alcanzado = true;
			auto *move = (FiniteTimeAction *) MoveBy::create(0.5/KOALA_SPEED,
			Point(0,-visibleSize.height - 
			Seguir->getBoundingBox().size.height ));
			move->retain();

			seq = Sequence::create(move,NULL);
			 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
		//	NULL);

			seq->retain();
		 
			addChild(Koala2,2);

			Koala2->runAction(seq);
			if (koala_position.x < visibleSize.width/2)
			{
				auto rotateTo = RotateTo::create(1.0f, -90.0f);
				Koala2->runAction(rotateTo);
			}

			else
			{
				auto rotateTo = RotateTo::create(1.0f, 90.0f);
				Koala2->runAction(rotateTo);
			}
			contactListener->setEnabled(false);
			listener->setEnabled(false);
			this->unscheduleUpdate();
		}
	}	// COMPROBAMOS LAS COLISIONES CON LAS RAMAS Y SI EL PANDA DERECHO ES VISIBLE
	else if (PlaceHolder2->isVisible()){

		if (PlaceHolder2->boundingBox().containsPoint(Rama1->getPosition()) || (PlaceHolder2->boundingBox().containsPoint(Rama3->getPosition())) ){
			cambiado = true;
			koala_position = KoalaDer->getPosition();
			this->removeChild(KoalaDer);
			this->removeChild(Koala);
			this->removeChild(spritesheet3);
			this->removeChild(spritesheet2);
			Koala2Der = Sprite::create("PlayScene5Japan/Sprites/Panda/PandaTrepandoDer2.png");
	
			Koala2Der->setPosition(koala_position);
			alcanzado = true;
			auto *move = (FiniteTimeAction *) MoveBy::create(0.5/KOALA_SPEED,
			Point(0,-visibleSize.height - 
			Seguir->getBoundingBox().size.height ));
			move->retain();

			seq = Sequence::create(move,NULL);
			 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
		//	NULL);

			seq->retain();
		 
			addChild(Koala2Der,2);

			Koala2Der->runAction(seq);
			if (koala_position.x < visibleSize.width/2)
			{
				auto rotateTo = RotateTo::create(1.0f, -90.0f);
				Koala2Der->runAction(rotateTo);
			}

			else
			{
				auto rotateTo = RotateTo::create(1.0f, 90.0f);
				Koala2Der->runAction(rotateTo);
			}
			contactListener->setEnabled(false);
			listener->setEnabled(false);
			this->unscheduleUpdate();
		}
	}
	
	pos_y ++;
	
	posicion_mouse = get_cursor_pos();
	posicion_mouse.y += pos_y;
	if (Cursor->getPositionX() < visibleSize.width/2)
	{
		
		this->removeChild(Cursor);
		Cursor = Sprite::create("PlayScene5Japan/Sprites/RatonParteIzq.png");
	}

	else
	{
	
		this->removeChild(Cursor);
		Cursor = Sprite::create("PlayScene5Japan/Sprites/RatonParteDer.png");
	}

	Cursor->setPosition(posicion_mouse);


	if (Cursor->getPositionX() > visibleSize.width)
	{
		Cursor->setPositionX(visibleSize.width);
	}

	

	if (Cursor->getPositionX() < 0)
	{
		Cursor->setPositionX(0);
	}
	
	addChild(Cursor,0);

	
	if (movimiento < -1)
	{
		movimiento = -1;
	}

	else if (movimiento > 1)
	{
		movimiento = 1;
	}
	
	
	
}

void PlayScene5Japan::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	counter--;
	//Frame->setPosition(Seguir->getPositionX() + visibleSize.width /4 + visibleSize.width/5, Seguir->getPositionY() + visibleSize.height/4 + visibleSize.height/6);
	if (counter==   -3)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	//	gotoMenu(this);
		if (!alcanzado)
		{
			alegre = true;	
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
			
		}

		else
		{
			//Cambiar esto si sigue dando problemas, el lugar de asignacion de vidas_japon-- ponerlo arriba;
			alegre = false;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
			vidas_japon--;
			vidas_final--;
		}

		
		if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
		gotoCountry2(this);
	}
}

void PlayScene5Japan::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

void PlayScene5Japan::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;		switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_A:
			movimiento -= 2;
			//tajado = true;
			//this->schedule(schedule_selector(PlayScene4Japan::TimerEspadazo),0.5f);
			//keyboardListener->setEnabled(false);
			break;

		case EventKeyboard::KeyCode::KEY_D:
			movimiento += 2;
			break;
		

	}
}

void PlayScene5Japan::Koalallegado(Node *pSender) {	//pSender->stopAllActions();
}

void PlayScene5Japan::spawnMadera(float dt) {
	 Size visibleSize = Director::getInstance()->getVisibleSize();

	 int asteroidIndex = (std::rand() % 3 + 1);
	 __String *filename = __String::createWithFormat ("PlayScene4Japan/Jorge.jpg");
 
	 Sprite *tempAsteroid = Sprite::create(filename->getCString());
 
	 int xRandomPosition = (std::rand() % (int)(visibleSize.width/2 + visibleSize.width / 4));// - 
	// tempAsteroid->getContentSize().width)) + 
	// tempAsteroid->getContentSize().width / 2;
	 if (xRandomPosition < 300)
	 {
		 xRandomPosition = 300;
	 }
	 tempAsteroid->setPosition(Point(xRandomPosition,
	 visibleSize.height ));
	 tempAsteroid->setScale(0.5);

	 auto asteroid_body = PhysicsBody::createCircle(tempAsteroid->getBoundingBox().size.width/2);
	 asteroid_body->setContactTestBitmask(true);
     asteroid_body->setDynamic(true);
     tempAsteroid->setPhysicsBody(asteroid_body);
 
	 auto *move_madera = (FiniteTimeAction *) MoveBy::create(2/MADERA_SPEED,
	 Point(0, - visibleSize.height - 400));

	 move_madera->retain();

	 auto *seq_madera = Sequence::create(move_madera,NULL);
	 //CallFuncN::create(CC_CALLBACK_1(PlayScene4::asteroidDone, this)), 
	 //NULL);

	 seq_madera->retain();
	
	// _asteroids.push_back(tempAsteroid);
	 
	 tempAsteroid->runAction(seq_madera);
 
	 addChild(tempAsteroid, 2);

	 /*
	 int angle = (std::rand() % 360) - 180;
	 Action *rotate = RepeatForever::create(RotateBy::create(0.5, angle));
 
	 rotate->retain();
	 tempAsteroid->runAction(rotate);
	 */

 }

void  PlayScene5Japan::setPhysicsWorld(PhysicsWorld *world) {
		mWorld = world;
		mWorld->setGravity(Vec2::ZERO);
	}

bool PlayScene5Japan::onContactBegin(PhysicsContact &contact) {
	
	Size visibleSize = Director::getInstance()->getVisibleSize();

	//alcanzado = true;
	//gotoMenu(this);
	if (Koala->isVisible()){

		koala_position = Koala->getPosition();
		this->removeChild(Koala);
		Koala = Sprite::create("PlayScene5Japan/Sprites/Panda/PandaTrepando2.png");
	
		Koala->setPosition(koala_position);
		alcanzado = true;
		auto *move = (FiniteTimeAction *) MoveBy::create(0.5/KOALA_SPEED,
		Point(0,-visibleSize.height - 
		Seguir->getBoundingBox().size.height ));
		move->retain();

		seq = Sequence::create(move,NULL);
		 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
	//	NULL);

		seq->retain();
		 
	
	
		addChild(Koala,2);
		Koala->runAction(seq);
		if (koala_position.x < visibleSize.width/2)
		{
			auto rotateTo = RotateTo::create(1.0f, -90.0f);
			Koala->runAction(rotateTo);
		}

		else
		{
			auto rotateTo = RotateTo::create(1.0f, 90.0f);
			Koala->runAction(rotateTo);
		}
		contactListener->setEnabled(false);
		listener->setEnabled(false);
	}
	else {
		koala_position = KoalaDer->getPosition();
		this->removeChild(KoalaDer);
		KoalaDer = Sprite::create("PlayScene5Japan/Sprites/Panda/PandaTrepandoDer2.png");
	
		KoalaDer->setPosition(koala_position);
		alcanzado = true;
		auto *move = (FiniteTimeAction *) MoveBy::create(0.5/KOALA_SPEED,
		Point(0,-visibleSize.height - 
		Seguir->getBoundingBox().size.height ));
		move->retain();

		seq = Sequence::create(move,NULL);
		 //CallFuncN::create(CC_CALLBACK_1(PlayScene5Japan::Koalallegado, this)), 
	//	NULL);

		seq->retain();
		 
	
	
		addChild(KoalaDer,2);
		KoalaDer->runAction(seq);
		if (koala_position.x < visibleSize.width/2)
		{
			auto rotateTo = RotateTo::create(1.0f, 90.0f);
			KoalaDer->runAction(rotateTo);
		}

		else
		{
			auto rotateTo = RotateTo::create(1.0f, -90.0f);
			KoalaDer->runAction(rotateTo);
		}
		contactListener->setEnabled(false);
		listener->setEnabled(false);
	}
	//this->unscheduleUpdate();
	return true;
}

void PlayScene5Japan::gotoCountry2(Ref* pSender)
{


	//auto scene = CountryScene::createScene();
	
	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();
}

void PlayScene5Japan::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
		//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");
        CCPoint location = touch->getLocation();		if (location.x < visibleSize.width / 2)		{			movimiento -= 2;					}		else		{						movimiento += 2;		}		CCLOG("%f,%f", location.x, location.y);	}}		cocos2d::Point PlayScene5Japan::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}