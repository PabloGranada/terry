#ifndef __PLAY_SCENEFINAL_H__
#define __PLAY_SCENEFINAL_H__

#include "cocos2d.h"

USING_NS_CC;

const float VELOCIDAD_BOLA = 1.3f;

class PlaySceneFinal : public cocos2d::Layer
{
public:

	int temp_principio,contador_reflector,contador_perder,contador_ganar;
	int contador_colision,numero_colisiones;
	bool reflector_disponible,empezado;
	CCSprite *Magneto;
	Sprite *Terry;
	Sprite *Bola;
	Sprite *Reflector;
	Sprite *Barras;
	Sprite *Fondo;
	Sprite *Intro1;
	Sprite *Intro2;
	Sprite *BocadilloReflector;
	Sprite *BocadilloMagneto;
	LabelTTF *label_timer;
	EventListenerKeyboard *keyboardListener;
	//FARAMIR
	//Mierdas para la animacion reloj + vaquero
	const char* Spritesheet;	
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	//Vec2* charDirector;
	int i;

	

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();


	void UpdateTimerPrincipio(float ct);
	void UpdateTimerReflector(float ct);
	void UpdateTimerPerder(float ct);
	void UpdateTimerPerder2(float ct);
	void UpdateTimerPerder3(float ct);
	void UpdateTimerColision_final(float ct);
//	void UpdateTimerPerder(float ct);
	void update(float dt);
	void gotoMenu(Ref* pSender);
	void gotoSelectCountry(Ref* pSender);
	void gotoPantallaWin(Ref* pSender);
	void gotoCountry(Ref* pSender);
	void Colisionado(Node *pSender) ;
	void Colisionado2(Node *pSender) ;
	void Colisionado3(Node *pSender) ;
	void Colisionado_Magneto(Node *pSender) ;
	void Colisionado_Magneto2(Node *pSender) ;
	void Colisionado_Magneto_final(Node *pSender) ;
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlaySceneFinal);
};

#endif // __MENU_SCENE_H__
