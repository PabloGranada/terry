#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene3Japan.h"
#include "FinalChallenge.h"
#include "Credits.h"
#include "CountryScene.h"
#include "GameInformation.h"
#include "PlayScene5Japan.h"

USING_NS_CC;



Scene* Credits::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = Credits::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Credits::init()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/MusicaFinalExtendida.mp3");

	pos_y = 0;
	srand(time(NULL));
	counter = 6000;
	visibleSize = Director::getInstance()->getVisibleSize();
	altura = visibleSize.height - 100;

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	for (int i = 0; i < 5; i++) {
		_backgroundSpriteArray[i] = Sprite::create("Credits/FondoCreditos/CreditosCompletoV2.png");
		_backgroundSpriteArray[i]->setScale(2.2);
		_backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * (i + 0.5f),
		 visibleSize.height/2 ));
		addChild(_backgroundSpriteArray[i], 0);
 }

	Creditos = Sprite::create("Credits/TextoCreditos/Creditos.png");
	//Creditos->setPosition(Vec2(visibleSize.width + 200, altura));
	Creditos->setScale(2);
	addChild(Creditos);
	Jorge = Sprite::create("Credits/Staff/Jorge.png");
	Jorge->setScale(0.4);
	//Jorge->setPosition(Vec2(visibleSize.width + 400, altura));
	addChild(Jorge);
	JorgeCartel = Sprite::create("Credits/TextoCreditos/CreditosJorge.png");
	JorgeCartel->setScale(0.7);
	//JorgeCartel->setPosition(Vec2(visibleSize.width + 600, altura));
	addChild(JorgeCartel);
	Pablo = Sprite::create("Credits/Staff/Pablo.png");
	Pablo->setScaleY(2.2);
	Pablo->setScaleX(1.8);
	//Pablo->setPosition(Vec2(visibleSize.width + 800, altura));
	addChild(Pablo);
	PabloCartel = Sprite::create("Credits/TextoCreditos/CreditosPablo.png");
	PabloCartel->setScale(1.7);
	//PabloCartel->setPosition(Vec2(visibleSize.width + 1000, altura));
	addChild(PabloCartel);
	Alejandro = Sprite::create("Credits/Staff/Alex.png");
	Alejandro->setScale(1.8);
	//Alejandro->setPosition(Vec2(visibleSize.width + 1200, altura));
	addChild(Alejandro);
	AlexCartel = Sprite::create("Credits/TextoCreditos/CreditosAlejandro.png");
	AlexCartel->setScale(1.8);
	//AlexCartel->setPosition(Vec2(visibleSize.width + 1400, altura));
	addChild(AlexCartel);
	GameOver= Sprite::create("Credits/TextoCreditos/GameOver.png");
	GameOver->setScale(1.5);
	//GameOver->setPosition(Vec2(visibleSize.width + 1600, altura));
	addChild(GameOver);
	Thanks= Sprite::create("Credits/TextoCreditos/GraciasJugar.png");
	Thanks ->setScale(1.5);
	Thanks->setPosition(Vec2(visibleSize.width /2, visibleSize.height/2));
	Thanks->setVisible(false);
	addChild(Thanks);



	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("Credits/Avion/avion.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("Credits/Avion/avion.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(4);
	for ( i = 1; i <= 4; i++)
	{
		CCString* filename = CCString::createWithFormat("V2TerryVolando%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.1);
	Frame2 = CCSprite::createWithSpriteFrameName("V2TerryVolando1.png");

	Frame2->setPosition(Point(visibleSize.width /4 - 100, visibleSize.height/4-50));
	Frame2->setScale(1.6);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Frame2->runAction(action2);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Frame2,3);	

	//Avion= Sprite::create("Credits/TerryCreditos/TerryAvion1.png");
	//Avion->setScale(1.6);
	//Avion->setPosition(Vec2(visibleSize.width /4 - 100, visibleSize.height/4-50));
	//addChild(Avion);

	this->scheduleUpdate();
	this->schedule(schedule_selector(Credits::UpdateTimer,1.0f));

	return true ;
}   

void Credits::update(float dt)
{
	pos_y ++;
	for (int i = 0; i < 5; i++) {
		if (_backgroundSpriteArray[i]->getPosition().x < (visibleSize.width* -0.5))
		 _backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * 1.5f,
	     visibleSize.height/2 ));
		 else
			 _backgroundSpriteArray[i]->setPosition(
			 Point(_backgroundSpriteArray[i]->getPosition().x  -
			 (CINTA_SPEED * visibleSize.width * dt),
			 _backgroundSpriteArray[i]->getPosition().y));
	}

	
	Creditos->setPosition(Vec2(visibleSize.width + 200 - pos_y, altura - 20));

	Jorge->setPosition(Vec2(visibleSize.width + 800 - pos_y, altura - 50));
	
	JorgeCartel->setPosition(Vec2(visibleSize.width + 1200 - pos_y, altura - 50));
	
	Pablo->setPosition(Vec2(visibleSize.width + 1600 - pos_y, altura - 50));
	
	PabloCartel->setPosition(Vec2(visibleSize.width + 2000 - pos_y, altura - 50));
	
	Alejandro->setPosition(Vec2(visibleSize.width + 2500 - pos_y, altura - 50));
	
	AlexCartel->setPosition(Vec2(visibleSize.width + 2900 - pos_y, altura - 50));
	
	GameOver->setPosition(Vec2(visibleSize.width + 4000 - pos_y, altura - 50));

	if (GameOver->getPositionX() < visibleSize.width/2)
	{
		GameOver->setPositionX(visibleSize.width/2);
	}
	
	//Thanks->setPosition(Vec2(visibleSize.width + 4100 - pos_y, altura - 50));
	
}

void Credits::UpdateTimer(float ct)
{
	counter --;

	if (counter == 1500)
	{
		auto *move = (FiniteTimeAction *) MoveBy::create(2/KOALA_SPEED,Point(20000,0 ));
		move->retain();

		auto *seq = Sequence::create(move,NULL);
		
		seq->retain();
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
		Frame2->runAction(seq);
	}

	if (counter == 1300)
	{
		Thanks->setVisible(true);
	}

	if (counter == 1100)
	{
		//Director::getInstance()->end();
		this->unscheduleUpdate();
		this->unschedule(schedule_selector(Credits::UpdateTimer));
		gotoMenu(this);
	}

}

void Credits::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void Credits::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	
}
