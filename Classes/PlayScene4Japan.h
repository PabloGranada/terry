#ifndef __PLAYSCENE4_JAPAN_H__
#define __PLAYSCENE4_JAPAN_H__

#include "cocos2d.h"

USING_NS_CC;

const float MADERA_SPEED = 0.4f;

class PlayScene4Japan : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	LabelTTF *label_timer;
	Sprite *Madera;
	Sprite *Samurai;
	Sprite *Fondo;
	Sprite *Terry;
	Sprite *BarrilIzq;
	Sprite *BarrilDer;
	Sprite *PlaceHolder;
	const char* Spritesheet;
	const char* Spritesheet2;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSprite* Frame;
	CCSprite* Frame2;
	Vec2* charDirector;
	cocos2d::FiniteTimeAction *move;
	cocos2d::Sequence *seq;
	EventListenerKeyboard *keyboardListener;
	Point posicion_barril;
	int counter,counter_espadazo, i,j;
	bool tajado;
	bool eliminado;




	void update(float dt);
	void UpdateTimer(float ct);
	void TimerEspadazo(float ct);
	void Maderallegada(Node* pSender);
	void gotoCountry2(Ref* pSender);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void gotoMenu(Ref* pSender);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene4Japan);
};

#endif // __MENU_SCENE_H__
