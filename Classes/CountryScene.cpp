#define COCOS2D_DEBUG 1
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "PlayScene6.h"
#include "PlaySceneFinal.h"
#include "CountryScene.h"
#include "SelectCountryScene.h"
#include "GameOver.h"
#include "GameInformation.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

extern int vidas;

bool alegre = true;

Scene* CountryScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = CountryScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool CountryScene::init()
{
	Fondo_Musica = CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");

	srand(time(NULL));
	_podVector = Vec2::ZERO;
	_isMoving = false;
	counter = 5;
	generado = false;
	alegre = true;
	empezado = false;
	repe = false;
	displayed = false;
	senyalando = true;

	
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	
	auto Fondo = Sprite::create("CountryScene/Estatua-de-la-Libertad-Nueva-York.png");

    // position the sprite on the center of the screen
    Fondo->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2 ));
	Fondo->setScale(0.85);
	Fondo->setScaleX(1.2);
    // add the sprite as a child to this layer
    this->addChild(Fondo, 0);

	Terry = Sprite::create("CountryScene/TerrySenyalando.png");
	Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
	Terry->setScale(3.0);
	this->addChild(Terry,0);

	

	//__String *text = __String::createWithFormat(" %d",
   // counter );
	//label_timer = LabelTTF::create(text->getCString(), "Arial", 35);



    // position the label on the center of the screen
  //  label_timer->setPosition(Vec2(visibleSize.width - 50,
//                            visibleSize.height - 50 ));
	
	//label_timer->setColor(Gold);
    // add the label as a child to this layer
    //addChild(label_timer, 3);

	Vidas_banderas = Sprite::create("CountryScene/banderasSeparadas.png");
	Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
	Vidas_banderas->setScale(0.25);
	addChild(Vidas_banderas,1);

	Cartel = Sprite::create("CountryScene/TerrySpriteSheet.png");
	Cartel ->setPosition(2000.0,2000.0);
	Cartel ->setVisible(false);
	this->addChild(Cartel,0);
	
	
	this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
	this->scheduleUpdate();

	
	//auto keyboardListener = EventListenerKeyboard::create();
	//keyboardListener->onKeyPressed = CC_CALLBACK_2(CountryScene::keyPressed, this);
	//keyboardListener->onKeyReleased = CC_CALLBACK_2(CountryScene::keyReleased, this);
	
	//Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	
	
	return true;
}

void CountryScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void CountryScene::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;
	
	//__String *text = __String::createWithFormat("%d ",
	//counter);
	//label_timer->setString(text->getCString());
	//text = __String::createWithFormat("Vidas: %d ",
	// vidas);
	// label_vidas->setString(text->getCString());
	//addChild(label_vidas,3);

	if (numero_nivel == -1 && !displayed)
	{
		this->removeChild(Cartel);
		Cartel = Sprite::create("CountryScene/TituloMicrojuegoMagneto.png");
		Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
		Cartel ->setScale(1.5);
		this->addChild(Cartel,0);

		Cartel2 = Sprite::create("CountryScene/TituloMicrojuegoFinal.png");
		Cartel2 ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 200));
		//Cartel2 ->setScale(1.5);
		this->addChild(Cartel2,0);
		//Cartel ->setVisible(false);
		displayed = true;
	}

	 if (!generado)
	 {
		numero_nivel = aleatorio(minimo,maximo,repe);
		//if (numero_nivel == -1)
		//{
		//	Cartel->setVisible(false);
		//}
		generado = true;
	 }
	 
	if (counter == 3)
	{
		senyalando = true;
		
	}
	 if (counter == 1)
	 {
		 //if (numero_nivel != -1)
		// {
		//	  Cartel->setVisible(true);
		// }
		
		 switch (numero_nivel)
		{
			case 1:
		
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioSaloon.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 2:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioTapper.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 3:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioPunchOut.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 4:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioRugby.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 5:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioSaloon.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
			case 6:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/Protect.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
		}
	 }


	if (counter==0)
	{
		Cartel->setVisible(false);
		empezado = true;
		generado = false;
		senyalando = false;
		
		this->unschedule( schedule_selector(CountryScene::UpdateTimer));
		
		if (numero_nivel == -1)
		{
			//gotoMenu(this);
			//gotoGameOver(this);
			 alegre = true;
			 Cartel ->setVisible(false);
			 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
			 gotoPlayFinal(this);
			 this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
			// CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
			 counter = 5;
					
		}

		else
		{
			switch (numero_nivel)
			{
			   case 1:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay(this);
				   this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				   counter = 5;
				   break;

			   case 2:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay2(this);
				  this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				//      CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				
				   counter = 5;
				   break;

			   case 3:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay3(this);
				   this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				 //   CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				 
				   counter = 5;
				   break;
			   case 4:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay4(this);
				   this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				   // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				  
				   counter = 5;
				   break;
			   case 5:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay5(this);
				   this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				   //  CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				   
				   counter = 5;
				   break;
			   case 6:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay6(this);
				   this->schedule(schedule_selector(CountryScene::UpdateTimer),1.0f);
				   //  CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				   
				   counter = 5;
				   break;
				   break;

			}
		}
			
	}
}
void CountryScene::gotoPlay(Ref* pSender)
{
	auto scene = PlayScene::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

/*
void CountryScene::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;

	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  if (Asteroid->boundingBox().containsPoint(location) && _playerSprite->boundingBox().containsPoint(location))
	  {
	    menuCloseCallback(this);
	  }
	       
    }
 }
 */
 
/*
void CountryScene::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_podVector = Vec2(0, POD_STEP_MOVE3 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_podVector = Vec2(0, -POD_STEP_MOVE3 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_podVector = Vec2(-POD_STEP_MOVE3, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_podVector = Vec2(POD_STEP_MOVE3, 0 );
			_isMoving = true;
			break;
		
	}
}
*/
/*
void CountryScene::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}
*/
void CountryScene::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();

	//Cartel->setVisible(false);
	if (vidas == 0)
		{
			gotoGameOver(this);
	
		}

	else if (vidas == 1)
	{
			this->removeChild(Vidas_banderas);
			Vidas_banderas = Sprite::create("CountryScene/Bandera1.png");
			Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
			Vidas_banderas->setScale(0.25);
			addChild(Vidas_banderas,1);
	}
		
	
	if (alegre && empezado)
	{
		this->removeChild(Terry);
		
		Terry = Sprite::create("CountryScene/TerryMovAbajo.png");

		Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	else if(!alegre && empezado)
	{
		
		this->removeChild(Terry);
		Terry = Sprite::create("CountryScene/TerryLose.png");
		Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
		Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	if (senyalando)
	{
		this->removeChild(Terry);
		 Terry = Sprite::create("CountryScene/TerrySenyalando.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width/4,Terry->getContentSize().height));
		 Terry->setScale(3.0);
		 this->addChild(Terry,0);
	}
		

}

 void CountryScene::gotoPlay6(Ref* pSender)
{   
	auto scene = PlayScene6::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoPlay2(Ref* pSender)
{   
	auto scene = PlayScene2::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoPlay3(Ref* pSender)
{   
	auto scene = PlayScene3::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoPlay4(Ref* pSender)
{   
	auto scene = PlayScene4::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoPlay5(Ref* pSender)
{   
	auto scene = PlayScene5::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoPlayFinal(Ref* pSender)
{   
	auto scene = PlaySceneFinal::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void CountryScene::gotoGameOver(Ref* pSender)
{   
	auto scene =GameOver::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

bool CountryScene::repetido(int numero)
{
	numero_repetido = false;
	for (int i = 0; i < niveles.size(); i++)
	{
		if (numero == niveles[i])
		{
			numero_repetido = true;
			break;
		}
	}

	return numero_repetido;
}

long int CountryScene::aleatorio(int min, int max,bool repe)
{
	
	if (niveles.size() != NUMERO_NIVELES)
	{
		do
		{
			num = rand()% NUMERO_NIVELES + 1;
			repe = repetido(num);
		} while (repe != false);
		
		
		niveles.push_back(num);
		return num;
	}
	else
	{
		return -1;
	}
}

void CountryScene::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}




