#ifndef __COUNTRY_SCENE2_H__
#define __COUNTRY_SCENE2_H__

#include "cocos2d.h"

USING_NS_CC;

const int NUMERO_NIVELES_JAPON = 5;

class CountryScene2 : public cocos2d::Layer
{
public:

	unsigned int Fondo_Musica;
	int counter;
	int minimo, maximo;
	 long int num;
	 long int numero_nivel;
	  bool numero_repetido; // el booleano que utiliza la funcion con el mismo nombre
	  bool generado;
	  bool repe;
	  bool empezado;
	  bool senyalando;
	  bool displayed;
	 std::vector< long int > niveles;

	  Sprite *Vidas_banderas;
	 Sprite *_playerSprite;
	 Sprite *Terry;
	 Sprite *Cartel, *Cartel2;
	 Sprite *Cara_terry;
     LabelTTF *label_timer, *label_vidas;
	
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	 void update(float dt);
	// void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoGameOver(Ref* pSender);
	 void UpdateTimer(float ct);
	 void gotoMenu(Ref* pSender);
	 void gotoPlay1Japan(Ref* pSender);
	 void gotoPlay2Japan(Ref* pSender);
	 void gotoPlay3Japan(Ref* pSender);
	 void gotoPlay4Japan(Ref* pSender);
	 void gotoPlay5Japan(Ref* pSender);
	 void gotoPlayFinalJapan(Ref* pSender);
	  bool repetido(int numero);
	 long int aleatorio(int min, int max,bool repe);

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(CountryScene2);
};

#endif // __MENU_SCENE_H__
