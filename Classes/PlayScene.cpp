//Juego del disparo

#define COCOS2D_DEBUG 1
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "CountryScene.h"
#include "GameOver.h"
#include "GameInformation.h"
#include "SimpleAudioEngine.h"
#include "FinalChallenge.h"

USING_NS_CC;

extern int vidas;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene::init()
{

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	_podVector = Vec2::ZERO;
	disparado = false;
	_isMoving = false;
	counter = 5;
	posxcowboy = (int) visibleSize.width;
	posxcowboy = rand()%posxcowboy;
	
	
	this->schedule(schedule_selector(PlayScene::UpdateTimer),1.0f);
	this->scheduleUpdate();

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoOeste.mp3");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/EnemigoAparece.mp3");

	auto listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);

	//auto keyboardListener = EventListenerKeyboard::create();
	//keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene::keyPressed, this);
	//keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene::keyReleased, this);
	
	//Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	

	mouseListener = EventListenerMouse::create();
	mouseListener->onMouseMove = CC_CALLBACK_1(PlayScene::onMouseMove, this);
	
 
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
	
	
	//FARAMIR
	//A�adido para el reloj.
	/*
	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 1; i <= 5; i++)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	
	*/

	//Aqui termina el a�adido para el reloj.
	


	Fondo = Sprite::create("PlayScene/Fondo_Duelo.png");
	Fondo ->setPosition(visibleSize.width/2,visibleSize.height/2);
	Fondo ->setScale(0.6);
	addChild(Fondo,1);

	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
	label_timer->setVisible(false);
    // add the label as a child to this layer
    addChild(label_timer, 3);

	/*
	//FARAMIR
	//A�adido para el vaquero.

	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("PlayScene/cowboy.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("PlayScene/cowboy.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(5);
	for ( j = 1; j <= 5; j++)
	{
		CCString* filename = CCString::createWithFormat("Cowboy_%d.png", j);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,1);
	Frame2 = CCSprite::createWithSpriteFrameName("PlayScene/Cowboy_1.png");

	Frame2->setPosition(Point((float)posxcowboy,
						   visibleSize.height/2 - 75));
	Frame2->setScale(1.3);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
	Frame2->runAction(action2);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Frame2,3);	
	*/
	//Aqui termina el a�adido para el vaquero.

	//FARAMIR
	//No te olvides de comentar la seccion de la img del vaquero previa.

	Cowboy = Sprite::create("PlayScene/Cowboy004.png");
 	 Cowboy->setPosition(Point((float)posxcowboy,  visibleSize.height/2 - 75));
	  Cowboy -> setScale(1.3);
	 addChild(Cowboy,1);


	posx = visibleSize.width/2;
	posy = visibleSize.height/2;
	 Objetivo = Sprite::create("PlayScene/Objetivo.png");
	 Objetivo -> setPosition(Point(rand()%posx ,rand()%posy));
	 Objetivo -> setScale(0.3);
 	 //Objetivo->setPosition(Point(visibleSize.width/2,
	 //Objetivo->getContentSize().height * 0.75));

	 addChild(Objetivo, 1);

    

    return true;
}

void PlayScene::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void PlayScene::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		this->unschedule( schedule_selector(PlayScene::UpdateTimer));
			
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		  if (!disparado)
		{
			alegre = false;
			vidas--;
			vidas_final--;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
		}

		else
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		 
		gotoCountry(this);
	}
}

void PlayScene::gotoCountry(Ref* pSender)
{
	Director::getInstance()->popScene();
}


void PlayScene::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}


void PlayScene::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");
	for (auto& touch : touches)
    {
		  
      CCPoint location = touch->getLocation();
	  if (Cowboy->boundingBox().containsPoint(location) && Objetivo->boundingBox().containsPoint(location))
	  {
		
	      disparado = true;
		  this->removeChild(Cowboy,true);
		   Cowboy_disparado = Sprite::create("PlayScene/Cowboy011.png");
		   Cowboy_disparado->setPosition(Point((float)posxcowboy,
						   visibleSize.height/2 - 75));
	       Cowboy_disparado -> setScale(1.3);
	      addChild(Cowboy_disparado, 1);
		  Objetivo->setVisible(false);
	
	  }
	       
    }
 }

/*
void PlayScene::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	
}

void PlayScene::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}
*/

void PlayScene::onMouseMove(Event *event) {
	static Vec2 *oldPosition;
	auto *e = dynamic_cast<EventMouse *>(event) ;
	if (oldPosition == NULL) {
		oldPosition = new Vec2(e->getCursorX(), e->getCursorY());
		} else {
			_podVector = Vec2(e->getCursorX() - oldPosition->x,
			e->getCursorY() - oldPosition->y);
 
	 if (!_isMovingByMouse) {
		_isMovingByMouse = true;
		oldPosition->x = e->getCursorX();
		oldPosition->y = e->getCursorY();
		}
	}
}

void PlayScene::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	posicion_mouse = get_cursor_pos();
	Objetivo->setPosition(posicion_mouse);
	
		
}
cocos2d::Point PlayScene::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}







