#ifndef __PLAY_SCENE6_H__
#define __PLAY_SCENE6_H__

#include "cocos2d.h"

USING_NS_CC;

const float POD_STEP_MOVE_ZOMBI = 10;
const float ZOMBI_SPEED = 0.2f;

class PlayScene6 : public cocos2d::Layer
{
public:

	 PhysicsWorld *mWorld; 
 	//FARAMIR
	//Mierdas para la animacion reloj + vaquero
	const char* Spritesheet;	
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* tempZombi;
	//Vec2* charDirector;
	int i;

	const char* Spritesheet3;	
	CCSpriteFrameCache* frameCache3;
	CCSpriteBatchNode* spritesheet3;
	CCSprite* tempZombi2;
	//Vec2* charDirector;
	int l;

	const char* Spritesheet2;
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSprite* Frame2;
	int j;

	const char* Spritesheet4;
	CCSpriteFrameCache* frameCache4;
	CCSpriteBatchNode* spritesheet4;
	CCSprite* Frame4;
	CCSprite* Terry;
	CCSprite* Terry2;
	CCAction* action4;
	const char* Spritesheet5;
	CCSpriteFrameCache* frameCache5;
	CCSpriteBatchNode* spritesheet5;
	CCSprite* Frame5;
	CCAction* action5;
	const char* Spritesheet6;
	CCSpriteFrameCache* frameCache6;
	CCSpriteBatchNode* spritesheet6;
	CCSprite* Frame6;
	CCAction* action6;
	EventListenerPhysicsContact *contactListener;

	Point pos_anterior;
	//Fin de la declaracion de mierda para la animacion.
	 
	 Vec2 _podVector;
	 int counter;
	 int Zombi_perder_int;
	 bool _isMoving;
	 bool navajazo;
		bool terryizq;
	 bool lanzado;
	 bool colisionado;
	 bool fallado;
	 int pos_vaquero;
	 CCPoint posicion1;
	 CCPoint posicion2;
	 CCPoint posicion3;
	 CCPoint posicion4;
	 CCPoint posicion1_izq;
	 CCPoint posicion2_izq;
	 CCPoint posicion3_izq;
	 CCPoint posicion4_izq;
	 CCPoint Offset;
	 int posicion;
	 int pos_terry;
	 std::list<cocos2d::Node *> _Zombis;
	 
	 Sprite *_playerSprite;
	 Sprite *Asteroid;
	 Node *Zombi;
	// Sprite *Terry;
	 LabelTTF *label_timer;
	 Sprite *Fondo;
	 EventListenerKeyboard *keyboardListener;
	//Evento de toques con el rat�n
	 
	
	 // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
	 void update(float dt);
	 void UpdateTimer(float ct);
	  void Zombi_perder(float ct);
	 void spawnZombi(float dt);
	 void Zombillegado(Node *pSender);
	 bool onContactBegin(PhysicsContact &contact);
	 void setPhysicsWorld(PhysicsWorld *world);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoSelectCountry(Ref* pSender);
	 void gotoCountry(Ref* pSender);
	 void gotoPantallaWin(Ref* pSender);
	 void Acabar(float pt);
	 void gotoGameOver(Ref* pSender);
	// void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);

	 void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	 //void onTouchesMoved(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	// void onTouchesEnded(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene6);
};

#endif // __ABOUT_SCENE_H__
