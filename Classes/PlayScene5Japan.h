#ifndef __PLAY_SCENE5JAPAN_H__
#define __PLAY_SCENE5JAPAN_H__

#include "cocos2d.h"

USING_NS_CC;

const float KOALA_SPEED = 0.4f;

class PlayScene5Japan : public cocos2d::Layer
{
public:

	//FARAMIR
	//Mierdas para la animacion reloj + vaquero
	
	CCSprite* Koala;
	int j;
	Sprite* Koala2;
	Sprite* Koala2Der;
	Sprite *Seguir;
	Sprite *Cursor;
	Sprite *Fondo;
	Sprite *Arbol;
	Sprite *Rama1;
	Sprite *Rama2;
	Sprite *Rama3;
	Sprite *Rama4;
	bool cambiado;
	Sprite *PlaceHolder1;
	Sprite *PlaceHolder2;
	bool colision;
	const char* Spritesheet;
	const char* Spritesheet2;
	CCAction* action2;
	CCAction* action3;
	PhysicsBody* bodyder;
	PhysicsBody* bodyizq;	
	CCSpriteFrameCache* frameCache;
	CCSpriteFrameCache* frameCache3;
	CCSpriteBatchNode* spritesheet;
	CCSpriteBatchNode* koalasheet;
	CCSpriteFrameCache* frameCache2;
	CCSpriteBatchNode* spritesheet2;
	CCSpriteBatchNode* spritesheet3;
	CCSprite* Frame;
	CCSprite* Frame2;
	CCSprite* KoalaDer;
	Vec2* charDirector;
	cocos2d::FiniteTimeAction *move;
	cocos2d::Sequence *seq;
	Point posicion_mouse,posicion_mouse_original,koala_position;
	EventListenerKeyboard *keyboardListener;
	EventListenerPhysicsContact *contactListener;
	EventListenerTouchAllAtOnce *listener;
	PhysicsWorld *mWorld; 
	int counter, i, movimiento,pos_y;
	bool alcanzado;
	
	void update(float dt);
	void UpdateTimer(float ct);
	void Koalallegado(Node* pSender);
	bool onContactBegin(PhysicsContact &contact);
	void gotoCountry2(Ref* pSender);
	void setPhysicsWorld (PhysicsWorld *world);
	void spawnMadera(float dt);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	void gotoMenu(Ref* pSender);
	cocos2d::Point get_cursor_pos();

    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    	
         
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene5Japan);
};

#endif // __MENU_SCENE_H__
