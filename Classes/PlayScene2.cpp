//Juego de la copichuela

#define COCOS2D_DEBUG 1

#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "GameOver.h"
#include "CountryScene.h"
#include "FinalChallenge.h"
#include "GameInformation.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

extern int vidas; 
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene2::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene2::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene2::init()
{	

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoTapper.mp3");
	srand(time(NULL));
	posicion = 1;
	
	posicion1.x = 830.42;
	posicion1.y = 78.41;
	posicion2.x = 775.51;
    posicion2.y = 208.2;
	posicion3.x = 720.6;
	posicion3.y = 340.98;
	posicion4.x = 663.69;
	posicion4.y = 470.76;
	posicion1_izq.x = 141.55;
	posicion1_izq.y = 104.35;
	posicion2_izq.x = 192.47;
	posicion2_izq.y = 238.13;
	posicion3_izq.x = 243.39;
	posicion3_izq.y = 376.9;
	posicion4_izq.x = 288.31;
	posicion4_izq.y = 508.69;
	Offset.x = 21.0;
	Offset.y = 20.0;

	
	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;


	_podVector = Vec2::ZERO;
	_isMoving = false;
	colisionado = false;
	fallado = true;
	counter = 5;
	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);

	this->schedule(schedule_selector(PlayScene2::UpdateTimer),1.0f);
	this->scheduleUpdate();

	auto listener = EventListenerTouchAllAtOnce::create();
	
    listener-> onTouchesBegan= CC_CALLBACK_2(PlayScene2::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener,this);
	

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene2::keyPressed, this);
	//keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene2::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	CCLOG("Pos_Terry: %d", pos_terry);


    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	//A�ado el fondo a la escena

	Fondo = Sprite::create("PlayScene2/FondoTapper.png");
 	Fondo->setPosition(Point(visibleSize.width/2,
	  visibleSize.height /2 ));
    Fondo ->setScaleY(1.4);
	Fondo -> setScaleX(1.9);
	 addChild(Fondo, 1);


	 //A�ado a Tapper
	 _playerSprite = Sprite::create("PlayScene2/personajeTapper/TapperCervezaLlena.png");
 	 _playerSprite->setPosition(posicion1);
	 _playerSprite->setScale(1.2);

	 addChild(_playerSprite, 1);

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

	 //auto Barrera = Sprite::create("PlayScene2/personajeTapper/CervezaTapperGame.png");
	// Barrera ->setPosition(posicion2_izq);
	// addChild(Barrera,1);

	 //A�ado la beer
	Asteroid = Sprite::create("PlayScene2/personajeTapper/CervezaTapperGame.png");
 	 Asteroid->setPosition(Point(2000.0,2000.0));
	 Asteroid->setScale(1.2);
	// Asteroid->setPositionX(posicion1 + 31f);
	 addChild(Asteroid, 1);

	 /*
    auto closeItem = MenuItemImage::create(
                                           "MenuScene/CloseNormal.png",
                                           "MenuScene/CloseSelected.png",
                                           CC_CALLBACK_1(PlayScene2::menuCloseCallback, this));
    
	closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

	auto BackItem = MenuItemImage::create(
											"AboutScene/Back_Button.png",
											"AboutScene/Back_Button.png",
											CC_CALLBACK_1(PlayScene2::gotoMenu, this));

	BackItem->setScale(0.4);
	//Size final_length = BackItem->getContentSize()*0.3;
	//BackItem->setContentSize(final_length);
	BackItem->setPosition(BackItem->getContentSize().width/5,BackItem->getContentSize().height/2-60);
	
    // create menu, it's an autorelease object

    auto menu = Menu::create(closeItem,BackItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);
	*/


	/*
    auto label = LabelTTF::create("Pagina del nivel 2 en construccion ", "Helvetica", 35);
    
    // position the label on the center of the screen
    label->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 ));
	
	label->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label, 1);

	auto label2 = LabelTTF::create(" Por favor, disculpen las molestias", "Helvetica", 35);
    
    // position the label on the center of the screen
    label2->setPosition(Vec2(visibleSize.width/2,
                            visibleSize.height/2 - 35 ));
	
	label2->setColor(Gold);
    // add the label as a child to this layer
    this->addChild(label2, 1);
	*/
	
	 pos_terry = rand()%4  ;

		switch (pos_terry)
			{
			case 0:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion1_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 1:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion2_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 2:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion3_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 3:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion4_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			}

	//Asigno a los vaqueros y a terry
	for (int i = 0; i < 4; i++)
	{
		
			//Codigo meter vaqueros
			switch (i)
			{
			case 0:
				//pos_vaquero = 0;
				Vaquero1 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero1.png");
				Vaquero1->setPosition(posicion1_izq);
				addChild(Vaquero1,1);
			    if (Terry->boundingBox().containsPoint(Vaquero1->getPosition()))
				{
					Vaquero1 ->setPosition(3000.0,3000.0);
					Vaquero1 ->setVisible(false);
					  //this->removeChild(Vaquero1);
	            }
				break;
			case 1:
				//pos_vaquero= 1;
				Vaquero4 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero2.png");
				Vaquero4->setPosition(posicion2_izq);
				addChild(Vaquero4,1);
				  if (Terry->boundingBox().containsPoint(Vaquero4->getPosition()))
				{
					Vaquero4 ->setPosition(3000.0,3000.0);
					Vaquero4 ->setVisible(false);
					 // this->removeChild(Vaquero4);
	            }
				break;
			case 2:
				Vaquero2 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero2.png");
				Vaquero2->setPosition(posicion3_izq);
				addChild(Vaquero2,1);
				  if (Terry->boundingBox().containsPoint(Vaquero2->getPosition()))
				{
					Vaquero2 ->setPosition(3000.0,3000.0);
					Vaquero2 ->setVisible(false);
					  //this->removeChild(Vaquero2);
	            }
				break;
			case 3:
				Vaquero3 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero3.png");
				Vaquero3->setPosition(posicion4_izq);
				addChild(Vaquero3,1);
				  if (Terry->boundingBox().containsPoint(Vaquero3->getPosition()))
				{
					Vaquero3 ->setPosition(3000.0,3000.0);
					Vaquero3 ->setVisible(false);
					  //this->removeChild(Vaquero3);
	            }
				break;
			}

	}

	return true;
      /*
		else
		{
			//Codigo de poner a terry
			switch (pos_terry)
			{
			case 0:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion1_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 1:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion2_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 2:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion3_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			case 3:
				 Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryTapper.png");
 				 Terry->setPosition(posicion4_izq);
				 Terry->setScale(1.5);

				 addChild(Terry, 1);
				break;
			}
		}
		*/
	
}

void PlayScene2::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void PlayScene2::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());
	if (counter==0)
	{
		
		this->unschedule( schedule_selector(PlayScene2::UpdateTimer));
		//menuCloseCallback(this);
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		  if (fallado)
		{
			vidas --;
			vidas_final--;
			alegre = false;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");

		}
		else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		  
	   	gotoCountry(this);
	}
}

void PlayScene2::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}

void PlayScene2::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

/*
void PlayScene2::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;

	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  if (Asteroid->boundingBox().containsPoint(location) && _playerSprite->boundingBox().containsPoint(location))
	  {
	    menuCloseCallback(this);
	  }
	       
    }
 }
 */
 
void PlayScene2::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	for (auto& touch : touches)
    {
      CCPoint location = touch->getLocation();
	  CCLOG("X: %f, Y: %f", location.x, location.y);
	}
}
	


void PlayScene2::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Moverse.mp3");
			if (posicion >= 1 && posicion <5)
			{
				posicion ++;
			}
			CCLOG("%d",posicion);
			
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Moverse.mp3");
			if (posicion >= 1 && posicion <5)
			{
				posicion --;
				
			}
			
			CCLOG("%d",posicion);
			
			break;
		
			
		case EventKeyboard::KeyCode::KEY_SPACE:
			
		 keyboardListener->setEnabled(false);
		 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Lanzamiento.mp3");
		  switch (posicion)
	     {
		 case 1:
			  this->removeChild(_playerSprite);
		 _playerSprite = Sprite::create("PlayScene2/personajeTapper/TapperSoltarBeer.png");
 	     _playerSprite->setPosition(posicion1);
	     _playerSprite->setScale(1.2);

	     addChild(_playerSprite, 1);
			// _playerSprite->setPosition(posicion1);
			  Asteroid->setPosition(posicion1 + Offset);
			 break;

		 case 2:
			  this->removeChild(_playerSprite);
		 _playerSprite = Sprite::create("PlayScene2/personajeTapper/TapperSoltarBeer.png");
 	     _playerSprite->setPosition(posicion2);
	     _playerSprite->setScale(1.2);

	     addChild(_playerSprite, 1);
			 // _playerSprite->setPosition(posicion2);
			 Asteroid->setPosition(posicion2 + Offset);
			  break;

		 case 3:
			  this->removeChild(_playerSprite);
		 _playerSprite = Sprite::create("PlayScene2/personajeTapper/TapperSoltarBeer.png");
 	     _playerSprite->setPosition(posicion3);
	     _playerSprite->setScale(1.2);

	     addChild(_playerSprite, 1);
			 // _playerSprite->setPosition(posicion3);
			   Asteroid->setPosition(posicion3 + Offset);
			  break;

		 case 4:
			  this->removeChild(_playerSprite);
		 _playerSprite = Sprite::create("PlayScene2/personajeTapper/TapperSoltarBeer.png");
 	     _playerSprite->setPosition(posicion4);
	     _playerSprite->setScale(1.2);

	     addChild(_playerSprite, 1);
			 // _playerSprite->setPosition(posicion4);
			  Asteroid->setPosition(posicion4 + Offset);
			  break;
	      }
		  
		  auto *move = (FiniteTimeAction *) MoveBy::create(2/METEOR_SPEED,
	 Point(-(visibleSize.width - 
	 Asteroid ->getBoundingBox().size.width),0 ));

	 move->retain();

	 auto *seq = Sequence::create(move, NULL);

	 seq->retain();
		 
	 Asteroid->runAction(seq);
	
	
	
	}
}


void PlayScene2::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}



void PlayScene2::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	 
	if (colisionado)
	{
		Asteroid->setVisible(false);
	}

	else
	{
		Asteroid->setVisible(true);
	}

	 if (posicion == 0)
	 {
		 posicion = 1;
	 }

	 else if (posicion == 5)
	 {
		 posicion = 4;
	 }

	 switch (posicion)
	 {
		 case 1:
			 _playerSprite->setPosition(posicion1);
			 // Asteroid->setPosition(posicion1 + Offset);
			 break;

		 case 2:
			  _playerSprite->setPosition(posicion2);
			  // Asteroid->setPosition(posicion2 + Offset);
			  break;

		 case 3:
			  _playerSprite->setPosition(posicion3);
			   //Asteroid->setPosition(posicion3 + Offset);
			  break;

		 case 4:
			  _playerSprite->setPosition(posicion4);
			   //Asteroid->setPosition(posicion4 + Offset);
			  break;
	 }
	/*
	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	 if (_isMoving) {
		 Vec2 newPos = Vec2(_playerSprite->getPosition().x + _podVector.x,
		 _playerSprite->getPosition().y + _podVector.y);
 
	 if (newPos.x >=_playerSprite->getBoundingBox().size.width/2 &&
		 newPos.x <= visibleSize.width - _playerSprite->getBoundingBox().size.width/2 &&
		 newPos.y >=_playerSprite->getBoundingBox().size.height/2 &&
		 newPos.y <= visibleSize.height - _playerSprite->getBoundingBox().size.height/2)
		{
		  _playerSprite->setPosition(newPos);
		}	 }
	 */

	  
	if (Asteroid->boundingBox().containsPoint(Terry->getPosition()))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Acierto.mp3");
		fallado = false;
		//Cambiar Sprite Terry y ganar
		this->removeChild(Terry); 
		Terry = Sprite::create("PlayScene2/EnemigosTapper/TerryGanaTapper.png");
		switch (pos_terry)
		{
		case 0:
			 Terry->setPosition(Point(posicion1_izq.x, posicion1_izq.y + 15));
			 Terry->setScale(1.5);

			 addChild(Terry, 1);
			break;
		case 1:
			 Terry->setPosition(Point(posicion2_izq.x, posicion2_izq.y + 15));
			 Terry->setScale(1.5);

			 addChild(Terry, 1);
			break;
		case 2:
			 Terry->setPosition(Point(posicion3_izq.x, posicion3_izq.y + 15));
			 Terry->setScale(1.5);

			 addChild(Terry, 1);
			break;
		case 3:
			 Terry->setPosition(Point(posicion4_izq.x, posicion4_izq.y + 15));
			 Terry->setScale(1.5);

			 addChild(Terry, 1);
			break;
		}
 		//Terry->setPosition(posicion1_izq);
		//Terry->setScale(1.5);

		//addChild(Terry, 1);

		//this->removeChild(Asteroid);
		//Asteroid->setVisible(false);
		colisionado = true;
	}
	
	if (Asteroid->boundingBox().containsPoint(Vaquero1->getPosition()))
	{
		    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Perder.mp3");
			Vec2 posVaquero = Vaquero1 ->getPosition();
		    this->removeChild(Vaquero1);
			Vaquero1 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero1Win.png");
			colisionado = true;
			Vaquero1->setPosition(posVaquero);
			addChild(Vaquero1,1);
			//this->removeChild(Asteroid);
			//Asteroid->setVisible(false);
 			
			
	
	}

	if (Asteroid->boundingBox().containsPoint(Vaquero2->getPosition()))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Perder.mp3");
		Vec2 posVaquero = Vaquero2 ->getPosition();
		//Cambiar Sprite vaqueros, y Terry
		this->removeChild(Vaquero2);
		Vaquero2 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero2Win.png");
		colisionado = true;
		Vaquero2->setPosition(posVaquero);
		addChild(Vaquero2,1);
		//Asteroid->setVisible(false);
		//this->removeChild(Asteroid);
		//Asteroid->setVisible(false);
		
		
	}

	if (Asteroid->boundingBox().containsPoint(Vaquero3->getPosition()))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Perder.mp3");
		Vec2 posVaquero = Vaquero3 ->getPosition();
		//Cambiar Sprite vaqueros, y Terry
		this->removeChild(Vaquero3);
		Vaquero3 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero1Win.png");
		colisionado = true;
		Vaquero3->setPosition(posVaquero);
		addChild(Vaquero3,1);
		//Asteroid->setVisible(false);
		//this->removeChild(Asteroid);
		//Asteroid->setVisible(false);
		
		
	}

	if (Asteroid->boundingBox().containsPoint(Vaquero4->getPosition()))
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Tapper/Perder.mp3");
		Vec2 posVaquero = Vaquero4 ->getPosition();
		//Cambiar Sprite vaqueros, y Terry
		this->removeChild(Vaquero4);
		Vaquero4 = Sprite::create("PlayScene2/EnemigosTapper/Vaquero2Win.png");
		colisionado = true;
		Vaquero4->setPosition(posVaquero);
		addChild(Vaquero4,1);
		//Asteroid->setVisible(false);
		//this->removeChild(Asteroid);
		//Asteroid->setVisible(false);
		
		
	}
	
}








