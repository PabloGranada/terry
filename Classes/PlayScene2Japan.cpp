//Juego de bola genki

#define COCOS2D_DEBUG 1
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene2Japan.h"
#include "CountryScene2.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "FinalChallenge.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene2Japan::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene2Japan::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene2Japan::init()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("audio/Sonidos/DragonBall/CargarBola2.wav");
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoVersion1.mp3");
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/DragonBall/CargarBola2.wav");
			//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/DragonBall/LanzarBola.wav");
	
	counter = 10;
	lanzado = false;
	contador_destello = 1;
	aumento_bola = 0.0;
	escala_inicial = 1.0;
	ganado = false;
	VELOCIDAD = 0.5F;
	pos_abajo_y = 350.37;
	pos_arriba_y = 303.68;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	this->scheduleUpdate();
	this->schedule(schedule_selector(PlayScene2Japan::UpdateTimer),1.0f);
	

	touchListener = EventListenerTouchAllAtOnce::create();
	
    touchListener-> onTouchesBegan= CC_CALLBACK_2(PlayScene2Japan::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener,this);

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene2Japan::keyPressed, this);
	
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,2);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.

	//__String *text = __String::createWithFormat(" %d",
   // counter );
	// label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
   // label_timer->setPosition(Vec2(visibleSize.width - 50,
                   //         visibleSize.height - 50 ));
	
	//label_timer->setColor(Gold);
    // add the label as a child to this layer
   // addChild(label_timer, 3);
	//label_timer->setVisible(false);
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	Fondo = Sprite::create("PlayScene2Japan/FondoMicrojuego.png");
	Fondo ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Fondo -> setScaleX(1.2);
	Fondo ->setScaleY(1.1);
	addChild(Fondo,1);
	
	
	Terry = Sprite::create("PlayScene2Japan/Terry/Terry.png");
	Terry ->setPosition(Vec2(visibleSize.width/2 - visibleSize.width / 4, visibleSize.height/2 - 100));
	Terry -> setScale(0.8);
	addChild(Terry,1);
	
	Freezer = Sprite::create("PlayScene2Japan/Freezer/Normal.png");
	Freezer ->setPosition(Vec2(visibleSize.width/2 + visibleSize.width / 4, Freezer->getContentSize().height / 2 + 50));
	Freezer -> setScale(2.5);
	addChild(Freezer,1);
	

	BolaGenki = Sprite::create("PlayScene2Japan/BolaGenki/BolaGenkiVersion2.png");
	BolaGenki ->setPosition(Vec2(Terry->getPositionX(), Terry->getPositionY() + 300));
	BolaGenki -> setScale(1.2);
	addChild(BolaGenki,1);
	int angle = (std::rand() % 360) - 180;
		
	Action *rotate = RepeatForever::create(RotateBy::create(0.25, angle));
 
	rotate->retain();
	BolaGenki->runAction(rotate);

	/*

	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Terry->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene2Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 Terry->runAction(seq);
	// SpriteVacio = Sprite::create("PlayScene3/spriteVacio.png");
	// SpriteVacio -> setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	// SpriteVacio -> setScale(0.3);
 	 //Objetivo->setPosition(Point(visibleSize.width/2,
	 //Objetivo->getContentSize().height * 0.75));

	// addChild(SpriteVacio, 1);

	 posicion_mouse_original = get_cursor_pos();
*/
	
	return true ;
}   

void PlayScene2Japan::UpdateTimer(float ct)
{
	counter--;
	//__String *text = __String::createWithFormat("%d ",
	//counter);
	//label_timer->setString(text->getCString());
	if (counter == 5)
	{
		lanzado = true;
	}
	else if (counter==0)
	{
		 
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		if (ganado)
		{
			alegre = true;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
			//vidas_japon--;
		}

		else
		{
			alegre = false;
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
			vidas_japon --;
			vidas_final--;
		}

		
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
		this->unschedule( schedule_selector(PlayScene2Japan::UpdateTimer));
	//	 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		gotoCountry2(this);
	}
}

void PlayScene2Japan::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	
	BolaGenki->setScale(escala_inicial + aumento_bola);

	if (lanzado)
	{
		//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/DragonBall/LanzarBola.wav");
		escala_final = BolaGenki->getScale();
		CCLOG("%f",escala_final);
		 auto *move = (FiniteTimeAction *) MoveTo::create(0.5,Vec2(Freezer->getPositionX() - 50,Freezer ->getPositionY() + 50));
 	     move->retain();
	     //auto *seq = Sequence::create(move);
		 auto *seq = Sequence::create(move,
		 CallFuncN::create(CC_CALLBACK_1(PlayScene2Japan::Lanzado, this)), 
		 NULL);
		 seq->retain();
		//  Terry->stopAllActions();
		BolaGenki->runAction(seq);
		
		keyboardListener->setEnabled(false);
		lanzado = false;
	}
	/*
	if (Terry->getPositionY() < visibleSize.height/4 - 50)
	{
		Terry->setPositionY(visibleSize.height/4 - 50);
	}
	if (movido)
	{
		
		 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Terry->getPositionX(),0));
 		 move->retain();
		 //auto *seq = Sequence::create(move);
		 auto *seq = Sequence::create(move,
		 CallFuncN::create(CC_CALLBACK_1(PlayScene2Japan::Movido, this)), 
		 NULL);
		 seq->retain();
		//  Terry->stopAllActions();
		 Terry->runAction(seq);
		 
	}
	else
	{
		posicion_mouse = get_cursor_pos();
	

		if (posicion_mouse != posicion_mouse_original)
		{
			movido = true;
		}
	}
	
	if (Terry->getPositionY() == visibleSize.height / 4 - 50)
	{
		VELOCIDAD = 0;
		Terry->stopAllActions();
	}
	//SpriteVacio->setPosition(posicion_mouse);
		*/
}


void PlayScene2Japan::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
	//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");
	for (auto& touch : touches)
    {
		 CCPoint location = touch->getLocation();
		 CCLOG("Y: %f",location.y );
	     //movido = true;
    }
 }
 
/*
cocos2d::Point PlayScene2Japan::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}
*/

void PlayScene2Japan::gotoCountry2(Ref* pSender)
{
	Director::getInstance()->popScene();
}
/*
void PlayScene2Japan::MoverAbajo(Node *pSender) {
	pSender->stopAllActions();
	 
	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(pSender->getPositionX(),pos_abajo_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene2Japan::MoverArriba, this)), 
	 NULL);
	 seq->retain();

	 if (movido)
	 {
		  pSender->stopAllActions();
		 VELOCIDAD = 0;
	 }

	 else
	 {
		 pSender->runAction(seq);
	 }
	
}

void PlayScene2Japan::MoverArriba(Node *pSender) {
	pSender->stopAllActions();
	 
	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(pSender->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene2Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 
	 if (movido)
	 {
		  pSender->stopAllActions();
		 VELOCIDAD = 0;
	 }

	 else
	 {
		 pSender->runAction(seq);
	 }
	 
}

void PlayScene2Japan::Movido(Node *pSender) {
	pSender->stopAllActions();
	VELOCIDAD = 0;
	//touchListener->setEnabled(false);
	keyboardListener->setEnabled(false);
}
*/
void PlayScene2Japan::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;		switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_SPACE:
			aumento_bola += 0.1;
			break;
		

	}
}

void PlayScene2Japan::Lanzado(Node *pSender) {
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/DragonBall/ColisionBola.wav");
	Size visibleSize = Director::getInstance()->getVisibleSize();
	pSender->stopAllActions();
	keyboardListener->setEnabled(false);
	BolaGenki->setVisible(false);

	Destello_imagen = Sprite::create("PlayScene2Japan/Destello.jpg");
	Destello_imagen ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Destello_imagen -> setScale(4);
	Destello_imagen -> setScaleX(4.3);
	addChild(Destello_imagen,1);

	this->schedule(schedule_selector(PlayScene2Japan::Destello),1.0f);
	

}

void PlayScene2Japan::Destello(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	contador_destello --;
	if (contador_destello == 0)
	{
		this->removeChild(Destello_imagen);
		if (escala_final >= 2.8 )
		{
			ganado = true;
			this->removeChild(Terry);
			Terry = Sprite::create("PlayScene2Japan/Terry/Terryganado.png");
			Terry ->setPosition(Vec2(visibleSize.width/2 - visibleSize.width / 4, Terry->getContentSize().height/2 + 20));
			Terry -> setScale(0.8);
			addChild(Terry,1);
		
			this->removeChild(Freezer);
			Freezer = Sprite::create("PlayScene2Japan/Freezer/Perdido.png");
			Freezer ->setPosition(Vec2(visibleSize.width/2 + visibleSize.width / 4, Freezer->getContentSize().height / 2 + 50));
			Freezer -> setScale(2.5);
			addChild(Freezer,1);
		}

		else
		{
			this->removeChild(Terry);
			Terry = Sprite::create("PlayScene2Japan/Terry/TerryLose.png");
			Terry ->setPosition(Vec2(visibleSize.width/2 - visibleSize.width / 4, Terry->getContentSize().height/2 + 20));
			Terry -> setScale(0.5);
			addChild(Terry,1);
		
			this->removeChild(Freezer);
			Freezer = Sprite::create("PlayScene2Japan/Freezer/Ganado.png");
			Freezer ->setPosition(Vec2(visibleSize.width/2 + visibleSize.width / 4, Freezer->getContentSize().height / 2 + 70));
			Freezer -> setScale(2.5);
			addChild(Freezer,1);
		}

		this->unschedule(schedule_selector(PlayScene2Japan::Destello));
	}
	
}