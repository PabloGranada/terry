#ifndef __PLAY_SCENE2_H__
#define __PLAY_SCENE2_H__

#include "cocos2d.h"

USING_NS_CC;

const float POD_STEP_MOVE2 = 10;
const float METEOR_SPEED = 1.0f;

class PlayScene2 : public cocos2d::Layer
{
public:

	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	//Fin de la declaracion de mierda para la animacion.

	 Vec2 _podVector;
	 int counter,i;
	 bool _isMoving;
	 bool lanzado;
	 bool colisionado;
	 bool fallado;
	 int pos_vaquero;
	 CCPoint posicion1;
	 CCPoint posicion2;
	 CCPoint posicion3;
	 CCPoint posicion4;
	 CCPoint posicion1_izq;
	 CCPoint posicion2_izq;
	 CCPoint posicion3_izq;
	 CCPoint posicion4_izq;
	 CCPoint Offset;
	 int posicion;
	 int pos_terry;
	 Sprite *_playerSprite;
	 Sprite *Asteroid;
	 Sprite *Vaquero1;
	 Sprite *Vaquero2;
	 Sprite *Vaquero3;
	 Sprite *Vaquero4;
	 Sprite *Terry;
	 LabelTTF *label_timer;
	 Sprite *Fondo;
	 EventListenerKeyboard *keyboardListener;
	//Evento de toques con el rat�n
	 
	
	 // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callbackS
	 void update(float dt);
	 void UpdateTimer(float ct);
     void menuCloseCallback(cocos2d::Ref* pSender);
	 void gotoMenu(Ref* pSender);
	 void gotoCountry(Ref* pSender);
	 void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);

	 void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	 //void onTouchesMoved(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	// void onTouchesEnded(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
    
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene2);
};

#endif // __ABOUT_SCENE_H__
