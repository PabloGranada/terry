//Juego del taichi

#define COCOS2D_DEBUG 1
#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene1Japan.h"
#include "CountryScene2.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "GameInformation.h"
#include "FinalChallenge.h"

USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool alegre;
extern bool desafio_activo;

Scene* PlayScene1Japan::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlayScene1Japan::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlayScene1Japan::init()
{

	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoRelax.mp3");

	counter = 5;
	movido = false;
	VELOCIDAD = 0.5F;
	pos_abajo_y = 350.37;
	pos_arriba_y = 303.68;

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	this->scheduleUpdate();
	this->schedule(schedule_selector(PlayScene1Japan::UpdateTimer),1.0f);

	touchListener = EventListenerTouchAllAtOnce::create();
	
    touchListener-> onTouchesBegan= CC_CALLBACK_2(PlayScene1Japan::onTouchesBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener,this);

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlayScene1Japan::keyPressed, this);
	
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);


	//FARAMIR
	//A�adido para el reloj.
	//HAY QUE PONER DEBAJO EN EL CODIGO DEL CONTADOR ANTIGUO 	label_timer->setVisible(false);  PARA QUE NO SE VEA.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlayScene/reloj.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlayScene/reloj.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 4; i >= 0; i--)
	{
		CCString* filename = CCString::createWithFormat("reloj_%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,1);/*Tiempo entre frames*/
	Frame = CCSprite::createWithSpriteFrameName("reloj_1.png");

	Frame->setPosition(visibleSize.width*14/15, visibleSize.height*12/13);
	Frame->setScale(0.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Frame->runAction(action);
	Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Frame,3);	

	//Aqui termina el a�adido para el reloj.
	__String *text = __String::createWithFormat(" %d",
    counter );
	 label_timer = LabelTTF::create(text->getCString(), "Arial", 35);
    
    // position the label on the center of the screen
    label_timer->setPosition(Vec2(visibleSize.width - 50,
                            visibleSize.height - 50 ));
	
	label_timer->setColor(Gold);
    // add the label as a child to this layer
    addChild(label_timer, 3);
	label_timer->setVisible(false);
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	Fondo = Sprite::create("PlayScene1Japan/FondoMicrojuegoRelax.png");
	Fondo ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Fondo -> setScale(3.0);
	Fondo ->setScaleY(3.2);
	addChild(Fondo,1);
	

	Terry = Sprite::create("PlayScene1Japan/terryMeditando.png");
	Terry ->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	Terry -> setScale(2.5);
	addChild(Terry,1);

	Dhalsim1 = Sprite::create("PlayScene1Japan/monjeIzquierda.png");
	Dhalsim1 ->setPosition(Vec2(visibleSize.width/2 + visibleSize.width / 4, visibleSize.height/2));
	Dhalsim1 -> setScale(2.5);
	addChild(Dhalsim1,1);

	Dhalsim2 = Sprite::create("PlayScene1Japan/monjeDerecha.png");
	Dhalsim2 ->setPosition(Vec2(visibleSize.width/2 - visibleSize.width / 4, visibleSize.height/2));
	Dhalsim2 -> setScale(2.5);
	addChild(Dhalsim2,1);

	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Terry->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 Terry->runAction(seq);

	 move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Dhalsim1->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 Dhalsim1->runAction(seq);

	 move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Dhalsim2->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 Dhalsim2->runAction(seq);
	// SpriteVacio = Sprite::create("PlayScene3/spriteVacio.png");
	// SpriteVacio -> setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
	// SpriteVacio -> setScale(0.3);
 	 //Objetivo->setPosition(Point(visibleSize.width/2,
	 //Objetivo->getContentSize().height * 0.75));

	// addChild(SpriteVacio, 1);

	 posicion_mouse_original = get_cursor_pos();

	return true ;
}   

void PlayScene1Japan::UpdateTimer(float ct)
{
	counter--;
	__String *text = __String::createWithFormat("%d ",
	counter);
	label_timer->setString(text->getCString());

	if (counter==0)
	{
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		
		  
		this->unschedule( schedule_selector(PlayScene1Japan::UpdateTimer));
		
		if (movido)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/PerderMinijuegoPROVISIONAL.mp3");
			alegre = false;
			vidas_japon--;
			vidas_final--;
		}

		else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/GanarMinijuego.mp3");
		 if (desafio_activo)
		{
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");
		}

		else
			CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisJAPAN.mp3");
	//	 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/MusicaPaisUSA.mp3");
		gotoCountry2(this);
	}
}

void PlayScene1Japan::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();
	

	if (Terry->getPositionY() < visibleSize.height/4 - 50)
	{
		Terry->setPositionY(visibleSize.height/4 - 50);
	}

	if (Dhalsim1->getPositionY() < visibleSize.height/4 - 50)
	{
		Dhalsim1->setPositionY(visibleSize.height/4 - 50);
	}

	if (Dhalsim2->getPositionY() < visibleSize.height/4 - 50)
	{
		Dhalsim2->setPositionY(visibleSize.height/4 - 50);
	}
	if (movido)
	{
		
		 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Terry->getPositionX(),0));
 		 move->retain();
		 //auto *seq = Sequence::create(move);
		 auto *seq = Sequence::create(move,
		 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::Movido, this)), 
		 NULL);
		 seq->retain();
		//  Terry->stopAllActions();
		 Terry->runAction(seq);

		 move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Dhalsim1->getPositionX(),0));
 		 move->retain();
		 //auto *seq = Sequence::create(move);
		 seq = Sequence::create(move,
		 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::Movido, this)), 
		 NULL);
		 seq->retain();
		//  Terry->stopAllActions();
		 Dhalsim1->runAction(seq);

		 move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(Dhalsim2->getPositionX(),0));
 		 move->retain();
		 //auto *seq = Sequence::create(move);
		 seq = Sequence::create(move,
		 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::Movido, this)), 
		 NULL);
		 seq->retain();
		//  Terry->stopAllActions();
		 Dhalsim2->runAction(seq);
		 
	}
	else
	{
		posicion_mouse = get_cursor_pos();
	

		if (posicion_mouse != posicion_mouse_original)
		{
			movido = true;
		}
	}
	
	if (Terry->getPositionY() == visibleSize.height / 4 - 50)
	{
		VELOCIDAD = 0;
		Terry->stopAllActions();
	}
	//SpriteVacio->setPosition(posicion_mouse);
		
}

void PlayScene1Japan::onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, Event* event)
{
	CCPoint *location;
	Size visibleSize = Director::getInstance()->getVisibleSize();
	//CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1.0);
	//CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Saloon/Shoot.mp3");
	for (auto& touch : touches)
    {
		 CCPoint location = touch->getLocation();
		 CCLOG("Y: %f",location.y );
	     movido = true;
    }
 }

cocos2d::Point PlayScene1Japan::get_cursor_pos()
{
	CCEGLView*	egl_view = Director::getInstance()->getOpenGLView();
    GLFWwindow* window = egl_view->getWindow();
    double px, py;
    glfwGetCursorPos(window, &px, &py);

    //TODO: cache window size
    int x,y;
    glfwGetWindowSize(window, &x, &y);

    return cocos2d::Point(px, y-py);
}

void PlayScene1Japan::gotoCountry2(Ref* pSender)
{
	Director::getInstance()->popScene();
}

void PlayScene1Japan::MoverAbajo(Node *pSender) {
	pSender->stopAllActions();
	 
	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(pSender->getPositionX(),pos_abajo_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::MoverArriba, this)), 
	 NULL);
	 seq->retain();

	 if (movido)
	 {
		  pSender->stopAllActions();
		 VELOCIDAD = 0;
	 }

	 else
	 {
		 pSender->runAction(seq);
	 }
	
}

void PlayScene1Japan::MoverArriba(Node *pSender) {
	pSender->stopAllActions();
	 
	 auto *move = (FiniteTimeAction *) MoveTo::create(2/VELOCIDAD,Vec2(pSender->getPositionX(),pos_arriba_y));
 	 move->retain();
	 //auto *seq = Sequence::create(move);
	 auto *seq = Sequence::create(move,
	 CallFuncN::create(CC_CALLBACK_1(PlayScene1Japan::MoverAbajo, this)), 
	 NULL);
	 seq->retain();
	 
	 if (movido)
	 {
		  pSender->stopAllActions();
		 VELOCIDAD = 0;
	 }

	 else
	 {
		 pSender->runAction(seq);
	 }
	 
}

void PlayScene1Japan::Movido(Node *pSender) {
	pSender->stopAllActions();
	VELOCIDAD = 0;
	//touchListener->setEnabled(false);
	keyboardListener->setEnabled(false);
}

void PlayScene1Japan::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	movido = true;
}