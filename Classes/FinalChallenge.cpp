#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "PlayScene6.h"
#include "FinalChallenge.h"
#include "PlaySceneFinal.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "PlayScene5Japan.h"
#include "PlaySceneFinalJapan.h"
#include "PlaySceneFinal.h"
#include "Credits.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "GameInformation.h"

USING_NS_CC;

extern bool alegre;
extern int vidas_final;
extern bool desafio_activo;

Scene* FinalChallenge::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = FinalChallenge::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool FinalChallenge::init()
{
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/FINALCHALLENGE.mp3");

	srand(time(NULL));
	desafio_activo = true;
	_podVector = Vec2::ZERO;
	_isMoving = false;
	counter_credits = 400;
	counter = 5;
	generado = false;
	alegre = true;
	empezado = false;
	repe = false;
	NUMERO_NIVELES_FINAL = 11;
	displayed = false;
	aumentado = false;
	senyalando = true;
	
	

	Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Color3B Gold = Color3B::ORANGE;

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	auto Fondo = Sprite::create("FinalChallenge/PosibleFondo3.png");

    // position the sprite on the center of the screen
    Fondo->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2 ));
	Fondo->setScaleY(3.18);
	Fondo->setScaleX(3.3655);
	
    // add the sprite as a child to this layer
    this->addChild(Fondo, 0);

	Terry = Sprite::create("CountryScene/TerrySenyalando.png");
	Terry->setPosition(Vec2(visibleSize.width/2 ,visibleSize.height/2 - 50));
	Terry->setScale(3.0);
	this->addChild(Terry,0);

	Vidas_banderas = Sprite::create("FinalChallenge/VidasNivel/3lives.png");
	Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
	Vidas_banderas->setScale(2);
	addChild(Vidas_banderas,1);

	Cartel = Sprite::create("CountryScene/TerrySpriteSheet.png");
	Cartel ->setPosition(2000.0,2000.0);
	Cartel ->setVisible(false);
	this->addChild(Cartel,0);
	
	
	this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
	this->scheduleUpdate();

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	return true ;
}   

void FinalChallenge::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

void FinalChallenge::Transition(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	counter_credits --;
	if (counter_credits == 4)
	{
		Blanco->setPosition(visibleSize.width/2, visibleSize.height/2);
		Blanco->setScale(10);
		auto fadeIn = FadeIn::create(4.0f);
		auto *seq = Sequence::create(fadeIn,NULL);
		Blanco->runAction(seq);
	}
	if (counter_credits == -300)
	{
		gotoCredits(this);
	}
}

void FinalChallenge::gotoCredits(Ref *pSender)
{
	auto scene = Credits::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::UpdateTimer(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();

	counter--;
	
	
	
	if (numero_nivel == -1 & !displayed)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		Vidas_banderas->setVisible(false);
		this->removeChild(Cartel);
		Cartel2->setVisible(false);
		Cartel = Sprite::create("FinalChallenge/FinalChallengeComplete.png");
		Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
		//Cartel ->setScale(1.5);
		this->addChild(Cartel,0);
		this->removeChild(Terry);
		Terry = Sprite::create("CountryScene/TerryMovAbajo.png");
		Terry->setPosition(Vec2(visibleSize.width/2 ,visibleSize.height/2 - 50));
		Terry->setScale(3.0);
		this->addChild(Terry,0);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Menu/FINALCHALLENGE.mp3");
		displayed = true;
	}
	

	 if (!generado)
	 {
		numero_nivel = aleatorio(minimo,maximo,repe);
		
		//if (numero_nivel == -1)
		//{
		//	Cartel->setVisible(false);
		//}
		generado = true;
	 }
	 
	if (counter == 3)
	{
		senyalando = true;
		
	}
	 if (counter == 1)
	 {
		 //if (numero_nivel != -1)
		// {
		//	  Cartel->setVisible(true);
		// }
		
		 switch (numero_nivel)
		{
			case 1:
		
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioSaloon.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 2:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioTapper.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 3:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioPunchOut.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 4:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioRugby.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 5:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloPrevioSaloon.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
			case 6:
		
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoMeditar.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 7:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoGenki.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 8:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/TituloMicrojuegoSushi.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 9:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/Cut.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;

			case 10:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/Climb.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
			case 11:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/Protect.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);
				break;
			case 12:
				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene/TituloMicrojuegoMagneto.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);

				Cartel2 = Sprite::create("CountryScene/TituloMicrojuegoFinal.png");
				Cartel2 ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 200));
				//Cartel2 ->setScale(1.5);
				this->addChild(Cartel2,0);
				//Cartel ->setVisible(false);
				
				break;
			case 13:

				this->removeChild(Cartel);
				Cartel = Sprite::create("CountryScene2/Destroy.png");
				Cartel ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 100));
				Cartel ->setScale(1.5);
				this->addChild(Cartel,0);

				Cartel2 = Sprite::create("CountryScene/TituloMicrojuegoFinal.png");
				Cartel2 ->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 + 200));
				//Cartel2 ->setScale(1.5);
				this->addChild(Cartel2,0);
				//Cartel ->setVisible(false);
				
				break;
		}
	 }


	if (counter==0)
	{
	
		Cartel->setVisible(false);
		empezado = true;
		generado = false;
		senyalando = false;
		
		if (numero_nivel == -1)
		{
				Blanco = Sprite::create("FinalChallenge/lefa.jpg");
				Blanco->setPosition(2000000,200000);
				auto fadeOut = FadeOut::create(0.000001f);
				auto *seq = Sequence::create(fadeOut,NULL);
				Blanco->runAction(seq);
				addChild(Blanco);
				
				this->unschedule(schedule_selector(FinalChallenge::UpdateTimer));
				this->unscheduleUpdate();
				this->schedule(schedule_selector(FinalChallenge::Transition,1.0f));
		}
		

		else
		{
			this->unschedule( schedule_selector(FinalChallenge::UpdateTimer));
			switch (numero_nivel)
			{
			   case 1:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay(this);
				   this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
				  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				   counter = 5;
				   break;

			   case 2:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay2(this);
				   this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
				//      CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				
				   counter = 5;
				   break;

			   case 3:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay3(this);
				   this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
				 //   CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				 
				   counter = 5;
				   break;
			   case 4:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay4(this);
				   this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
				   // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				  
				   counter = 5;
				   break;
			   case 5:
				   alegre = true;
				   Cartel ->setVisible(false);
				   CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
				   gotoPlay5(this);
				   this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
				   //  CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
				   
				   counter = 5;
				   break;

				case 6:

					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay1Japan(this);
					this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 7:

					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay2Japan(this);
					this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 8:
					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay3Japan(this);
					this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 9:
					alegre = true;
				    Cartel ->setVisible(false);
					gotoPlay4Japan(this);
					this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;

				case 10:
					alegre = true;
					
				    Cartel ->setVisible(false);
					gotoPlay5Japan(this);
					this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					  // CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					counter = 5;
					break;
				case 11:
					 alegre = true;
					
					 Cartel ->setVisible(false);
					 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
					 gotoPlay6(this);
					 this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					// CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					 counter = 5;
					break;
				
				case 12:
					 alegre = true;
					
					 Cartel ->setVisible(false);
					 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
					 gotoPlayFinal(this);
					 this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					// CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					 counter = 5;
					break;
				case 13:
					 alegre = true;
					 
					 Cartel ->setVisible(false);
					 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
					 gotoPlayFinalJapan(this);
					 this->schedule(schedule_selector(FinalChallenge::UpdateTimer),1.0f);
					// CocosDenshion::SimpleAudioEngine::getInstance()->resumeEffect(Fondo_Musica);
					 counter = 5;
					break;
				
				
					


			}
		}
			
	}
}

void FinalChallenge::update(float dt) {

	Size visibleSize = Director::getInstance()->getVisibleSize();

	//Cartel->setVisible(false);
	if (vidas_final == 0)
		{
			gotoGameOver(this);
	
		}

	else if (vidas_final == 1)
	{
			this->removeChild(Vidas_banderas);
			Vidas_banderas = Sprite::create("FinalChallenge/VidasNivel/1lives.png");
			Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
			Vidas_banderas->setScale(2);
			addChild(Vidas_banderas,1);
	}

	else if (vidas_final == 2)
	{
			this->removeChild(Vidas_banderas);
			Vidas_banderas = Sprite::create("FinalChallenge/VidasNivel/2lives.png");
			Vidas_banderas->setPosition(Vec2(150,visibleSize.height/2));// visibleSize.height/2 ));
			Vidas_banderas->setScale(2);
			addChild(Vidas_banderas,1);
	}
		
	
	if (alegre && empezado)
	{
		this->removeChild(Terry);
		Terry = Sprite::create("CountryScene/TerryMovAbajo.png");
		Terry->setPosition(Vec2(visibleSize.width/2 ,visibleSize.height/2 - 50));
		Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	else if(!alegre && empezado)
	{
		
		this->removeChild(Terry);
		Terry = Sprite::create("CountryScene/TerryLose.png");
		Terry->setPosition(Vec2(visibleSize.width/2 ,visibleSize.height/2 - 50));
		Terry->setScale(3.0);
		this->addChild(Terry,0);
	}

	if (senyalando)
	{
		this->removeChild(Terry);
		 Terry = Sprite::create("FinalChallenge/TerryPensandoPais.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 ,visibleSize.height/2 - 50));
		 Terry->setScale(3.0);
		 this->addChild(Terry,0);
	}
		
}

void FinalChallenge::gotoPlay(Ref* pSender)
{
	auto scene = PlayScene::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay2(Ref* pSender)
{   
	auto scene = PlayScene2::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay3(Ref* pSender)
{   
	auto scene = PlayScene3::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay4(Ref* pSender)
{   
	auto scene = PlayScene4::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay5(Ref* pSender)
{   
	auto scene = PlayScene5::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay6(Ref* pSender)
{   
	auto scene = PlayScene6::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlayFinal(Ref* pSender)
{   
	auto scene = PlaySceneFinal::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay1Japan(Ref* pSender)
{
	auto scene = PlayScene1Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay2Japan(Ref* pSender)
{
	auto scene = PlayScene2Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay3Japan(Ref* pSender)
{
	auto scene = PlayScene3Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay4Japan(Ref* pSender)
{
	auto scene = PlayScene4Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlay5Japan(Ref* pSender)
{
	auto scene = PlayScene5Japan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoPlayFinalJapan(Ref* pSender)
{
	auto scene = PlaySceneFinalJapan::createScene();
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void FinalChallenge::gotoGameOver(Ref* pSender)
{   
	auto scene =GameOver::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

bool FinalChallenge::repetido(int numero)
{
	numero_repetido = false;
	for (int i = 0; i < niveles.size(); i++)
	{
		if (numero == niveles[i])
		{
			numero_repetido = true;
			break;
		}
	}

	return numero_repetido;
}

long int FinalChallenge::aleatorio(int min, int max,bool repe)
{
	
	if (niveles.size() != NUMERO_NIVELES_FINAL)
	{
		do
		{
			num = rand()% NUMERO_NIVELES_FINAL + 1;
			repe = repetido(num);
		} while (repe != false);
		
		
		niveles.push_back(num);
		return num;
	}
	else if (!aumentado)
	
	{
		NUMERO_NIVELES_FINAL += 2;
		aumentado = true;
		do
		{
			num = rand()% NUMERO_NIVELES_FINAL + 1;
			repe = repetido(num);
		} while (repe != false);
		
		
		niveles.push_back(num);
		return num;
		
		
	}
	else
	{
		return -1;
	}
}

void FinalChallenge::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}
