#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene1Japan.h"
#include "PlayScene2Japan.h"
#include "PlayScene3Japan.h"
#include "PlayScene4Japan.h"
#include "PlayScene5Japan.h"
#include "PlaySceneFinalJapan.h"
#include "SelectCountryScene.h"
#include "CountryScene.h"
#include "CountryScene2.h"
#include "GameOver.h"
#include "FinalChallenge.h"
#include "MyBodyParser.h"
#include "PantallaWin2.h"


USING_NS_CC;

extern int vidas_japon;
extern int vidas_final;
extern bool desafio_final_bloqueado;
extern bool alegre;
extern bool desafio_activo;

Scene* PlaySceneFinalJapan::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    // 'layer' is an autorelease object
    auto layer = PlaySceneFinalJapan::create();

	layer->setPhysicsWorld(scene->getPhysicsWorld());

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlaySceneFinalJapan::init()
{
	srand(time(NULL));
	visibleSize = Director::getInstance()->getVisibleSize();

	auto director = Director::getInstance();
	auto glview = director->getOpenGLView();
	
    glfwSetInputMode(glview->getWindow(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlaySceneFinalJapan::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(PlaySceneFinalJapan::keyReleased, this);

	contactListener = EventListenerPhysicsContact::create();
	
	contactListener->onContactBegin = CC_CALLBACK_1(PlaySceneFinalJapan::onContactBegin, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
	
	director->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	fin_nivel = 250;
	counter_disparo = 3;
	counter_disparo_naves = 30;
	shake = 20;
	impactos_godzilla = 50;
	godzilla_disparando = 250;
	ganar_nivel = 10;
	desvanecerse_disparo = 60; //Originariamente 3
	counter_naves = 0; //Originariamente 20
	counter_godzilla = 7; //Originariamente 6
	disparo_disponible = true;
	disparo_disponible_nave = true;
	pasado_shake = true;

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	for (int i = 0; i < 2; i++) {
		_backgroundSpriteArray[i] = Sprite::create("PlaySceneFinalJapan/FondoMicrojuego/PruebaParallax.png");
		_backgroundSpriteArray[i]->setScale(1.55);
		_backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * (i + 0.5f),
		 visibleSize.height/2 ));
		addChild(_backgroundSpriteArray[i], 0);
 }

	Bolazo = Sprite::create("PlaySceneFinalJapan/Godzilla/AtaqueGodzilla.png");
	Bolazo->setPosition(20000,20000);
	addChild(Bolazo,1);

	frameCache2 = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache2->addSpriteFramesWithFile("Credits/Avion/avion.plist");
    
	spritesheet2 = CCSpriteBatchNode::create("Credits/Avion/avion.png");
	this->addChild(spritesheet2,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames2(4);
	for ( i = 1; i <= 4; i++)
	{
		CCString* filename = CCString::createWithFormat("V2TerryVolando%d.png", i);//Spritesheet
		CCSpriteFrame* frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames2.pushBack(frame2);
	}

	CCAnimation* runAnim2 = CCAnimation::createWithSpriteFrames(SpriteFrames2,0.1);
	Nave = CCSprite::createWithSpriteFrameName("V2TerryVolando1.png");

	Nave->setPosition(Nave->getContentSize().width,visibleSize.height/2);
	Nave->setScale(1.4);
    
	CCAction* action2 = CCRepeatForever::create(CCAnimate::create(runAnim2));
    
		
	//Nave = Sprite::create("PlaySceneFinalJapan/Terry/V2TerryVolando3.png");
	//Nave->setPosition(Nave->getContentSize().width,visibleSize.height/2);
	//Nave->setScale(1.4);
	auto body = PhysicsBody::createCircle(Nave->getBoundingBox().size.width/2);
	body->setContactTestBitmask(true);
	body->setDynamic(true);
	Nave->setPhysicsBody(body);
	//addChild(Nave);
	Nave->runAction(action2);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet2->addChild(Nave,3);

	Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaStandBy.png");
	Godzilla->setPosition(2000,2000);
	//Godzilla->setScale(4);
	
	PlaceHolder = Sprite::create("PlayScene3/espera.png");
	PlaceHolder->setPosition(-20000,-20000);
	addChild(PlaceHolder);
	addChild(Godzilla);
	
	this->scheduleUpdate();
	this->schedule(schedule_selector(PlaySceneFinalJapan::TimerNaves),1.0f);
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoGodzillaPre.mp3");
	
	return true ;
}   

void PlaySceneFinalJapan::update(float dt) {

	if (Bolazo != nullptr)
	{
		if (Bolazo->boundingBox().containsPoint(Nave->getPosition()))
		{
			spritesheet2->setVisible(false);
			 this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerNaves));
		 this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla));
		 this->unschedule(schedule_selector(PlaySceneFinalJapan::GanarNivel));
		 keyboardListener->setEnabled(false);
		 //Nodo A es Terry y nodo B la nave/fuego
		
		pos_terry = Nave->getPosition();
		this->removeChild(Nave);
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		vidas_japon --;
		vidas_final--;
		Nave = Sprite::create("PlaySceneFinalJapan/Terry/V2TerryVolando3.png");
		Nave->setPosition(pos_terry);
		auto *move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(0, -200));
		move->retain();
		auto *seq = Sequence::create(move,NULL);//CallFuncN::create(CC_CALLBACK_2(PlaySceneFinalJapan::GodzillaAcabado, this)), NULL);
		this->schedule(schedule_selector(PlaySceneFinalJapan::GodzillaAcabado,1.0f));
		seq->retain();
		Nave->runAction(seq);
		addChild(Nave);
		
		Humo = Sprite::create("PlaySceneFinalJapan/EnemigosBasicos/EnemigoDestruido.png");
		Humo->setPosition(pos_terry);
		move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(pos_terry.x,-200));
			
		move->retain();
		seq = Sequence::create(move,NULL);
		seq->retain();
		Humo->runAction(seq);
		auto fadeOut = FadeOut::create(1.5f);
		seq = Sequence::create(fadeOut,NULL);
		Humo->runAction(seq);
		addChild(Humo);

	    this->schedule(schedule_selector(PlaySceneFinalJapan::Desvanecerse_disparo,1.0f));
		 
		}
	}

	for (int i = 0; i < 2; i++) {
		if (_backgroundSpriteArray[i]->getPosition().x < (visibleSize.width* -0.5))
		 _backgroundSpriteArray[i]->setPosition(Point(visibleSize.width * 1.5f,
	     visibleSize.height/2 ));
		 else
			 _backgroundSpriteArray[i]->setPosition(
			 Point(_backgroundSpriteArray[i]->getPosition().x  -
			 (CINTA_SPEED * visibleSize.width * dt),
			 _backgroundSpriteArray[i]->getPosition().y));
	}

	if (impactos_godzilla == 0)
	{
		desafio_final_bloqueado = false;
		alegre = true;
		this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla));
		this->unschedule(schedule_selector(PlaySceneFinalJapan::Proceso_disparar));
		if (Proyectil != nullptr)
		{
			this->removeChild(Proyectil);
		}

		
		this->unscheduleUpdate();
		this->schedule(schedule_selector(PlaySceneFinalJapan::GanarNivel),1.0f);
	}

	if (keyboardListener->isEnabled())
	{
		if (_isMoving ) {
			 Vec2 newPos = Vec2(Nave->getPosition().x + _podVector.x,
			Nave->getPosition().y + _podVector.y);
 
		if (newPos.x >=Nave->getBoundingBox().size.width/2 &&
			 newPos.x <= visibleSize.width - Nave->getBoundingBox().size.width/2 &&
			 newPos.y >=Nave->getBoundingBox().size.height/2 &&
			 newPos.y <= visibleSize.height - Nave->getBoundingBox().size.height/2)
			{
			  Nave->setPosition(newPos);
			}		}
	}
	
		
}


void PlaySceneFinalJapan::TimerNaves(float ct)
{
	counter_naves --;
	if (counter_naves%2 == 0)
	{
		 int EnemigoIndex = (std::rand() % 3 + 1);
	 __String *filename = __String::createWithFormat ("PlaySceneFinalJapan/EnemigosBasicos/Enemigo%i.png",
	 EnemigoIndex);

		Nave_enemiga = Sprite::create(filename->getCString());
		Nave_enemiga->setScale(1.9);
		 int yRandomPosition = (std::rand() % (int)(visibleSize.height - 
		 Nave_enemiga->getContentSize().width)) + 
		 Nave_enemiga->getContentSize().width / 2;

		 Nave_enemiga->setPosition(visibleSize.width + visibleSize.width/2, yRandomPosition);
		 auto body = PhysicsBody::createCircle(Nave_enemiga->getBoundingBox().size.width/2);
	     body->setContactTestBitmask(true);
	 	 body->setDynamic(true);
		 Nave_enemiga->setPhysicsBody(body);
		 move = (FiniteTimeAction *) MoveBy::create(2/DISPARO_SPEED_GODZILLA,
		 Point(-(visibleSize.width/2 + visibleSize.width) ,0 ));
		 move->retain();
		 seq = Sequence::create(move,CallFuncN::create(CC_CALLBACK_1(PlaySceneFinalJapan::Navellegada, this)), 
		 NULL);
		 seq->retain();
		 Nave_enemiga->runAction(seq);
		 addChild(Nave_enemiga,1);
		 
	}

	if (counter_naves == -12)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoGodzilla.mp3");
	}
	if (counter_naves < -14)
	{
		this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerNaves));
		this->contactListener->setEnabled(false);
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaStandBy.png");
		Godzilla->setPosition(visibleSize.width+100,visibleSize.height/2);
		
		//MyBodyParser::getInstance()->parseJsonFile("bodies.json");
		//auto _body = MyBodyParser::getInstance()->bodyFormJson(Godzilla, "Godzilla");
		//if (_body != nullptr) {
		// _body->setDynamic(true); //set it static body.
		
	 	//_body->setContactTestBitmask(true);
		
		// Godzilla->setPhysicsBody(_body);
		// }
		Godzilla->setScale(4);
		
		auto moveBy = (FiniteTimeAction *)  MoveBy::create(2, Vec2(-280,0));
		auto delay = DelayTime::create(2.0f);
		auto sequence = Sequence::create(moveBy, delay,NULL);// schedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla),1.0f), NULL);
		sequence->retain();
		this->keyboardListener->setEnabled(false);
		initial_pos_nave = Nave->getPosition();
		if (Proyectil != nullptr)
		{
			this->removeChild(Proyectil);
		}

		if (Nave_enemiga != nullptr)
		{
			this->removeChild(Nave_enemiga);
		}
		this->schedule(schedule_selector(PlaySceneFinalJapan::shakeScreen),0.2f);
		pasado_shake = false;
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		Godzilla->runAction(sequence);
		addChild(Godzilla,1);
		this->schedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla),1.0f);
	}
	
}

void PlaySceneFinalJapan::TimerGodzilla(float ct)
{
	counter_godzilla --;
	if (counter_godzilla == 0)
	{
		godzilla_disparando = 250;
		this->schedule(schedule_selector(PlaySceneFinalJapan::Proceso_disparar,1.0f));
		this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla));
		
	}

}

void PlaySceneFinalJapan::TimerDisparo(float ct)
{
	counter_disparo --;
	if (counter_disparo < 0)
	{
		disparo_disponible = true;
		this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerDisparo));
	}
}


void PlaySceneFinalJapan::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	_pressedKey = keyCode;	switch (_pressedKey) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_podVector = Vec2(0, POD_STEP_MOVE );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW:
			_podVector = Vec2(0, -POD_STEP_MOVE );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_SPACE:
						
			if (disparo_disponible && pasado_shake)
			{
				this->schedule(schedule_selector(PlaySceneFinalJapan::TimerDisparo),0.5f);
				disparo_disponible = false;
				CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Godzilla/DisparoTerry2.mp3");
				Proyectil = Sprite::create("PlaySceneFinalJapan/Terry/DisparoTerry1.png");
				Proyectil->setPosition(Nave->getPositionX() + 120, Nave->getPositionY());
				Proyectil->setScale(3);
				auto body = PhysicsBody::createCircle(Proyectil->getBoundingBox().size.width/2);
				body->setContactTestBitmask(true);
				body->setDynamic(true);
				Proyectil->setPhysicsBody(body);
				move = (FiniteTimeAction *) MoveBy::create(2/DISPARO_SPEED_GODZILLA,
				Point(visibleSize.width + 100 ,0 ));
				move->retain();
				seq = Sequence::create(move,CallFuncN::create(CC_CALLBACK_1(PlaySceneFinalJapan::Disparollegado, this)), 
				NULL);
				seq->retain();
		 		Proyectil->runAction(seq);
				addChild(Proyectil,1);
				int angle = (std::rand() % 360) - 180;
				Action *rotate = RepeatForever::create(RotateBy::create(0.5, angle));
 
				rotate->retain();
				Proyectil->runAction(rotate);
			}
			break;

			//Disparo(this);
			
		/*
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW:
			_podVector = Vec2(-POD_STEP_MOVE, 0 );
			_isMoving = true;
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_podVector = Vec2(POD_STEP_MOVE, 0 );
			_isMoving = true;
			break;
		
		case EventKeyboard::KeyCode::KEY_M:
			if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

			else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
				break;
				*/
	}
}
void PlaySceneFinalJapan::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	_pressedKey = keyCode;

	if (_pressedKey == keyCode) {
		 _pressedKey = EventKeyboard::KeyCode::KEY_NONE;
		 _isMoving = false;
		 _podVector = Vec2::ZERO;
	}
}

void PlaySceneFinalJapan::Disparollegado(Node *pSender)
{
	 removeChild(pSender);
}



void PlaySceneFinalJapan::GodzillaAcabado(float ct)
{

	fin_nivel --;
	if (fin_nivel == 0)
	{
		alegre = false;
		gotoCountry2(this);
	}
	
}

void PlaySceneFinalJapan::GanarNivel(float ct)
{
	ganar_nivel--;
		
	if (ganar_nivel == 9)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPetando1.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		Godzilla->setScale(4);
		this->keyboardListener->setEnabled(false);
		addChild(Godzilla,1);
	}

	if (ganar_nivel == 7)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPetando2.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		Godzilla->setScale(4);
		this->keyboardListener->setEnabled(false);
		addChild(Godzilla,1);
		Cabeza = Sprite::create("PlaySceneFinalJapan/Godzilla/CabezaMorir.png");
		Cabeza->setPosition(Godzilla->getPositionX(),Godzilla->getPositionY() + 200);
		Cabeza->setScale(4);
		move = (FiniteTimeAction *) MoveBy::create(0.5/DISPARO_SPEED_GODZILLA,
		Point(0 ,500 ));
		move->retain();
		seq = Sequence::create(move,NULL);//CallFuncN::create(CC_CALLBACK_1(PlaySceneFinalJapan::Disparollegado, this)), 
		//NULL);
		seq->retain();
		Cabeza->runAction(seq);
		addChild(Cabeza,1);

	}
	
	if (ganar_nivel == 2) // Inicialmente 6
	{
		keyboardListener->setEnabled(false);
		pos_terry = Nave->getPosition();
		spritesheet2->setVisible(false);
		this->removeChild(Nave);
		
	auto *move = (FiniteTimeAction *) MoveTo::create(2/RUGBY_SPEED,Vec2(3000,3000)); // Aumentar esta distancia pa que tarde m�s en irser
	move->retain();
	//auto *seq = Sequence::create(move);
	auto *seq = Sequence::create(move,CallFuncN::create(CC_CALLBACK_1(PlaySceneFinalJapan::gotoPantallaWin, this)), 
	NULL);
	seq->retain();
	
	Nave = Sprite::create("PantallaWin/TerryAvionDerecha.png");
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/SonidoAvion.wav");
	Nave->setPosition(pos_terry);
	Nave->setScale(1.5);
	Nave->runAction(seq);
 	addChild(Nave, 2);
	}
	
	if (ganar_nivel == 5)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaDestruida.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		Godzilla->setScale(4);
		this->keyboardListener->setEnabled(false);
		addChild(Godzilla,1);
	}

	
	
	

}


void PlaySceneFinalJapan::Navellegada(Node *pSender)
{
	removeChild(pSender);
}

void PlaySceneFinalJapan::setPhysicsWorld(PhysicsWorld *world) {
	 mWorld = world;
	 mWorld->setGravity(Vec2::ZERO);
}

bool PlaySceneFinalJapan::onContactBegin(PhysicsContact &contact) {

	
	 auto nodeA = contact.getShapeA()->getBody()->getNode();
      auto nodeB = contact.getShapeB()->getBody()->getNode();
	 //Nodo A o B son Godzilla	
	 
	 if (nodeA->getPosition() == PlaceHolder->getPosition() || nodeB->getPosition() == PlaceHolder->getPosition() )
	 {
		 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Godzilla/DanyoGodzilla.mp3");
		
		 //El nodo A es Godzilla
		 
		 if (nodeA->getPosition() == PlaceHolder->getPosition())
		 {
			 impactos_godzilla --;
			//this->removeChild(nodeB);
			
		 }

		 //El nodo B es Godzilla
		 else if (nodeB->getPosition() == PlaceHolder->getPosition())
		 {
			  impactos_godzilla --;
			 //this->removeChild(nodeA);
		 }
		 
		
			
		
		 return true ;
	 }
	 
	 //Nodo A o B son Terry
	 else if (nodeA->getPosition() == Nave->getPosition() || nodeB-> getPosition() == Nave->getPosition() )
	 {
		 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Godzilla/Destruccion2.mp3");
		 this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerNaves));
		 this->unschedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla));
		 this->unschedule(schedule_selector(PlaySceneFinalJapan::GanarNivel));
		 keyboardListener->setEnabled(false);
		 //Nodo A es Terry y nodo B la nave/fuego
		 if (nodeA->getPosition() == Nave->getPosition())
		 {
			pos_terry = nodeA->getPosition();
			this->removeChild(nodeA);
			this->removeChild(nodeB);
			spritesheet2->setVisible(false);
			CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
			vidas_japon --;
			vidas_final--;
			Nave = Sprite::create("PlaySceneFinalJapan/Terry/V2TerryVolando3.png");
			Nave->setPosition(pos_terry);
			auto *move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(0, -200));
			move->retain();
			auto *seq = Sequence::create(move,NULL);//CallFuncN::create(CC_CALLBACK_2(PlaySceneFinalJapan::GodzillaAcabado, this)), NULL);
			this->schedule(schedule_selector(PlaySceneFinalJapan::GodzillaAcabado,1.0f));
			seq->retain();
			Nave->runAction(seq);
			addChild(Nave);
			this->removeChild(nodeB);
			Humo = Sprite::create("PlaySceneFinalJapan/EnemigosBasicos/EnemigoDestruido.png");
			Humo->setPosition(pos_terry);
			Humo->setScale(3);
			move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(pos_terry.x,-200));
			
			move->retain();
			seq = Sequence::create(move,NULL);
			seq->retain();
			Humo->runAction(seq);
			auto fadeOut = FadeOut::create(1.5f);
			seq = Sequence::create(fadeOut,NULL);
			Humo->runAction(seq);
			addChild(Humo);

	     	this->schedule(schedule_selector(PlaySceneFinalJapan::Desvanecerse_disparo,1.0f));
		 }

		 //Nodo B es Terry y nodo A la nave/fuego
		 else
		 {
			pos_terry = nodeB->getPosition();
			this->removeChild(nodeA);
			this->removeChild(nodeB);
			spritesheet2->setVisible(false);
			CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
			vidas_japon --;
			Nave = Sprite::create("PlaySceneFinalJapan/Terry/V2TerryVolando3.png");
			Nave->setPosition(pos_terry);
			auto *move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(0,-200));
			
			move->retain();
			auto *seq = Sequence::create(move,NULL);
			this->schedule(schedule_selector(PlaySceneFinalJapan::GodzillaAcabado,1.0f));
			//CallFuncN::create(CC_CALLBACK_2(PlaySceneFinalJapan::GodzillaAcabado, this)), NULL);
			seq->retain();
			Nave->runAction(seq);
			addChild(Nave);
			this->removeChild(nodeA);
			Humo = Sprite::create("PlaySceneFinalJapan/EnemigosBasicos/EnemigoDestruido.png");
	     	Humo->setPosition(pos_terry);
	    	move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(pos_disparo.x,-200));
			
		    move->retain();
			seq = Sequence::create(move,NULL);
			seq->retain();
			Humo->runAction(seq);
			auto fadeOut = FadeOut::create(1.5f);
			seq = Sequence::create(fadeOut,NULL);
			Humo->runAction(seq);
			addChild(Humo);

			this->schedule(schedule_selector(PlaySceneFinalJapan::Desvanecerse_disparo,1.0f));

		 }
		 		  
		 return true ;
	
	 }

	 else // Ha chocado con una nave: una bala y una nave
	 {
		 if (spritesheet2->isVisible())
		 {
			 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Godzilla/Destruccion2.mp3");
		pos_disparo = nodeB->getPosition();
		this->removeChild(nodeB);
		this->removeChild(nodeA);
		
		Humo = Sprite::create("PlaySceneFinalJapan/EnemigosBasicos/EnemigoDestruido.png");
		Humo->setPosition(pos_disparo);
		Humo->setScale(2);
		auto *move = (FiniteTimeAction *) MoveTo::create(2/DISPARO_SPEED_GODZILLA,Vec2(pos_disparo.x,-200));
			
		move->retain();
		auto *seq = Sequence::create(move,NULL);
		seq->retain();
		Humo->runAction(seq);
		auto fadeOut = FadeOut::create(1.5f);
		seq = Sequence::create(fadeOut,NULL);
		Humo->runAction(seq);
		addChild(Humo);

		this->schedule(schedule_selector(PlaySceneFinalJapan::Desvanecerse_disparo,1.0f));
		
		 return true ;
		 }
		
	 }
	 
	  
	
	//menuCloseCallback(this);
	
}


void PlaySceneFinalJapan::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif

    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void PlaySceneFinalJapan::shakeScreen(float dt)
{
    float randx = rangeRandom( -5.0f, 5.0f );
    float randy = rangeRandom( -5.0f, 5.0f );

    this->setPosition(Point(randx, randy));
    Nave->setPosition(Point(Nave->getPositionX() + randx, Nave->getPositionY() + randy));
	Godzilla->setPosition(Point(Godzilla->getPositionX() + randx, Godzilla->getPositionY() + randy));
	
    shake -= 1;

    if (shake <= 0)
    {
		Nave->setPosition(initial_pos_nave);
        Godzilla->setPosition(Point(visibleSize.width -180, visibleSize.height/2 ));
		PlaceHolder = Sprite::create("PlayScene3/spriteVacio.png");
		PlaceHolder->setPosition(Godzilla->getPositionX(),Godzilla->getPositionY());
		
		MyBodyParser::getInstance()->parseJsonFile("bodies.json");
		GodziBody = MyBodyParser::getInstance()->bodyFormJson(PlaceHolder, "Godzilla");
		if (GodziBody != nullptr) {
		 GodziBody->setDynamic(false); //set it static body.
		
	 	GodziBody->setContactTestBitmask(true);
		
		 PlaceHolder->setPhysicsBody(GodziBody);
		 }
		PlaceHolder->setScale(16);
		addChild(PlaceHolder,1);
        this->unschedule(schedule_selector(PlaySceneFinalJapan::shakeScreen));
		this->keyboardListener->setEnabled(true);
		this->contactListener->setEnabled(true);
    }
}

float PlaySceneFinalJapan::rangeRandom( float min, float max )
{
    float rnd = ((float)rand()/(float)RAND_MAX);
    return rnd*(max-min)+min;
}


void PlaySceneFinalJapan::gotoCountry2(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}

void PlaySceneFinalJapan::Desvanecerse_disparo(float ct)
{
	desvanecerse_disparo --;
	if (desvanecerse_disparo == 0)
	{
		this->removeChild(Humo);
		desvanecerse_disparo = 60;
		this->unschedule(schedule_selector(PlaySceneFinalJapan::Desvanecerse_disparo));

	}
}

void PlaySceneFinalJapan::Proceso_disparar(float ct)
{
	godzilla_disparando --;
	if (godzilla_disparando == 220)
	{
		
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPreparaAtaque1.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
	}

	if (godzilla_disparando == 195)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPreparaAtaque2.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
	}

	if (godzilla_disparando == 175)
	{
		if (!pasado_shake)
		{
			pasado_shake = true;
		}
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPreparaAtaque3.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
	}

	if (godzilla_disparando == 140)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaPreparaAtaque4.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
	}

	if (godzilla_disparando == 110)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Godzilla/GodzillaLanzaElFuego.mp3");
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaLanzadoAtaque.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
		//this->removeChild(Bolazo);
		Bolazo = Sprite::create("PlaySceneFinalJapan/Godzilla/AtaqueGodzilla.png");
		Bolazo->setPosition(Godzilla->getPositionX() - 80, visibleSize.height - 110);
		Bolazo->setScale(3.5);
		//auto body = PhysicsBody::createCircle(Bolazo->getBoundingBox().size.width/2);
		//body->setContactTestBitmask(true);
		//body->setDynamic(true);
		//Bolazo->setPhysicsBody(body);
		move = (FiniteTimeAction *) MoveTo::create(2/BOLA_SPEED_GODZILLA,
		Point(Nave->getPositionX() - 180, Nave->getPositionY()));
		move->retain();
		seq = Sequence::create(move,NULL);//CallFuncN::create(CC_CALLBACK_1(PlaySceneFinalJapan::Disparollegado, this)), 
	//	NULL);
		seq->retain();
			
		Bolazo->runAction(seq);
		int angle = 180;
		Action *rotate = RepeatForever::create(RotateBy::create(0.5, angle));
 
		rotate->retain();
		Bolazo->runAction(rotate);
		addChild(Bolazo,1);
		
	}

	if (godzilla_disparando == 70)
	{
		this->removeChild(Godzilla);
		Godzilla = Sprite::create("PlaySceneFinalJapan/Godzilla/GodzillaStandBy.png");
		Godzilla->setPosition(visibleSize.width-180,visibleSize.height/2);
		
		
		Godzilla->setScale(4);
		addChild(Godzilla,1);
		counter_godzilla = 6;
		this->unschedule(schedule_selector(PlaySceneFinalJapan::Proceso_disparar));
		this->schedule(schedule_selector(PlaySceneFinalJapan::TimerGodzilla,1.0f));
	}


}

void PlaySceneFinalJapan::gotoPantallaWin(Ref* pSender)
{
	if (desafio_activo)
	{
		Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
	}
	else
	{
		auto scene = PantallaWin2::createScene();
		if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

		Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	}
	
}
