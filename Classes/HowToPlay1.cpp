#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "HowToPlay2.h"
#include "CountryScene.h"
#include "HowToPlay1.h"

USING_NS_CC;



Scene* HowToPlay1::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HowToPlay1::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HowToPlay1::init()
{
	

    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }

	Size visibleSize = Director::getInstance()->getVisibleSize();

	Fondo = Sprite::create("HowToPlayScene/HowToPlayPantalla1.png");
	Fondo->setPosition(visibleSize.width/2,visibleSize.height/2);
	this->addChild(Fondo,0);

	auto Exit = MenuItemImage::create(
											"HowToPlayScene/BotonExit.png",
											"HowToPlayScene/BotonExit.png",
											CC_CALLBACK_1(HowToPlay1::gotoMenu, this));

	Exit ->setPosition(visibleSize.width/2 , visibleSize.height/2 - visibleSize.height/4 + 30);
	Exit->setScale(0.5);
	//Fondo = Sprite::create("HowToPlayScene/HowToPlayPantalla1.png");
	//this->addChild(Fondo,0);

	auto Next = MenuItemImage::create(
											"HowToPlayScene/BotonNext.png",
											"HowToPlayScene/BotonNext.png",
											CC_CALLBACK_1(HowToPlay1::gotoHowToPlay2, this));

	Next->setPosition(visibleSize.width/2 + visibleSize.width/4, visibleSize.height/2 - visibleSize.height/4 + 30);
	Next->setScale(0.5);
	//Exit->setPosition(Vec2(visibleSize.width/2,visibleSize.height/4-50));

	auto menu = Menu::create(Exit,Next, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);



	return true ;
}   

void HowToPlay1::gotoHowToPlay2(Ref* pSender)
{
	auto scene = HowToPlay2::createScene();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void HowToPlay1::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	
	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	
}