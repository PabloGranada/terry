#include "MenuScene.h"
#include "AboutScene.h"
#include "PlayScene.h"
#include "PlayScene2.h"
#include "PlayScene3.h"
#include "PlayScene4.h"
#include "PlayScene5.h"
#include "PlayScene6.h"
#include "PlaySceneFinal.h"
#include "PantallaWin.h"
#include "CountryScene.h"
#include "SelectCountryScene.h"
#include "FinalChallenge.h"
#include "GameOver.h"
#include "GameInformation.h"

USING_NS_CC;

extern int vidas;
extern int vidas_final;
extern bool japon_bloqueado;
extern bool desafio_activo;

Scene* PlaySceneFinal::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = PlaySceneFinal::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PlaySceneFinal::init()
{
	temp_principio = 59;
	contador_reflector = 0;
	contador_perder = 0;
	contador_ganar = 0;
	contador_colision = 15;
	numero_colisiones = 0;
	reflector_disponible = false;
	empezado = false;

	Size visibleSize = Director::getInstance()->getVisibleSize();

	keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(PlaySceneFinal::keyPressed, this);
	//keyboardListener->onKeyReleased = CC_CALLBACK_2(PlayScene2::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
	
	//  if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
		//	 CocosDenshion::SimpleAudioEngine::getInstance()->
		//	 preloadBackgroundMusic("audio/Microjuegos/FUNDIDOSALIDA/FondoMagneto.mp3");
		//	 CocosDenshion::SimpleAudioEngine::getInstance()->
		//	 playBackgroundMusic("audio/Microjuegos/FUNDIDOSALIDA/FondoMagneto.mp3", true);
	//}
	CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Microjuegos/FUNDIDOSALIDA/FondoMagneto.mp3");
    __String *text = __String::createWithFormat(" %d",
    temp_principio );
	


	Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoIntro.png");
	Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
	Magneto->setScale(1.5);
	addChild(Magneto,0);

	Terry = Sprite::create("PlaySceneFinal/Terry/TerryStandBy.png");
	Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
	Terry->setScale(1.5);
	addChild(Terry,0);

	Reflector = Sprite::create("PlaySceneFinal/BolaEnergiaReflector/Reflector.png");
	Reflector ->setPosition(2000.0,2000.0);
	Reflector->setVisible(false);
	addChild(Reflector,0);

	BocadilloReflector = Sprite::create("PlaySceneFinal/Conversaciones/Terry/AtaqueTerry.png");
	BocadilloReflector->setPosition(2000,2000);//Terry->getPosition().x + Terry->getContentSize().width, Terry->getPosition().y + 100);
	addChild(BocadilloReflector,0);

	this->scheduleUpdate();
	this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerPrincipio),1.0f);
	
	return true ;
}   

void PlaySceneFinal::UpdateTimerPrincipio(float ct)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	temp_principio --;

	if (temp_principio == 57)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/Intro1.mp3");
		Intro1 = Sprite::create("PlaySceneFinal/Conversaciones/Magneto/Intro1.png");
		Intro1 ->setPosition(Vec2(Magneto->getPositionX() - 80,Magneto->getPositionY() + 230));
		Intro1 ->setScale(0.5f);
		addChild(Intro1,0);
	}

	if (temp_principio == 53)
	{
		this->removeChild(Intro1);
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/Intro2.mp3");
		Intro2 = Sprite::create("PlaySceneFinal/Conversaciones/Magneto/Intro2.png");
		Intro2 ->setPosition(Vec2(Magneto->getPositionX() - 80,Magneto->getPositionY() + 230));
		Intro2 ->setScale(0.5f);
		addChild(Intro2,0);
	}

	if (temp_principio == 50)
	{
		this->removeChild(Intro2);
		Fondo = Sprite::create("PlaySceneFinal/EscenarioFinal/escenarioMagnetoFinal.png");
		Fondo->setPosition(Vec2(visibleSize.width/2, visibleSize.height/2));
		Fondo ->setScaleX(1.25);
		Fondo->setScaleY(1.46);
		addChild(Fondo,0);

		
		Barras = Sprite::create("PlaySceneFinal/BarraEnergia/TodaLaBarra.png");
		Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		Barras->setScale(1.25);
		addChild(Barras,0);
		

		this->removeChild(Magneto);
		this->removeChild(Terry);
		//A�adido para el reloj.

	frameCache = CCSpriteFrameCache::sharedSpriteFrameCache();
	frameCache->addSpriteFramesWithFile("PlaySceneFinal/Magneto/standby.plist");
    
	spritesheet = CCSpriteBatchNode::create("PlaySceneFinal/Magneto/standby.png");
	this->addChild(spritesheet,3); //Lo ponemos en primera plana con el 3.

	Vector<SpriteFrame*> SpriteFrames(5);
	for ( i = 1; i <= 9; i++)
	{
		CCString* filename = CCString::createWithFormat("standby_0%d.png", i);//Spritesheet
		CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(filename->getCString());
		SpriteFrames.pushBack(frame);
	}

	CCAnimation* runAnim = CCAnimation::createWithSpriteFrames(SpriteFrames,0.4);
	Magneto = CCSprite::createWithSpriteFrameName("standby_01.png");

	Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
	Magneto->setScale(1.5);
    
	CCAction* action = CCRepeatForever::create(CCAnimate::create(runAnim));
    
	Magneto->runAction(action);
	//Frame->setColor(Gold); //Para cambiar los frames a naranja y que no sea del mismo color que el salon.
	spritesheet->addChild(Magneto,3);	
	
	//Aqui termina el a�adido para el reloj.

		//Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoStandBy.png");
		//Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		//Magneto->setScale(1.5);
		//addChild(Magneto,0);

		Terry = Sprite::create("PlaySceneFinal/Terry/TerryStandBy.png");
		Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
		Terry->setScale(1.5);
		addChild(Terry,0);
		empezado = true;
		
		
	    }
  
	if (temp_principio == 46)
	{
		this->removeChild(spritesheet);
		this->removeChild(Magneto);
		Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoCharge.png");
		Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		Magneto->setScale(1.5);
		addChild(Magneto,0);
	}

	if (temp_principio == 45)
	{
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/MagnetoAtaque.mp3");
		BocadilloMagneto = Sprite::create("PlaySceneFinal/Conversaciones/Magneto/MagnetoAtaque.png");
		BocadilloMagneto ->setPosition(Vec2(Magneto->getPositionX() - 80,Magneto->getPositionY() + 230));
		BocadilloMagneto ->setScale(0.5f);
		addChild(BocadilloMagneto,0);

		this->removeChild(Magneto);
		Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoAtaque.png");
		Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		Magneto->setScale(1.5);
		addChild(Magneto,0);

		Bola = Sprite::create("PlaySceneFinal/BolaEnergiaReflector/BolaEnergia.png");
		Bola -> setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4 - 100,Magneto->getContentSize().height / 2 + 120));

		auto *move = (FiniteTimeAction *) MoveTo::create(VELOCIDAD_BOLA,Vec2(Terry->getPositionX() + 80,Terry ->getPositionY() + 80));

		move->retain();

	   	auto *seq = Sequence::create(move,  CallFuncN::create(CC_CALLBACK_1(PlaySceneFinal::Colisionado, this)), 
		 NULL);
		
		seq->retain();
		 
		Bola->runAction(seq);
		Action *rotate = RepeatForever::create(RotateBy::create(0.25, 180));
 
		rotate->retain();
		
		
		Bola->runAction(rotate);
		
		
		addChild(Bola,0);

	}

	if (temp_principio == 43)
	{
		BocadilloMagneto->setVisible(false);
		this->removeChild(Magneto);
		Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoStandBy.png");
		Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		Magneto->setScale(1.5);
		addChild(Magneto,0);
	}


}

 void PlaySceneFinal::update(float dt)
 {
	 Size visibleSize = Director::getInstance()->getVisibleSize();
	 if (empezado)
	 {
		 reflector_disponible = true;
		 empezado = false;
	 }

 }

 void PlaySceneFinal::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	cocos2d::EventKeyboard::KeyCode _pressedKey = keyCode;	switch (_pressedKey) {
		
		case EventKeyboard::KeyCode::KEY_SPACE:
			if (contador_reflector == 0 && reflector_disponible)
			{
				
				reflector_disponible = false;
				this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerReflector),0.4f);
			//	this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerReflector));
				
			}
			break;
	}
}

 void PlaySceneFinal::UpdateTimerReflector(float ct)
 {
	 Size visibleSize = Director::getInstance()->getVisibleSize();
	 contador_reflector ++;

	 if (contador_reflector == 1)
	 {
		 
		 this->removeChild(BocadilloReflector);
		 BocadilloReflector = Sprite::create("PlaySceneFinal/Conversaciones/Terry/AtaqueTerry.png");
		 BocadilloReflector->setPosition(Terry->getPosition().x + Terry->getContentSize().width + 100, Terry->getPosition().y + 120);
		 BocadilloReflector ->setScale(0.5);
		 addChild(BocadilloReflector,0);
	 }
	 
	 if (contador_reflector == 2)
	 {
		 CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/Reflector.mp3");
		 BocadilloReflector->setVisible(false);
		 this->removeChild(Reflector);
		 Reflector = Sprite::create("PlaySceneFinal/BolaEnergiaReflector/Reflector.png");
		 Reflector->setScale(1.5);
		 Reflector->setPosition(Terry->getPosition());
		 addChild(Reflector,0);

		 this->removeChild(Terry);
		 Terry = Sprite::create("PlaySceneFinal/Terry/TerryReflector.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height - 25));
		 Terry->setScale(1.5);
		 addChild(Terry,0);


		 
	 }

	 if (contador_reflector == 5)
	 {
		 Reflector->setVisible(false);
		 this->removeChild(Terry);
		 Terry = Sprite::create("PlaySceneFinal/Terry/TerryRecargandoEnergy.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
		 Terry->setScale(1.5);
		 addChild(Terry,0);
		// reflector_disponible = true;
	 }

	 if (contador_reflector == 7)
	 {
		 this->removeChild(Terry);
		 Terry = Sprite::create("PlaySceneFinal/Terry/TerryStandBy.png");
		 Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
		 Terry->setScale(1.5);
		 addChild(Terry,0);
		 contador_reflector = 0;
		 reflector_disponible = true;
		 this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerReflector));
	 }
 }

 void PlaySceneFinal::Colisionado(Node *pSender) 
 {
	 Size visibleSize = Director::getInstance()->getVisibleSize();

	 if (!Reflector->isVisible())
	 {		 		 
		 if (numero_colisiones == 1)
		 {
			 this->removeChild(Barras);
		     Barras = Sprite::create("PlaySceneFinal/BarraEnergia/Tocado1TerryKO.png");
		     Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		     Barras->setScale(1.25);
		     addChild(Barras,0);
			 contador_perder = 0;
			 this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerPerder,1.0f));
		 }

		 else if (numero_colisiones == 2)
		 {
			 this->removeChild(Barras);
		     Barras = Sprite::create("PlaySceneFinal/BarraEnergia/Tocado2TerryKO.png");
		     Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		     Barras->setScale(1.25);
		     addChild(Barras,0);
			 this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerPerder,1.0f));
		 }

		 else
		 {
			 this->removeChild(Barras);
		     Barras = Sprite::create("PlaySceneFinal/BarraEnergia/TodaLaBarraTerryKO.png");
		     Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		     Barras->setScale(1.25);
		     addChild(Barras,0);
			 this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerPerder,1.0f));
		 }
		//gotoMenu(this);
	 }

	 else
	 {

		 //this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerPerder));
		 //contador_perder = 0;
		 this->removeChild(Bola);
		 Bola = Sprite::create("PlaySceneFinal/BolaEnergiaReflector/BolaEnergia.png");
		Bola -> setPosition(Vec2(Terry->getPositionX() + 80,Terry ->getPositionY() + 80));

		auto *move = (FiniteTimeAction *) MoveTo::create(VELOCIDAD_BOLA,Vec2(Magneto->getPositionX(),Magneto ->getPositionY() + 80));

		move->retain();

		//auto *seq =  Sequence::create(move,NULL);
		auto *seq = Sequence::create(move, CallFuncN::create(CC_CALLBACK_1(PlaySceneFinal::Colisionado_Magneto, this)), NULL);
		 
		Action *rotate = RepeatForever::create(RotateBy::create(0.25, 180));
 
		rotate->retain();
		
		seq->retain();
		Bola->runAction(rotate);
		Bola->runAction(seq);
		addChild(Bola,0);

		//this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision,0.75f));
	 }
 }

 void PlaySceneFinal::gotoMenu(Ref* pSender)
{   
	auto scene = MenuScene::createScene();
	CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying() == false) {
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 preloadBackgroundMusic("audio/Menu/MainTheme.mp3");
			 CocosDenshion::SimpleAudioEngine::getInstance()->
			 playBackgroundMusic("audio/Menu/MainTheme.mp3", true);
	}
	Director::getInstance()->popScene();
	Director::getInstance()->replaceScene(TransitionFade::create(1.0,scene));
	
}

void PlaySceneFinal::UpdateTimerPerder(float ct)
{
	
	  Size visibleSize = Director::getInstance()->getVisibleSize();

	 if (contador_perder == 0)
	 {
		 this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerPrincipio));
		  this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerReflector));
		  keyboardListener->setEnabled(false);
		  this->removeChild(Terry);
		  Terry = Sprite::create("PlaySceneFinal/Terry/TerryDead.png");
		  Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
		  Terry->setScale(0.75);
		  addChild(Terry,0);

		  this->removeChild(BocadilloMagneto);
		  this->removeChild(Intro2);
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/MagnetoWin.mp3");
		  this->removeChild(Magneto);
		  Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoIntro.png");
		  Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		  Magneto->setScale(1.5);
		  addChild(Magneto,0);
		  Intro2 = Sprite::create("PlaySceneFinal/Conversaciones/Magneto/MagnetoWin.png");
		  Intro2 ->setPosition(Vec2(Magneto->getPositionX() - 80,Magneto->getPositionY() + 230));
		  Intro2 ->setScale(0.5f);
		  addChild(Intro2,0);

		  this->removeChild(Bola);
	 }
	 if (contador_perder == 200)
	 {
		  CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		  vidas --;
		  vidas_final--;
		 gotoCountry(this);
		 
	 }
	  contador_perder += 1;

	
}


  void PlaySceneFinal::Colisionado_Magneto(Node *pSender) 
  {
	  
	  Size visibleSize = Director::getInstance()->getVisibleSize();

	  this->removeChild(Bola);
	  this->removeChild(Magneto);
	  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/MagnetoDamage.mp3");
	  Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoGolpeado.png");
	  Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
	  Magneto->setScale(1.5);
	  addChild(Magneto,0);
	  numero_colisiones ++;

		if (numero_colisiones == 1 )
	    {
		  this->removeChild(Barras);
		  Barras = Sprite::create("PlaySceneFinal/BarraEnergia/Tocado1.png");
		  Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		  Barras->setScale(1.25);
		  temp_principio = 50;
		  addChild(Barras,0);
		  
		  //this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerColision));
		  //this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision,2.0f));
	    }

	    else if (numero_colisiones == 2 )
	    {
		  this->removeChild(Barras);
		  Barras = Sprite::create("PlaySceneFinal/BarraEnergia/Tocado2.png");
		  Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		  Barras->setScale(1.25);
		  addChild(Barras,0);
		  temp_principio = 49;
		 
		 // this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerColision));
		 // this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision,0.5f));
	    }

	    else if (numero_colisiones == 3 )
	    {
		  this->removeChild(Barras);
		  Barras = Sprite::create("PlaySceneFinal/BarraEnergia/MagnetoKO.png");
		   Barras ->setPosition(Vec2(visibleSize.width/2, visibleSize.height - 100));
		   Barras->setScale(1.25);
		   addChild(Barras,0);
		   this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerPrincipio));
		   this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision_final,1.0f));

		  
		  // this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerColision));
		   //this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision_final,1.0f));
	    }
	 
	 
	  
	  
	 /* 
	  */
	 // this->schedule(schedule_selector(PlaySceneFinal::UpdateTimerColision,0.75f));

  }
  /*
   void PlaySceneFinal::UpdateTimerColision(float ct)
   {
	    Size visibleSize = Director::getInstance()->getVisibleSize();

	   contador_colision --;
	   if (contador_colision == 13)
	   {
		    Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoStandBy.png");
	        Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
	        Magneto->setScale(1.5);
	        addChild(Magneto,0);
	   }

	   else if (contador_colision == 10)
	   {
		   this->removeChild(Magneto);
		   Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoCharge.png");
		   Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		   Magneto->setScale(1.5);
		   addChild(Magneto,0);
	   }

	   else if (contador_colision == 7)
	   {
		  // CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/MagnetoAtaque.mp3japon_bl
		   BocadilloMagneto = Sprite::create("PlaySceneFinal/Conversaciones/Magneto/MagnetoAtaque.png");
	       BocadilloMagneto ->setPosition(Vec2(Magneto->getPositionX() - 80,Magneto->getPositionY() + 230));
		   BocadilloMagneto ->setScale(0.5f);
		   addChild(BocadilloMagneto,0);

		   this->removeChild(Magneto);
		   Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoAtaque.png");
		   Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 + 80));
		   Magneto->setScale(1.5);
		   addChild(Magneto,0);

		   Bola = Sprite::create("PlaySceneFinal/BolaEnergiaReflector/BolaEnergia.png");
		   Bola -> setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4 - 100,Magneto->getContentSize().height / 2 + 120));

		   auto *move = (FiniteTimeAction *) MoveTo::create(VELOCIDAD_BOLA,Vec2(Terry->getPositionX() + 80,Terry ->getPositionY() + 80));

		   move->retain();

	   	   auto *seq = Sequence::create(move, CallFuncN::create(CC_CALLBACK_1(PlaySceneFinal::Colisionado, this)),  NULL);
		

		   seq->retain();
		 
		   Bola->runAction(seq);
		   addChild(Bola,0);

		   //contador_colision = 15;
		   //this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerColision));
	   }
   }
   */

 void PlaySceneFinal::UpdateTimerColision_final(float ct)
 {
	 Size visibleSize = Director::getInstance()->getVisibleSize();

	 contador_ganar ++;
	  if (contador_ganar == 20)
	 {
		  this->unschedule(schedule_selector(PlaySceneFinal::UpdateTimerReflector));
		  keyboardListener->setEnabled(false);
		  this->removeChild(Terry);
		  Terry = Sprite::create("PlaySceneFinal/Terry/TerryWin.png");
		  Terry->setPosition(Vec2(visibleSize.width/2 - visibleSize.width /4, Terry->getContentSize().height));
		  Terry->setScale(1.25);
		  addChild(Terry,0);

		  this->removeChild(Magneto);
		  CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("audio/Sonidos/Magneto/MagnetoLose.mp3");
		  Magneto = Sprite::create("PlaySceneFinal/Magneto/MagnetoDead.png");
		  Magneto->setPosition(Vec2(visibleSize.width/2 + visibleSize.width /4, Magneto->getContentSize().height / 2 ));
	      Magneto->setScale(1.5);
	      addChild(Magneto,0);

		  this->removeChild(Bola);
	 }
	 if (contador_ganar == 200)
	 {
		 CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
		 japon_bloqueado = false;
		 gotoPantallaWin(this);
		 
	 }
	 //gotoMenu(this);
 }

 void PlaySceneFinal::gotoSelectCountry(Ref* pSender)
{
	auto scene = SelectCountryScene::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
}

void PlaySceneFinal::gotoPantallaWin(Ref* pSender)
{
	if (desafio_activo)
	{
		Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
	}
	else
	{
	auto scene = PantallaWin::createScene();
	if (CocosDenshion::SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying()) 
			{
				CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
			}

		else
				CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();

	Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	}
}


void PlaySceneFinal::gotoCountry(Ref* pSender)
{
	//auto scene = CountryScene::createScene();

	//Director::getInstance()->pushScene(TransitionFade::create(1.0,scene));
	Director::getInstance()->popScene();//ushScene(TransitionFade::create(1.0,scene));
}


