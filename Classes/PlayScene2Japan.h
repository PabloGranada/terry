#ifndef __PLAY_SCENE2JAPAN_H__
#define __PLAY_SCENE2JAPAN_H__

#include "cocos2d.h"

USING_NS_CC;



class PlayScene2Japan : public cocos2d::Layer
{
public:

	//FARAMIR
	//Animacion de reloj
	const char* Spritesheet;
	CCSpriteFrameCache* frameCache;
	CCSpriteBatchNode* spritesheet;
	CCSprite* Frame;
	Vec2* charDirector;
	//Fin de la declaracion de mierda para la animacion.

	bool ganado;
	bool lanzado;
	int contador_destello,i;
	float VELOCIDAD ;
	float pos_abajo_y;
	float pos_arriba_y;
	float aumento_bola;
	float escala_inicial;
	float escala_final;
	int counter;
	Sprite *SpriteVacio;
	Sprite *Fondo;
	Sprite *Destello_imagen;
	Sprite *Terry;
	Sprite *Freezer;
	Sprite *BolaGenki;
	LabelTTF *label_timer;
	Point posicion_mouse,posicion_mouse_original;
	EventListenerKeyboard *keyboardListener;
	EventListenerTouchAllAtOnce *touchListener;
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

	void update(float dt);
	void Destello(float ct);
	void UpdateTimer(float ct);
	void MoverAbajo(Node *pSender);
	void MoverArriba(Node *pSender);
	void Movido(Node *pSender);

	void onTouchesBegan(const std::vector<cocos2d::Touch *> &touches, cocos2d::Event *event);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	cocos2d::Point get_cursor_pos();
	void gotoCountry2(Ref* pSender);
	void Lanzado(Node *pSender);
	void Lanzar_bola();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	
    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(PlayScene2Japan);
};

#endif // __MENU_SCENE_H__
