#ifndef __HOWTO_PLAY1_H__
#define __HOWTO_PLAY1_H__

#include "cocos2d.h"

USING_NS_CC;



class HowToPlay1 : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
	Sprite *Fondo;
	Sprite *Exit;
	Sprite *Next;
	
	void gotoHowToPlay1(Ref* pSender);
	void gotoHowToPlay2(Ref* pSender);
	 void gotoMenu(Ref* pSender);

    // a selector callbackS
        
    // implement the "static create()" method manually
    CREATE_FUNC(HowToPlay1);
};

#endif // __MENU_SCENE_H__
